package PageObject;

import java.io.File;
import java.sql.Driver;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

//import airtestclass.Faredepot_Test;


public class Faredepot_chkoutpge {
	
	public WebDriver driver1;
	//public Faredepot_Test driver2 = new Faredepot_Test();
	
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_Title")
	WebElement gndr;

	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_FirstName")
	WebElement FrstNme;
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_LastName")
	WebElement Lastnme;
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Month")
	WebElement Month;
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Day")
	WebElement Day;
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Year")
	WebElement Year;
	
	@FindBy(xpath="//*[@css='INPUT.spc-cont-btn.primary-btn.fl-right-imp']")
	WebElement Continue;
				
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingContact_EMail")
	WebElement Email;
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingContact_ConfirmEMail")
	WebElement CnfrmEmail;
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingContact_Phone1_Number")
	WebElement Phone;
	
	@FindBy(id="_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_GuestResidency_StateSel")
	WebElement State;
	
	@FindBy(id="InsurranceCHK_239399")
	WebElement Insurance;
	
	@FindBy(xpath="//*[@text='Pages']")
	WebElement Pages;
	
	@FindBy(xpath="//*[@text='Close ‎webservices.travelguard.com/Product/FileRetrieval.aspx?CountryCode=US&StateCode=CA&ProductCode=CA9081&PlanCode=P1&FileType=PROD_PLAN_DOC']")
	WebElement CloseWindow;
	
	@FindBy(xpath="//*[@text='Select Air Passengers & Preferences : FareDepot.com']")
	WebElement SelectWindow;


	
	
public void Chkoutpge() {
	try {
		
		Select select = new Select(gndr);
		select.selectByVisibleText("Mr"); 
		FrstNme.sendKeys("Monish");
		
		Lastnme.sendKeys("Luthra");
		Select select1 = new Select(Month);
		select1.selectByVisibleText("May");
		Select select2 = new Select(Day);
		select2.selectByVisibleText("15");
		Select select3 = new Select(Year);
		select3.selectByVisibleText("1960");
		Continue.click();
		Email.click();
		Email.sendKeys("Earl_Fernandes@odysseussolutions.com");
		CnfrmEmail.sendKeys("Earl_Fernandes@odysseussolutions.com");
		Phone.sendKeys("1234567891");
		Continue.click();
		Select select4 = new Select(State);
		select4.selectByVisibleText("California");
		
		
	//this.driver = driver;

    //This initElements method will create all WebElements

    //PageFactory.initElements(driver, this);
	}
	catch (Exception e){
		
	
	/*{
		File scr = ((TakesScreenshot)driver2.driver).getScreenshotAs(OutputType.FILE);
		
		   try
		   {
			   FileUtils.copyFile(scr, new File("E:\\Automation\\Faredepot_TestError\\bookingpge.jpg"));
			
		   } catch(Exception e1)
		     {
			     e1.printStackTrace();
			     e.getMessage();
			     System.out.println(e);
			     
		     }
		   e.getMessage();
System.out.println(e);*/	
}
}
}
