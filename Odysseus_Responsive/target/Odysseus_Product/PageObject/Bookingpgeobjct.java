package PageObject;

import org.openqa.selenium.By;

import org.openqa.selenium.Keys;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Bookingpgeobjct

{

	@FindBy(id = "AirSearchForm_From")

	private WebElement Fromloc;
	
	@FindBy (xpath="//*[@css='DIV.list_item_hover']")
	WebElement CityFrom;

	@FindBy(id = "AirSearchForm_To")

	private WebElement Toloc;
	
	@FindBy (xpath="//*[@css='DIV.list_item_hover']")
	WebElement CityTo;

	@FindBy(id = "AirSearchForm_FromDate")

	private WebElement selectfrmdte;

	@FindBy(id = "AirSearchForm_FromTime")

	private WebElement searchtime;
	
	@FindBy (xpath="//*[@css='SELECT.calText']")
	WebElement Date;

	@FindBy(id = "AirSearchForm_ReturnDate")

	private WebElement selecttodte;

	@FindBy(id = "AirSearchForm_Adults")

	private WebElement selectadlt;
	
	@FindBy (xpath ="//*[@css='DIV.list_item_hover']")
	WebElement Adult;

	@FindBy(id = "AirSearchForm_Children")

	private WebElement selectchild;
	
	@FindBy (xpath ="//*[@css='DIV.list_item_hover']")
	WebElement Child;

	@FindBy (xpath="//*[@text='>']")
	WebElement NextBtn;
	
	@FindBy (linkText="1")
	WebElement First;
	
	@FindBy(xpath = "//*[@id='DDListPopUp']/div[1]")

	private WebElement Allclass;

	@FindBy(xpath = "//*[@id='AirSearchForm']/div/div[2]/div/dl[4]/dd/a/span")
		
	private WebElement SearchAir;
	
	@FindBy(id="AirSearchForm_Lbl_FlightSearch")
	private WebElement AlanitaSearchAir;
	
		
	public void BookingToTitle(String fmloc, String toloc)

	{

		try

		{

			Fromloc.sendKeys(fmloc);

			CityFrom.click();


			Toloc.sendKeys(toloc);

			CityTo.click();


			selectfrmdte.click();

	
			for (int i=1; i<2; i++) {
				NextBtn.click();
			}
			
			
			First.click();																										
																			

			//selectadlt.click();

			//Adult.click();

			//selectchild.click();
			
			//Child.click();

			//SearchAir.click();

		}

		catch (Exception e)

		{

			e.printStackTrace();
			System.out.println(e);

		}

	}
	
	
	public boolean isDisplayed() {

		// TODO Auto-generated method stub

		return false;

	}

}
