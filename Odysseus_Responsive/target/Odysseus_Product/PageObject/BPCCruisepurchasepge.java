package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;


public class BPCCruisepurchasepge
{

		public IOSDriver<IOSElement> driver;
	 
	    //For Purchase page(Passenger details)
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Title")
		private WebElement selecttittle;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_GenderSEL")
		private WebElement selectgender;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_FirstName")
		private WebElement firstnme;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_MiddleName")
		private WebElement midlenme;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_LastName")
		private WebElement lastnme;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_DateUC_Month")
		private WebElement month;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_DateUC_Day")
		private WebElement Day;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_DateUC_Year")
		private WebElement Year;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Nationality")
		private WebElement natnality;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_PastPaxNumber")
		private WebElement pastpsngr;
		
		@FindBy(id="//*[@id='_ctl0_MainContentsPH__ctl0_TravelerAccount_1_AddressUC_CountrySel']")
		private WebElement cunty;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_AddressUC_AddrLine1")
		private WebElement Address;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_AddressUC_CityInput")
		private WebElement cty; 
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_AddressUC_StateSel")
		private WebElement State; 
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_AddressUC_ZipCodeInput")
		private WebElement Pncode;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_EMail")
		private WebElement Email;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone1_Number")
		private WebElement Phne;
		
		@FindBy(xpath="//*[@id='_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone1_PhoneTypeSEL']")
		private WebElement Phntype;
		
		@FindBy(xpath="//*[@id='_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone2_Number']")
		private WebElement secondPhn;
		
		@FindBy(xpath="//*[@id='_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone2_PhoneTypeSEL']")
		private WebElement secondPhntype;
		
		//For Guest Two
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_Title")
		private WebElement Titleofguesttwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_GenderSEL")
		private WebElement Gendertwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_FirstName")
		private WebElement Firstnmeofguesttwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_MiddleName")
		private WebElement Middlenmeofguesttwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_LastName")
		private WebElement Lastnmeofguesttwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_DateUC_Month")
		private WebElement Monthofguesttwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_DateUC_Day")
		private WebElement Dayofguesttwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_DateUC_Year")
		private WebElement Yearofguesttwo;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_TravelerAccount_2_Nationality")
		private WebElement selctcntry;
		
		//For Dining Preference 
		@FindBy(id="DiningPrefOption_0")
		private WebElement diningpref;
		
		//Table size preferences
		@FindBy(id="_ctl0_MainContentsPH__ctl0_DiningTblSizeSEL")
		private WebElement tblszepref;
		
		//For select bed preference
		@FindBy(xpath="//*[@id='BedTypePrefOption_0']")
		private WebElement bedpref; 
		
		//For select special requests
		@FindBy(id="cb6")
		private WebElement spclreq;
		
		@FindBy(id="SServ_PPGR_0")
		private WebElement selctservce;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_XRefBookingsTXT")
		private WebElement Othrpref;
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_AgentRequestedTXT")
		private WebElement spcificagnt;
		
		//For click on continue to final booking review button
		@FindBy(xpath="//*[@id='_ctl0_MainContentsPH__ctl0_ContinueLNK']/span[1]")
		private WebElement clckonfnalbkng; 
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_FastBookLNK")
		private WebElement Bookmycruisebtn;
		
		
		@FindBy(id="_ctl0_MainContentsPH__ctl0_ContinueLNK")
		private WebElement DLCContinuebtn;
		

	

		
	 public void PurchaseToTittle(String frstnme, String mdlnme, String lstnme, String addrss, String ct, String pncde, String eml, String phn, String frstnmeoftwo, String mdlnmetwo, String lstnmetwo)
	 {
	  	 try
		 {
	  		   Select select = new Select(selecttittle);
			   select.selectByVisibleText("Mr");
			   Select select1 = new Select(selectgender);
			   select1.selectByVisibleText("Male");
			   
			   firstnme.sendKeys(frstnme);
			   
			   midlenme.sendKeys(mdlnme);
			   
			   lastnme.sendKeys(lstnme);
			   Select select2 = new Select(month);
			   select2.selectByVisibleText("Apr");
			   Select select3 = new Select(Day);
			   select3.selectByVisibleText("15");			   
			   Select select4 = new Select(Year);
			   select4.selectByVisibleText("1950");			   
			   Select select5 = new Select(natnality);
			   select5.selectByVisibleText("United States");			   
			   pastpsngr.sendKeys("No");
			   
			   Address.sendKeys(addrss);
			
			   cty.sendKeys(ct);
			   
			   Select select6 = new Select(State);
			   select6.selectByVisibleText("Florida");
			   
			   Pncode.sendKeys(pncde);
			   
			   Email.sendKeys(eml);
			   Phne.sendKeys(phn);
			   
			   Select select7 = new Select(Phntype);
			   select7.selectByVisibleText("Cell");
			   /*secondPhn.sendKeys("8596587482");
			   secondPhn.sendKeys(Keys.TAB);
			   secondPhntype.findElement(By.xpath("//*[@id='_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone2_PhoneTypeSEL']/option[1]")).click();
			   secondPhntype.sendKeys(Keys.TAB);*/
			   Select select8 = new Select(Titleofguesttwo);
			   select8.selectByVisibleText("Miss");	
			   Select select9 = new Select(Gendertwo);
			   select9.selectByVisibleText("Female");
			  
			   Firstnmeofguesttwo.sendKeys(frstnmeoftwo);
			   Middlenmeofguesttwo.sendKeys(mdlnmetwo);
			   Lastnmeofguesttwo.sendKeys(lstnmetwo);
			   Select select10 = new Select(Monthofguesttwo);
			   select10.selectByVisibleText("Apr");
			   Select select11 = new Select(Dayofguesttwo);
			   select11.selectByVisibleText("15");
			   Select select12 = new Select(Yearofguesttwo);
			   select12.selectByVisibleText("2013");
			   Select select13 = new Select(selctcntry);
			   select13.selectByVisibleText("United States");
			   
			   
			   
			   /*if(diningpref.isDisplayed())
			   {
				   diningpref.click();
				   Thread.sleep(900);
			   }
			  
			   
		  
		     //For Table preferences	   
		       if(tblszepref.findElement(By.id("BedTypePrefOption_1")).isDisplayed())
			   {
				   tblszepref.findElement(By.id("BedTypePrefOption_1")).click();
				   Thread.sleep(800);
				   tblszepref.sendKeys(Keys.TAB);
			   } 
			       Thread.sleep(2000);
			   
			  
	 	   
			   if(bedpref.isDisplayed())
			   {
				   WebElement element = bedpref.findElement(By.xpath("//*[@id='BedTypePrefOption_0']"));
             	   element.sendKeys(Keys.ENTER);
				   Thread.sleep(500);
				   
			   }
	  		   
			       bedpref.sendKeys(Keys.TAB);
			       Thread.sleep(500);
			   
		     //For Select request preferences	 
		       if(spclreq.findElement(By.id("cb1")).isDisplayed())
		 	   {
				   spclreq.click();    //For pre_paid gratuities
				   Thread.sleep(500);
				   spclreq.findElement(By.id("SServ_E001_0")).click();
				   Thread.sleep(500);
				   spclreq.findElement(By.xpath("//*[@id='SServ_E001_0']/option[3]")).click();
				   Thread.sleep(1000); 
				   spclreq.findElement(By.id("SServ_E001_0_Remark")).click();
				   spclreq.sendKeys("20");
				   Thread.sleep(500);
				   spclreq.sendKeys(Keys.TAB);
				   spclreq.sendKeys(Keys.TAB);
				   spclreq.sendKeys(Keys.TAB);
				   spclreq.sendKeys(Keys.TAB);
				   Thread.sleep(500);
				   
			   } 
			   
			   if(Othrpref.findElement(By.id("_ctl0_MainContentsPH__ctl0_XRefBookingsTXT")).isDisplayed())
			   {
				   Othrpref.click();
				   Othrpref.sendKeys("No");
				   Thread.sleep(500);
				   Othrpref.sendKeys(Keys.TAB);
				   Thread.sleep(500);
			   }
			   
			   if(spcificagnt.findElement(By.id("_ctl0_MainContentsPH__ctl0_AgentRequestedTXT")).isDisplayed())
			   {
				   spcificagnt.click();
				   spcificagnt.sendKeys("No");
				   Thread.sleep(500);
				   spcificagnt.sendKeys(Keys.TAB);
				   Thread.sleep(500);
			   }*/
			   
			   
			   clckonfnalbkng.sendKeys(Keys.ENTER);
			   Thread.sleep(4000);
			 
		 }
		 catch(Exception e)
		 {
				 
			   e.printStackTrace();
				     
		 }
	  	 
	  	 
			
			
	}
	 
	 public void CDORPurchasetotitle(String frstnme, String mdlnme, String lstnme, String addrss, String ct, String pncde, String eml, String phn, String frstnmeoftwo, String mdlnmetwo, String lstnmetwo)
	 {
	  	 try
		 {
	  		   Select select1 = new Select(selectgender);
			   select1.selectByVisibleText("Male");
			   Select select = new Select(selecttittle);
			   select.selectByVisibleText("Mr");
			   
			   
			   firstnme.sendKeys(frstnme);
			   
			   midlenme.sendKeys(mdlnme);
			   
			   lastnme.sendKeys(lstnme);
			   Select select2 = new Select(month);
			   select2.selectByVisibleText("Apr");
			   Select select3 = new Select(Day);
			   select3.selectByVisibleText("15");			   
			   Select select4 = new Select(Year);
			   select4.selectByVisibleText("1950");			   
			   Select select5 = new Select(natnality);
			   select5.selectByVisibleText("United States");			   
			   
			   
			   
			   
			   
			   
			   Email.sendKeys(eml);
			   Phne.sendKeys(phn);
			   
			   /*secondPhn.sendKeys("8596587482");
			   secondPhn.sendKeys(Keys.TAB);
			   secondPhntype.findElement(By.xpath("//*[@id='_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone2_PhoneTypeSEL']/option[1]")).click();
			   secondPhntype.sendKeys(Keys.TAB);*/
			   Select select9 = new Select(Gendertwo);
			   select9.selectByVisibleText("Female");
			   Select select8 = new Select(Titleofguesttwo);
			   select8.selectByVisibleText("Miss");	
			   
			  
			   Firstnmeofguesttwo.sendKeys(frstnmeoftwo);
			   Middlenmeofguesttwo.sendKeys(mdlnmetwo);
			   Lastnmeofguesttwo.sendKeys(lstnmetwo);
			   Select select10 = new Select(Monthofguesttwo);
			   select10.selectByVisibleText("Apr");
			   Select select11 = new Select(Dayofguesttwo);
			   select11.selectByVisibleText("15");
			   Select select12 = new Select(Yearofguesttwo);
			   select12.selectByVisibleText("2013");
			   Select select13 = new Select(selctcntry);
			   select13.selectByVisibleText("United States");
			   
			   
			   Bookmycruisebtn.click();
			   
			   
			 
		 }
		 catch(Exception e)
		 {
				 
			   e.printStackTrace();
				     
		 }
	  	 
	  	 
			
			
	}
	 
	 public void DLCPurchasetotitle(String frstnme, String mdlnme, String lstnme, String addrss, String ct, String pncde, String eml, String phn, String frstnmeoftwo, String mdlnmetwo, String lstnmetwo)
	 {
	  	 try
		 {
	  		   
			   Select select = new Select(selecttittle);
			   select.selectByVisibleText("Mr");
			   Select select1 = new Select(selectgender);
			   select1.selectByVisibleText("Male");
			   
			   firstnme.sendKeys(frstnme);
			   
			   midlenme.sendKeys(mdlnme);
			   
			   lastnme.sendKeys(lstnme);
			   Select select2 = new Select(month);
			   select2.selectByVisibleText("Apr");
			   Select select3 = new Select(Day);
			   select3.selectByVisibleText("15");			   
			   Select select4 = new Select(Year);
			   select4.selectByVisibleText("1950");			   
			   Select select5 = new Select(natnality);
			   select5.selectByVisibleText("United States");			   
			   
			   
			   
			   
			   
			   
			   Email.sendKeys(eml);
			   Address.sendKeys(addrss);
			   cty.sendKeys("San Francisco");
			   Select select14 = new Select (State);
			   select14.selectByVisibleText("California");
			   Pncode.sendKeys(pncde);
			   Phne.sendKeys(phn);

			   
			   
			   /*secondPhn.sendKeys("8596587482");
			   secondPhn.sendKeys(Keys.TAB);
			   secondPhntype.findElement(By.xpath("//*[@id='_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone2_PhoneTypeSEL']/option[1]")).click();
			   secondPhntype.sendKeys(Keys.TAB);*/
			   Select select9 = new Select(Gendertwo);
			   select9.selectByVisibleText("Female");
			   Select select8 = new Select(Titleofguesttwo);
			   select8.selectByVisibleText("Miss");	
			   
			  
			   Firstnmeofguesttwo.sendKeys(frstnmeoftwo);
			   Middlenmeofguesttwo.sendKeys(mdlnmetwo);
			   Lastnmeofguesttwo.sendKeys(lstnmetwo);
			   Select select10 = new Select(Monthofguesttwo);
			   select10.selectByVisibleText("Apr");
			   Select select11 = new Select(Dayofguesttwo);
			   select11.selectByVisibleText("15");
			   Select select12 = new Select(Yearofguesttwo);
			   select12.selectByVisibleText("2013");
			   Select select13 = new Select(selctcntry);
			   select13.selectByVisibleText("United States");
			   
			   //Swipe down
			   
			   
			   
			 
		 }
		 catch(Exception e)
		 {
				 
			   e.printStackTrace();
				     
		 }
	  	 
	  	 
			
			
	}
	
}
