package PageObject;

import java.io.File;
import java.sql.Driver;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class Faredepot_Srchpge {
	
	WebDriver driver;
	
	@FindBy (id="AirSearchForm_From")
	WebElement SearchFrom;
	
	@FindBy (xpath="//*[@css='DIV.list_item_hover']")
	WebElement CityFrom;
	
	@FindBy (id="AirSearchForm_To")
	WebElement SearchTo;
	
	@FindBy (xpath="//*[@css='DIV.list_item_hover']")
	WebElement CityTo;
	
	@FindBy (id="AirSearchForm_FromDate")
	WebElement FromDate;
	
	@FindBy (xpath="//*[@text='>']")
	WebElement NextBtn;
	
	@FindBy (linkText="1")
	WebElement First;
	
	@FindBy (linkText="Search Now")
	WebElement SearchBtn;
	
	
	public void SearchPage() {
		try {
			
		
		SearchFrom.sendKeys("nyc");
		CityFrom.click();
		
		SearchTo.sendKeys("mia");
		CityTo.click();
		
		FromDate.sendKeys(Keys.ENTER);
		
		for (int i=1; i<2; i++) {
			NextBtn.click();
		}
		
		
		First.click();
		SearchBtn.click();
		//this.driver = driver;

        //This initElements method will create all WebElements

        //PageFactory.initElements(driver, this);
		}
		catch (Exception e)
		{
			File scr = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			
			   try
			   {
				   FileUtils.copyFile(scr, new File("D:\\Ajit\\Script_SS\\login\\bookingpge.jpg"));
				
			   } catch(Exception e1)
			     {
				     e1.printStackTrace();
				     
			     }
		}
	}

	
	
	

}
