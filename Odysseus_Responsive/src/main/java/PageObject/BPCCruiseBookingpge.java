package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class BPCCruiseBookingpge 
{

	// Cruise Search
	@FindBy(id="StartDate")
	private WebElement Selectfrmdte; 
	
    @FindBy(id="EndDate")
	private WebElement selecttodte;   
		
	@FindBy(id="Suppliers")  
	private WebElement selectcruiseline;    
		
	@FindBy(id="btnSearch")
	private WebElement Clicksearchbtn;   
	
	//For cruises/results.aspx?
	@FindBy(className="ns-viewBtn")
	private WebElement bookbtn;  
	
	//For cruises/details.aspx?
	@FindBy(id="_ctl0:MainContentsPH:_ctl0:_ctl1_GuestAge_1")
	private WebElement Guest1;
	
	@FindBy(id="_ctl0:MainContentsPH:_ctl0:_ctl1_GuestAge_2")
	private WebElement Guest2;
	
	@FindBy(id="_ctl0_MainContentsPH__ctl0__ctl1_ResidentState")
	private WebElement selectstate;
	
	@FindBy(id="_ctl0_MainContentsPH__ctl0__ctl1_CategoryIBTN")
	private WebElement selectcontinue;
	
	
	public void BookingToTittle()
	{
		try
		{
			Selectfrmdte.sendKeys("04/01/2018");
			Thread.sleep(1000);
			
			selecttodte.sendKeys("04/30/2018");
			Thread.sleep(1000);
			Select select = new Select(selectcruiseline);
			select.selectByVisibleText("Cunard"); // Viva Voyage: Norwegian Cruise Line, BPC: Seabourn, 
			Thread.sleep(1000);
			Clicksearchbtn.click();
			Thread.sleep(5000);
						
		}
		catch(Exception e)
		{
			 
			   e.printStackTrace();
			     
		}
	}	
		

	public boolean isDisplayed()
	{
		return false;
	}
	
	
	
}
