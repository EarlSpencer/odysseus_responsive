package PageObject;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.ui.Select;

public class Purchasepgeobjct

{

	// Click on Travel details

	@FindBy(xpath = "//*[@id='st-accordion']/ul/li[1]/a")

	private WebElement clckontrvldetls;

	// For Checkout page (For passenger 1)

	// For selecting "MR" or "MRS" ...etc
	@FindBy(xpath = "//*[@id='_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_Title']")

	private WebElement selctitle;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_Title")

	private WebElement Alanitaselctitle;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Title")
	private WebElement Faregeekselcttitle;

	// For selecing Gender... (only available in faregeek till now)

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_GenderSEL")

	private WebElement Gender;

	// For selecting Firstname...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_FirstName")

	private WebElement Firstnme;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_FirstName")

	private WebElement AlanitaFirstnme;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_FirstName")

	WebElement FaregeekFirstnme;

	// For selecting middlename...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_MiddleName")

	private WebElement Middlenme;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_MiddleName")

	private WebElement AlanitaMiddlenme;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_MiddleName")

	private WebElement FaregeekMiddlenme;

	// For selecting lastname...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_LastName")

	private WebElement lastnme;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_LastName")

	private WebElement Alanitalastnme;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_LastName")

	private WebElement Faregeeklastnme;

	// For selecting month...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Month")

	private WebElement selctmonth;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Month")

	private WebElement Alanitaselctmonth;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_DateUC_Month")

	private WebElement Faregeekselctmonth;

	// For selecting day...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Day")

	private WebElement selctday;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Day")

	private WebElement Alanitaselctday;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_DateUC_Day")

	private WebElement Faregeekselctday;

	// For selecting year...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Year")

	private WebElement selctyear;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingPassengers__ctl0_TravelerAccount_1_DateUC_Year")

	private WebElement Alanitaselctyear;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_DateUC_Year")

	private WebElement Faregeekselctyear;

	// For passenger 2

	@FindBy(xpath = "//*[@id='_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl1_TravelerAccount_2_Title']")

	private WebElement selecttitle;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl1_TravelerAccount_2_FirstName")

	private WebElement frstnm;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl1_TravelerAccount_2_MiddleName")

	private WebElement midlnme;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl1_TravelerAccount_2_LastName")

	private WebElement lstnm;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl1_TravelerAccount_2_DateUC_Month")

	private WebElement slctmnth;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl1_TravelerAccount_2_DateUC_Day")

	private WebElement slctday;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl1_TravelerAccount_2_DateUC_Year")

	private WebElement slctyr;

	// For passenger 3 (Child)

	@FindBy(xpath = "//*[@id='_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl2_TravelerAccount_3_Title']")

	private WebElement slctitle;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl2_TravelerAccount_3_FirstName")

	private WebElement slectfrstname;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl2_TravelerAccount_3_MiddleName")

	private WebElement mdlename;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl2_TravelerAccount_3_LastName")

	private WebElement slectlstnme;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl2_TravelerAccount_3_DateUC_Month")

	private WebElement slectmnth;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl2_TravelerAccount_3_DateUC_Day")

	private WebElement slectday;

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingPassengers__ctl2_TravelerAccount_3_DateUC_Year")

	private WebElement slectyr;

	@FindBy(xpath = "//*[@css='INPUT.spc-cont-btn.primary-btn.fl-right-imp']")

	private WebElement Continue;

	// For Booking Contact Information

	// For email...
	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingContact_EMail")

	private WebElement email;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingContact_EMail")

	private WebElement Alanitaemail;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_EMail")

	private WebElement Faregeekemail;

	// For confirm email...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingContact_ConfirmEMail")

	private WebElement cnfmemail;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingContact_ConfirmEMail")

	private WebElement Alanitacnfmemail;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_ConfirmEMail")

	private WebElement Faregeekcnfmemail;

	// For phone number...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingContact_Phone1_Number")

	private WebElement phoneno;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingContact_Phone1_Number")

	private WebElement Alanitaphoneno;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_TravelerAccount_1_Phone1_Number")

	private WebElement Faregeekphoneno;

	// For phone at destination...

	@FindBy(id = "_ctl0__ctl0_MainContentsParentPH_MainContentsPH__ctl0_BookingContact_PhoneAtDest_Number")

	private WebElement phoneatdestination;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingContact_PhoneAtDest_Number")

	private WebElement Alanitaphoneatdestination;

	@FindBy(id = "_ctl0_MainContentsPH__ctl0_BookingContact_PhoneAtDest_Number")

	private WebElement Faregeekphoneatdestination;

	 @FindBy(id="_ctl0_MainContentsPH__ctl0_ContinueLNK")
	
	 private WebElement continuebtn;

	public void FaredepotToTitle(String frstnme, String mdlnme, String lstnme, String fstnm, String mdlnm,
			String lastnm, String firstnm, String midlnam, String lastnam, String eml, String coneml, String phone,
			String phn)

	{

		try

		{

			Select select = new Select(selctitle);

			select.selectByVisibleText("Mr");

			Firstnme.sendKeys(frstnme);

			Middlenme.sendKeys(mdlnme);

			lastnme.sendKeys(lstnme);

			Select select1 = new Select(selctmonth);

			select1.selectByVisibleText("Mar");

			Select select2 = new Select(selctday);

			select2.selectByVisibleText("1");

			Select select3 = new Select(selctyear);

			select3.selectByVisibleText("1983");

			Continue.click();

			Thread.sleep(3000);
			email.sendKeys(eml);

			cnfmemail.sendKeys(coneml);

			phoneno.sendKeys(phone);

			Continue.click();

		}

		catch (Exception e)

		{

			e.printStackTrace();

		}

	}

	public void AlanitaBookingToTitle(String frstnme, String mdlnme, String lstnme, String fstnm, String mdlnm,
			String lastnm, String firstnm, String midlnam, String lastnam, String eml, String coneml, String phone,
			String phn)

	{

		try

		{

			Select select = new Select(Alanitaselctitle);

			select.selectByVisibleText("Mr");

			AlanitaFirstnme.sendKeys(frstnme);

			AlanitaMiddlenme.sendKeys(mdlnme);

			Alanitalastnme.sendKeys(lstnme);

			Select select1 = new Select(Alanitaselctmonth);

			select1.selectByVisibleText("Mar");

			Select select2 = new Select(Alanitaselctday);

			select2.selectByVisibleText("1");

			Select select3 = new Select(Alanitaselctyear);

			select3.selectByVisibleText("1983");

			Continue.click();

			Thread.sleep(3000);
			Alanitaemail.sendKeys(eml);

			Alanitacnfmemail.sendKeys(coneml);

			Alanitaphoneno.sendKeys(phone);

			Alanitaphoneatdestination.sendKeys(phn);

			Continue.click();

		}

		catch (Exception e)

		{

			e.printStackTrace();

		}

	}

	public void FaregeekToTitle(String frstnme, String mdlnme, String lstnme, String fstnm, String mdlnm, String lastnm,
			String firstnm, String midlnam, String lastnam, String eml, String coneml, String phone, String phn)

	{

		try

		{

			Select select = new Select(Faregeekselcttitle);

			select.selectByVisibleText("Mr");
			
			Select select4 = new Select(Gender);

			select4.selectByVisibleText("Male");

			FaregeekFirstnme.sendKeys(frstnme);
			
			
			FaregeekMiddlenme.sendKeys(mdlnme);

			Faregeeklastnme.sendKeys(lstnme);

			Select select1 = new Select(Faregeekselctmonth);

			select1.selectByVisibleText("Mar");

			Select select2 = new Select(Faregeekselctday);

			select2.selectByVisibleText("1");

			Select select3 = new Select(Faregeekselctyear);

			select3.selectByVisibleText("1983");

			//Continue.click();

			Thread.sleep(3000);
			
			Faregeekphoneno.sendKeys(phone);
			Faregeekemail.sendKeys(eml);

			Faregeekcnfmemail.sendKeys(coneml);

			

			//continuebtn.click();

		}

		catch (Exception e)

		{

			e.printStackTrace();

		}

	}

	public boolean isDisplayed() {

		return false;

	}

}
