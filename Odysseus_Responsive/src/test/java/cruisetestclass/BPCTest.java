package cruisetestclass;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.experitest.client.Client;

import PageObject.BPCCruiseBookingpge;
import PageObject.BPCCruisepurchasepge;
import PageObject.Bookingpgeobjct;
import PageObject.Purchasepgeobjct;
import Utility.LocalAirUtils;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

public class BPCTest {
	
	DesiredCapabilities dc = new DesiredCapabilities();
	BPCCruisepurchasepge crspurchpge;
	BPCCruiseBookingpge crsbkngpge;

	String Log = "E:\\Automation\\Result-log\\BPC.txt";


	Reporter report = new Reporter();

	private String reportDirectory = "reports";
	private String reportFormat = "xml";
	private String testName = "Faredepot(B2C)";
	static WebDriver wd;
	public IOSDriver<IOSElement> driver;
	public RemoteWebDriver driver2;

	public Client client = null;

	int scrollTimeout = 500;
	int header = 98;
	int footer = 0;
	float dpr = 2;

	String Native = "NATIVE_APP";
	String Webview = "WEBVIEW_1";

	public boolean implicitwait(long time)

	{

		try

		{

			driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);

			System.out.println("Waited for" + time + "sec implicitly.");

		} catch (Exception e)

		{

			System.out.println(e.getMessage());

		}

		return true;

	}

	@BeforeTest
    public void baseClass() throws InterruptedException, IOException

	{

		try {
			dc.setCapability("deviceName", "iPhone 6+");
			dc.setCapability(MobileCapabilityType.UDID, "c0009b556ca641c9f3f9932c327180bd5f671680");

			dc.setCapability("platformName", "iOS");
			dc.setCapability("platformVersion", "10.3.3");
			// dc.setCapability("bundleId", "com.apple.mobilesafari");

			// dc.setCapability(MobileCapabilityType.APP, "Safari");
			dc.setBrowserName(MobileBrowserType.SAFARI);
			// driver = new IOSDriver<IOSElement>(new
			// URL("http://0.0.0.0:8888/wd/hub"), dc);

//			 Earl's PC
			 driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.75:8888/wd/hub"), dc);

			// Rutuja's PC
//			driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.131:4723/wd/hub"), dc);
			 
			//Chintan's PC
//			 driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.55:4723/wd/hub"), dc);
		
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			/*airbkpge = PageFactory.initElements(driver, Bookingpgeobjct.class);

			faregkpurchse = PageFactory.initElements(driver, Purchasepgeobjct.class);
*/
			dc.setCapability("reportDirectory", reportDirectory); //
			dc.setCapability("reportFormat", reportFormat);
			dc.setCapability("testName", testName);
			dc.setCapability("autoAcceptAlerts", true);
			dc.setCapability("showIOSLog", true);

			
			// driver = new IOSDriver<IOSElement>(new
			// URL("http://localhost:4724/wd/hub"), dc);
			
			/*String text =  driver.getContext();
			  System.out.println(text);
			if (driver.findElement(By.xpath("//*[@text='Submit']")).isDisplayed()){
				

                driver.findElement(By.xpath("//*[@text='Submit']")).click();
                
                driver.context(Webview);
                
                driver.get("https://book.bestpricecruises.com/");
			}
			else{*/
			
			driver.get("https://book.bestpricecruises.com/");
			//}
			//driver.context(Webview);
			//Save to console
	        /* String testResultFile="E:\\Automation\\ConsoleError\\BPCCruisebookingError.txt";
	         File file = new File(testResultFile);  
	         FileOutputStream fis = new FileOutputStream(file);  
	         PrintStream out = new PrintStream(fis);  
	         System.setOut(out); 
	                    
	         Thread.sleep(1000);*/
/*	         
	         String myArg1 = "D:\\Ajit\\Script_SS\\BPC\\1_Searchpage.png";
	         String myArg2 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\1_Searchpage.png";
	         String myCommand = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
	         String Output = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\1_Searchpage.png";
	          
	         ProcessBuilder pb = new ProcessBuilder(myCommand, myArg1, myArg2, Output, Log);
	         pb.directory(new File("D:\\Ajit\\Script_SS"));
	         Process p = pb.start();
	         
	         System.out.println("" +p);
	         
	         System.out.println("\n");
		     System.out.println("Searchpage Logs..");
		     System.out.println("\n");
		     ExtractJSLogs();
*/	         
	         	         
	         crsbkngpge = PageFactory.initElements(driver, BPCCruiseBookingpge.class);
	         crspurchpge = PageFactory.initElements(driver, BPCCruisepurchasepge.class);
		}
	              
	     
	 
	     
		
		catch (Exception e) {
			e.getMessage();
		}
		
	}


	@Test(dataProvider = "Authentications")
	public void BPC_data(String FirstName_Of_Guest1, String MiddleName_Of_Guest1, String LastName_Of_Guest1,
			String Address, String CityName, String PinCode, String EmailAddress, String Phone,
			String Firstname_of_Guest2, String Middlename_of_Guest2, String Lastname_of_Guest2) throws Exception {

		try {
			// For web site and booking details
			Reporter.log("Website Name:- BPC", true);
			Thread.sleep(200);

			crsbkngpge.BookingToTittle();

			

			

	
			

			// For verify error message on result page
			try {
				if (driver.findElement(By.cssSelector("#MainForm > table > tbody > tr:nth-child(2) > td"))
						.isDisplayed()) {
					String err = driver.findElement(By.cssSelector("#MainForm > table > tbody > tr:nth-child(2) > td"))
							.getText();
					Thread.sleep(300);
					Reporter.log("Error message: " + err, true);
					Thread.sleep(1000);
					/*
					 * final Screenshot screenshot87 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image87 = screenshot87.getImage();
					 * ImageIO.write(image87, "PNG", new File(
					 * "D:\\Ajit\\Script_SS\\BPCError\\04_Erroronresultpage.png"
					 * ));
					 */
					Assert.assertFalse(false, "FAIL");
					AssertJUnit.assertTrue("Error on result page..: " + err, crsbkngpge.isDisplayed());
					return;
				} /*else {
					driver.navigate().refresh();
				}*/
			} catch (Exception e) {
				e.getMessage();
			}
			/*
			 * Thread.sleep(300); final Screenshot screenshot4 = new
			 * AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image4 = screenshot4.getImage();
			 * ImageIO.write(image4, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\2_Resultpage.png"));
			 * Thread.sleep(2000);
			 */
			/*String myArg1 = "D:\\Ajit\\Script_SS\\BPC\\2_Resultpage.png";
			String myArg2 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\2_Resultpage.png";
			String myCommand = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\2_Resultpage.png";

			ProcessBuilder pb = new ProcessBuilder(myCommand, myArg1, myArg2, Output, Log);
			pb.directory(new File("D:\\Ajit\\Script_SS"));
			Process p = pb.start();

			System.out.println("" + p);*/

			Thread.sleep(10000);
			
			WebDriver driver1 = new Augmenter().augment(driver);
			File file1  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file1, new File("E:\\Automation\\BPC_Test\\ResultPage.png"));
			
			// For Image comparison
						String actual1 = "E:\\Automation\\BPC_Test\\ResultPage.png";
						String reference1 = "E:\\Automation\\Reference images\\BPC\\ResultPage.png";
						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output1 = "E:\\Automation\\Difference of Images\\BPC\\Difference_ResultPage.png";

						ProcessBuilder pb1 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output1, actual1, reference1, Log);
						
						pb1.directory(new File("E:\\node_modules\\.bin"));
						pb1.start();

						
						
						Thread.sleep(5000);

		} catch (Exception e) {
			System.out.println("\n");
			System.out.println("Time out or Invalid search criteria on resultpage Logs..");
			System.out.println("\n");
			//ExtractJSLogs();
			/*
			 * final Screenshot screenshot33 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image33 = screenshot33.getImage();
			 * ImageIO.write(image33, "PNG", new File(
			 * "D:\\Ajit\\Script_SS\\BPCError\\1_TimeoutORinvalidsearchdetails.png"
			 * ));
			 */
			Assert.assertFalse(false, "FAIL");
			Reporter.log("Time out or Invalid search criteria on resultpage..", true);
			AssertJUnit.assertTrue("Time out or Invalid search criteria on resultpage...", crsbkngpge.isDisplayed());
			throw (e);
		}

		driver.navigate().refresh();
		driver.get("https://book.bestpricecruises.com/web/cruises/results.aspx?showtrace=true");
		Thread.sleep(1000);
		
		// For Select cruise from result page
		try {
			driver.findElement(By.xpath("//*[@id='sr3']/div[2]/div[3]/div[2]/button[2]")).click();

		} catch (Exception e) {

			System.out.println("Cruise not available..");
			System.out.println("\n");
			System.out.println("Cruise not available on resultpage Logs..");
			System.out.println("\n");
//			ExtractJSLogs();
			/*
			 * final Screenshot screenshot5 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image5 = screenshot5.getImage();
			 * ImageIO.write(image5, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPCError\\2_Cruisenotavailable.png"));
			 */
			Assert.assertFalse(false, "FAIL");
			Reporter.log("Cruise not available..", true);
			AssertJUnit.assertTrue("Cruise not available...", crsbkngpge.isDisplayed());
			throw (e);
		}
		long start = System.currentTimeMillis();

		Thread.sleep(1100);

		long finish = System.currentTimeMillis();
		long totalTime = finish - start;
		Reporter.log("Total Time for result page to details page load(Milisec) - " + totalTime, true);
		Thread.sleep(1000);

		/*for (int i = driver.getWindowHandles().size() - 1; i > 0; i--) {

			String winHandle = driver.getWindowHandles().toArray()[i].toString();

			driver.switchTo().window(winHandle);

			driver.navigate().refresh();

			Thread.sleep(3000);

			driver.switchTo().frame(driver.findElement(By.tagName("iframe")));

			if (driver.findElement(By.xpath("//*[@id='PopUp']/div[2]/div/div[7]/div[3]/button")).isDisplayed()) {

				driver.findElement(By.xpath("//*[@id='PopUp']/div[2]/div/div[7]/div[3]/button")).click(); // Handle
																											// popup
																											// on
																											// details
																											// page
				Thread.sleep(100);

			}
		
			driver.switchTo().defaultContent();
			Thread.sleep(3000);*/
		
			
			Thread.sleep(1000);
			
			WebDriver driver1 = new Augmenter().augment(driver);
			File file2  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file2, new File("E:\\Automation\\BPC_Test\\Cruisedetailspage.png"));
			
			// For Image comparison
						String actual2 = "E:\\Automation\\BPC_Test\\Cruisedetailspage.png";
						String reference2 = "E:\\Automation\\Reference images\\BPC\\Cruisedetailspage.png";
						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output2 = "E:\\Automation\\Difference of Images\\BPC\\Difference_Cruisedetailspage.png";

						ProcessBuilder pb2 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output2, actual2, reference2, Log);
						
						pb2.directory(new File("E:\\node_modules\\.bin"));
						pb2.start();

						
						
						Thread.sleep(5000);
			/*
			 * final Screenshot screenshot6 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image6 = screenshot6.getImage();
			 * ImageIO.write(image6, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\3_Cruisedetailspage.png"));
			 * Thread.sleep(1500);
			 */
			/*String myArg1 = "D:\\Ajit\\Script_SS\\BPC\\3_Cruisedetailspage.png";
			String myArg2 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\3_Cruisedetailspage.png";
			String myCommand = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\3_Cruisedetailspage.png";

			ProcessBuilder pb = new ProcessBuilder(myCommand, myArg1, myArg2, Output, Log);
			pb.directory(new File("D:\\Ajit\\Script_SS"));
			Process p = pb.start();

			System.out.println("" + p);*/

			System.out.println("Cruise selected successfully..");

			Thread.sleep(4000);

			// For the Cruise details are print on HTML report
			
			
			
			String CruiseLine = driver
					.findElement(By.xpath("//td[contains(text(), 'Cruise Line:')]/following::td[contains(@class, 'sdInfo')]")).getText();
			Reporter.log("Cruise Line:- " + CruiseLine, true);
			Thread.sleep(500);
			String shp = driver
					.findElement(By.xpath("//td[contains(text(), 'Ship:')]/following::td[contains(@class, 'sdInfo')]"))
					.getText();
			Reporter.log("Ship: " + shp, true);
			Thread.sleep(800);
			String Length = driver
					.findElement(By.xpath("//td[contains(text(), 'Length:')]/following::td[contains(@class, 'sdInfo')]"))
					.getText();
			Reporter.log("Length:- " + Length, true);
			Thread.sleep(500);
			String Itinerary = driver
					.findElement(By.xpath("//td[contains(text(), 'Itinerary:')]/following::td[contains(@class, 'sdInfo')]"))
					.getText();
			Reporter.log("Itinerary:- " + Itinerary, true);
			Thread.sleep(100);
			String Departing = driver
					.findElement(By.xpath("//td[contains(text(), 'Departing:')]/following::td[contains(@class, 'sdInfo')]"))
					.getText();
			Reporter.log("Departing:- " + Departing, true);
			String Returning = driver
					.findElement(By.xpath("//td[contains(text(), 'Returning:')]/following::td[contains(@class, 'sdInfo')]")).getText();
			Reporter.log("Returning:- " + Returning, true);
			Thread.sleep(400);

			// For the Cruise details page
			try {

				
				driver.findElement(By.id("_ctl0:MainContentsPH:_ctl0:_ctl1_GuestAge_1")).sendKeys("10");
				Thread.sleep(300);
				
				driver.findElement(By.id("_ctl0:MainContentsPH:_ctl0:_ctl1_GuestAge_2")).sendKeys("67");
				Thread.sleep(300);
				;
				Select select1 = new Select(driver.findElement(By.id("_ctl0_MainContentsPH__ctl0__ctl1_ResidentState")));
				select1.selectByVisibleText("California (CA)");
				Thread.sleep(500);
				driver.findElement(By.xpath("//*[@id='_ctl0_MainContentsPH__ctl0__ctl1_CategoryLNK']")).sendKeys(Keys.ENTER);

				long start12 = System.currentTimeMillis();

				


				long finish1 = System.currentTimeMillis();
				long totalTime1 = finish1 - start12;
				Reporter.log("Total Time for details page to category page load(Milisec) - " + totalTime1, true);
				Thread.sleep(2000);
			} 
			
			
			
			catch (Exception e) {
				System.out.println("\n");
				System.out.println("Invalid guest information on cruise detailspage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot8 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image8 = screenshot8.getImage();
				 * ImageIO.write(image8, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\3_InvalidGuestinformation.png"
				 * ));
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Invalid guest information on detailspage...", true);
				AssertJUnit.assertTrue("Invalid guest information on detailspage...", crsbkngpge.isDisplayed());
				throw (e);
			}

			// For select category from the category page
			try {
				/*
				 * driver.findElement(By.id("showIn")).click();
				 * Thread.sleep(1000);
				 * driver.findElement(By.id("showOut")).click();
				 * Thread.sleep(3000);
				 */

				Thread.sleep(10000);
				
				driver1 = new Augmenter().augment(driver);
				File file3  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(file3, new File("E:\\Automation\\BPC_Test\\Categorypage.png"));
				
				// For Image comparison
							String actual3 = "E:\\Automation\\BPC_Test\\Categorypage.png";
							String reference3 = "E:\\Automation\\Reference images\\BPC\\Categorypage.png";
							//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
							String Output3 = "E:\\Automation\\Difference of Images\\BPC\\Difference_Categorypage.png";

							ProcessBuilder pb3 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output3, actual3, reference3, Log);
							
							pb3.directory(new File("E:\\node_modules\\.bin"));
							pb3.start();

							
							
				/*
				 * final Screenshot screenshot9 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image9 = screenshot9.getImage();
				 * ImageIO.write(image9, "PNG", new
				 * File("D:\\Ajit\\Script_SS\\BPC\\4_Categorypage.png"));
				 * Thread.sleep(4000);
				 */
				/*String myArg3 = "D:\\Ajit\\Script_SS\\BPC\\4_Categorypage.png";
				String myArg4 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\4_Categorypage.png";
				String myCommand1 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
				String Output1 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\4_Categorypage.png";

				ProcessBuilder pb1 = new ProcessBuilder(myCommand1, myArg3, myArg4, Output1, Log);
				pb1.directory(new File("D:\\Ajit\\Script_SS"));
				Process p1 = pb1.start();

				System.out.println("" + p1);*/

				/*String expr1 = driver.findElement(By.id("TracerBlock")).getText();
				String proccessingloops = expr1.split("WEB-93")[58].split(":")[1].trim();
				Reporter.log("Category processing loop :- " + proccessingloops, true);
				Thread.sleep(500);
				String totalprocesstime = expr1.split("WEB-93")[69].split(":")[1].trim();
				Reporter.log("TotalProcessing Time on category page:- " + totalprocesstime, true);
				Thread.sleep(3000);*/
				
				
				
				
				Thread.sleep(10000);
				//For noting time from traces when showtrace=true is activated
				/*try
		        {
		        	//if(driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).isDisplayed())
		        		
		        	for(int i=1; i<15; i++){
							
							Boolean value = driver.findElement(By.id("TracerBlock")).isDisplayed();
									System.out.println(value);
							if (value == true){
							break;
							}
							else {
								driver.swipe(748, 1947, 748, 616, 288);
							}
							}
		        		//String ResultpageLoad = driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).getText();
		        		List<IOSElement> list = driver.findElements(By.id("TracerBlock"));
		        		int listcount = list.size();    
		        		@SuppressWarnings("rawtypes")
						List optionsList = null;
		        		String options[] = new String[listcount];

		        		for (int i=1; i<=listcount; i++) 
		        		{                     
		        		    options[i-1] = driver.findElement(By.id("TracerBlock")).getText();
		        		    //contains three words eg: Test Engineer Madhu               
		        		    String[] expectedOptions = options[i-1].split(" ");
		        		    //splitting based on the space           
		        		    // I hope the next line gives you the correct lastName
		        		    String lastName =expectedOptions[expectedOptions.length-1]; 
		        		    // lets store the last name in the array
		        		    options[i-1] = lastName;        
		        		 }         
		        		 optionsList = Arrays.asList(options);
		        		 
		        		 Reporter.log("Total Time for result page to checkout page load(Milisec) - " +optionsList, true);
		        		
		        		 Thread.sleep(1000);
		        		
		        }
		        catch(Exception e)
		        {
		        	e.getMessage();
		        }*/

				String bestprice = driver.findElement(By.xpath("//a[@class='btn btn-sm btn-default active' or @text='Best Deal']")).getText();
				driver.findElement(By.xpath("//a[@class='btn btn-sm btn-default active' or @text='Best Deal']")).click();

				System.out.println("Best Price applied"+ bestprice);
				WebElement viewcat = driver.findElement(By.xpath("//a[@class='btn btn-default btn-block catDescBtn' or @text=' View Category Info']"));
				viewcat.click();
				viewcat.getText();
				System.out.println("Category applied: " +viewcat);
				Thread.sleep(3000);
				System.out.println("\n");
				System.out.println("View category info on categorypage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				Thread.sleep(2000);

				
				Thread.sleep(5000);
				
				driver1 = new Augmenter().augment(driver);
				File file4  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(file4, new File("E:\\Automation\\BPC_Test\\Viewcategoryinfo.png"));
				
				// For Image comparison
							String actual4 = "E:\\Automation\\BPC_Test\\Viewcategoryinfo.png";
							String reference4 = "E:\\Automation\\Reference images\\BPC\\Viewcategoryinfo.png";
							//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
							String Output4 = "E:\\Automation\\Difference of Images\\BPC\\Difference_Viewcategoryinfo.png";

							ProcessBuilder pb4 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output4, actual4, reference4, Log);
							
							pb4.directory(new File("E:\\node_modules\\.bin"));
							pb4.start();
							
				/*
				 * final Screenshot screenshot99 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image99 = screenshot99.getImage();
				 * ImageIO.write(image99, "PNG", new
				 * File("D:\\Ajit\\Script_SS\\BPC\\5_Viewcategoryinfo.png"));
				 * Thread.sleep(1500);
				 */
				/*String myArg5 = "D:\\Ajit\\Script_SS\\BPC\\5_Viewcategoryinfo.png";
				String myArg6 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\5_Viewcategoryinfo.png";
				String myCommand2 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
				String Output2 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\5_Viewcategoryinfo.png";

				ProcessBuilder pb2 = new ProcessBuilder(myCommand2, myArg5, myArg6, Output2, Log);
				pb2.directory(new File("D:\\Ajit\\Script_SS"));
				Process p2 = pb2.start();

				System.out.println("" + p2);*/

				Thread.sleep(5000);
				driver.findElement(By.xpath("//a[@class='btn btn-default btn-block catDescBtn' or @text=' View Category Info']"))
						.click();
				Thread.sleep(8000);

				// For the Bonus offers
				if (driver
						.findElement(
								By.id("va_OBC_ctl0_MainContentsPH_Categories_Categories__ctl0__ctl0__ctl0_BestPricesRPT__ctl1_PromotionsUC_OBCredit__ctl0"))
						.isDisplayed()) {

					WebElement bonusoffr = driver.findElement(By.id(
							"va_OBC_ctl0_MainContentsPH_Categories_Categories__ctl0__ctl0__ctl0_BestPricesRPT__ctl1_PromotionsUC_OBCredit__ctl0"));
					bonusoffr.click();
					System.out.println("Bonus offers are available..");
					Reporter.log("Bonus offers are available..", true);
					Thread.sleep(3000);
					
					driver1 = new Augmenter().augment(driver);
					File file5  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(file5, new File("E:\\Automation\\BPC_Test\\BonusoffersAvailable.png"));
					
					// For Image comparison
								String actual5 = "E:\\Automation\\BPC_Test\\BonusoffersAvailable.png";
								String reference5 = "E:\\Automation\\Reference images\\BPC\\BonusoffersAvailable.png";
								//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
								String Output5 = "E:\\Automation\\Difference of Images\\BPC\\Difference_BonusoffersAvailable.png";

								ProcessBuilder pb5 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output5, actual5, reference5, Log);
								
								pb5.directory(new File("E:\\node_modules\\.bin"));
								pb5.start();
								
								
					/*File scr02 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
					org.codehaus.plexus.util.FileUtils.copyFile(scr02,
							new File("D:\\Ajit\\Script_SS\\BPC\\6_BonusoffersAvailable.png"));
					Thread.sleep(2000);
*/
					/*String myArg7 = "D:\\Ajit\\Script_SS\\BPC\\6_BonusoffersAvailable.png";
					String myArg8 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\6_BonusoffersAvailable.png";
					String myCommand3 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
					String Output3 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\6_BonusoffersAvailable.png";

					ProcessBuilder pb3 = new ProcessBuilder(myCommand3, myArg7, myArg8, Output3, Log);
					pb3.directory(new File("D:\\Ajit\\Script_SS"));
					Process p3 = pb3.start();

					System.out.println("" + p3);*/

					/*driver.findElement(By
							.xpath("//*[@id='full-terms_24403__ctl0_MainContentsPH_Categories_Categories__ctl0__ctl0__ctl0_BestPricesRPT__ctl1_PromotionsUC_OBCredit__ctl0']/div/div/div[1]/button/span"))
							.click();
					Thread.sleep(2000);*/
					System.out.println("\n");
					System.out.println("Bonus offers on categorypage Logs..");
					System.out.println("\n");
//					ExtractJSLogs();
					Thread.sleep(4000);
				} else {
					System.out.println("Bonus offers are not available..");
					Reporter.log("Bonus offers not are available..", true);
					System.out.println("\n");
					System.out.println("Bonus offers are not available on categorypage Logs..");
					System.out.println("\n");
//					ExtractJSLogs();
					Thread.sleep(1000);

					/*
					 * final Screenshot screenshot93 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image93 = screenshot93.getImage();
					 * ImageIO.write(image93, "PNG", new File(
					 * "D:\\Ajit\\Script_SS\\BPC\\7_BonusoffersNotavailable.png"
					 * )); Thread.sleep(1000);
					 */
				}

				try {
					// For price check on the category page
					if (driver
							.findElement(By.xpath("//div[@class='col-xs-12 catFCrvp']//following::span[@class='catFCfinCost']")).isDisplayed()) {
						String pricesoncat = driver
								.findElement(By.xpath("//div[@class='col-xs-12 catFCrvp']//following::span[@class='catFCfinCost']")).getText();
						System.out.println("Prices on category page is:- " + pricesoncat);
						Reporter.log("Prices on category page is:- " + pricesoncat, true);
						Thread.sleep(4000);
					}
				} catch (Exception e) {
					/*
					 * final Screenshot screenshot80 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image80 = screenshot80.getImage();
					 * ImageIO.write(image80, "PNG", new File(
					 * "D:\\Ajit\\Script_SS\\BPCError\\4_Pricenotavailableoncategorypage.png"
					 * ));
					 */
					Assert.assertFalse(false, "FAIL");
					Reporter.log("Price not availabe on category page...", true);
					AssertJUnit.assertTrue("Price not availabe on category page...", crsbkngpge.isDisplayed());
					throw (e);
				}

				driver.findElement(By.xpath("//div[@class='col-xs-12 catFCsub']//following::a[@class='btn btn-default btn-block']")).sendKeys(Keys.ENTER);
				Thread.sleep(2000);

				/*// For handle pop up on category page
				if (driver.findElement(By.xpath("//button[@class='btn btn-block btn-default noteBtn']"))
						.isDisplayed()) {

					driver.findElement(By.xpath("//button[@class='btn btn-block btn-default noteBtn']")).click(); // Handle
																													// popup
																													// on
																													// details
																													// page
					Thread.sleep(100);

				}

				Thread.sleep(3000);*/

			} catch (Exception e) {
				System.out.println("Category not available..");
				System.out.println("\n");
				System.out.println("Category not available on categorypage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot10 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image10 = screenshot10.getImage();
				 * ImageIO.write(image10, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\5_Categorynotavailable.png"))
				 * ;
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Category not available..", true);
				AssertJUnit.assertTrue("Category not available...", crsbkngpge.isDisplayed());
				throw (e);
			}

			System.out.println("Category selected successfully..");

			Thread.sleep(5000);

			// For select cabin from the cabin selection page
			try {
				// For check API, Office id And Test environment on cabin page
				// (If cabin available then put below code on before print
				// category success message)
				/*String expr = driver.findElement(By.id("TracerBlock")).getText();
				String api = expr.split("OdysseyGateway")[2].split(":")[1].trim();
				// List<String> items =
				// Arrays.asList(expr.split("$($('#TracerBlock')[0].innerHTML.split('OdysseyGateway')[2])[0].nodeValue.toString().split(':')[1]"));
				Reporter.log("API :- " + api, true);
				Thread.sleep(500);
				String envrnmnt = expr.split("OdysseyGateway")[2].split(":")[2].trim();
				Reporter.log("Environment :- " + envrnmnt, true);
				Thread.sleep(500);
				String officeid = expr.split("OdysseyGateway")[2].split(":")[3].trim();
				Reporter.log("Office Id :- " + officeid, true);
				Thread.sleep(3000);*/

				if (driver.findElement(By.xpath("//td[@class='cabinBtnTD']//following::button[@class='btn btn-default btn-block']")).isDisplayed()) {
					driver.findElement(By.xpath("//td[@class='cabinBtnTD']//following::button[@class='btn btn-default btn-block']")).click();

					long start13 = System.currentTimeMillis();

					Thread.sleep(2050);

					long finish1 = System.currentTimeMillis();
					long totalTime1 = finish1 - start13;
					Reporter.log("Total Time for category page to cabin selection page load(Milisec) - " + totalTime1, true);
					Thread.sleep(2000);

					Thread.sleep(5000);
					
					driver1 = new Augmenter().augment(driver);
					File file6  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(file6, new File("E:\\Automation\\BPC_Test\\Cabinselectionpage.png"));
					
					// For Image comparison
								String actual6 = "E:\\Automation\\BPC_Test\\Cabinselectionpage.png";
								String reference6 = "E:\\Automation\\Reference images\\BPC\\Cabinselectionpage.png";
								//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
								String Output6 = "E:\\Automation\\Difference of Images\\BPC\\Difference_Cabinselectionpage.png";

								ProcessBuilder pb6 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output6, actual6, reference6, Log);
								
								pb6.directory(new File("E:\\node_modules\\.bin"));
								pb6.start();
					/*
					 * final Screenshot screenshot11 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image11 = screenshot11.getImage();
					 * ImageIO.write(image11, "PNG", new
					 * File("D:\\Ajit\\Script_SS\\BPC\\8_Cabinselectionpage.png"
					 * )); Thread.sleep(2000);
					 */
					/*String myArg7 = "D:\\Ajit\\Script_SS\\BPC\\8_Cabinselectionpage.png";
					String myArg8 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\8_Cabinselectionpage.png";
					String myCommand3 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
					String Output3 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\8_Cabinselectionpage.png";

					ProcessBuilder pb3 = new ProcessBuilder(myCommand3, myArg7, myArg8, Output3, Log);
					pb3.directory(new File("D:\\Ajit\\Script_SS"));
					Process p3 = pb3.start();

					System.out.println("" + p3);*/

				}
			} catch (Exception e) {
				System.out.println("Cabin not available..");
				System.out.println("\n");
				System.out.println("Cabin not available on select cabinpage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot13 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image13 = screenshot13.getImage();
				 * ImageIO.write(image13, "PNG", new
				 * File("D:\\Ajit\\Script_SS\\BPCError\\6_Cabinnotavailable.png"
				 * ));
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Cabin not available..", true);
				// AssertJUnit.assertTrue("Cabin not available...",
				// crsbkngpge.isDisplayed());
				// throw(e);
			}

			System.out.println("\n");
			System.out.println("Continue without signing page Logs..");
			System.out.println("\n");
//			ExtractJSLogs();
			Thread.sleep(1000);

			// For handle unnecessary pop up
			try {
				if (driver
						.findElement(By.xpath("//div[@class='col-sm-5']//following::a[@class='btn btn-block btn-default noteBtn']")).isDisplayed()) {
					driver.findElement(By.xpath("//div[@class='col-sm-5']//following::a[@class='btn btn-block btn-default noteBtn']")).click();
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				e.getMessage();
			}
			/*
			 * final Screenshot screenshot12 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image12 = screenshot12.getImage();
			 * ImageIO.write(image12, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\9_Continuewithoutsigningpage.png"
			 * )); Thread.sleep(2000);
			 */
			/*String myArg7 = "D:\\Ajit\\Script_SS\\BPC\\9_Continuewithoutsigningpage.png";
			String myArg8 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\9_Continuewithoutsigningpage.png";
			String myCommand3 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output3 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\9_Continuewithoutsigningpage.png";

			ProcessBuilder pb3 = new ProcessBuilder(myCommand3, myArg7, myArg8, Output3, Log);
			pb3.directory(new File("D:\\Ajit\\Script_SS"));
			Process p3 = pb3.start();

			System.out.println("" + p3);*/

			System.out.println("Cabin selected successfully..");

			Thread.sleep(4000);

			// For Continue without signing In
			
			driver.swipe(748, 1947, 748, 616, 288);
      	   	driver.swipe(748, 1947, 748, 616, 288);
      	   	driver.swipe(748, 1947, 748, 616, 288);
      	   	driver.swipe(748, 1947, 748, 616, 288);	
      	   	driver.swipe(748, 1947, 748, 616, 288);	
      	try {
				if (driver.findElement(By.xpath("//span[contains(@class,'visible-xs')]")).isDisplayed()) {
					driver.findElement(By.xpath("//span[contains(@class,'visible-xs')]")).click();
					Thread.sleep(1000);
				}

			} catch (Exception e) {
				System.out.println("Continue without signing in button is not click or Not available..");
				System.out.println("\n");
				System.out.println("Continue without signing on statementpage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot33 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image33 = screenshot33.getImage();
				 * ImageIO.write(image33, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\7_Continuewithoutsigningnotclickonstatementpage.png"
				 * ));
				 * 
				 */ Assert.assertFalse(false, "FAIL");
				Reporter.log("Continue without signing in button is not click or Not available...", true);
				//AssertJUnit.assertTrue("Continue without signing in button is not click or Not available....",
				//		crsbkngpge.isDisplayed());
				throw (e);
			}

			long start15 = System.currentTimeMillis();
			Thread.sleep(1002);
			long finish3 = System.currentTimeMillis();
			long totalTime3 = finish3 - start15;
			Reporter.log("Total Time for cabin selection page to purchase page load(Milisec) - " + totalTime3, true);
			
			Thread.sleep(5000);
			
			driver1 = new Augmenter().augment(driver);
			File file7  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file7, new File("E:\\Automation\\BPC_Test\\Purchasepage.png"));
			
			// For Image comparison
						String actual7 = "E:\\Automation\\BPC_Test\\Purchasepage.png";
						String reference7 = "E:\\Automation\\Reference images\\BPC\\Purchasepage.png";
						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output7 = "E:\\Automation\\Difference of Images\\BPC\\Difference_Purchasepage.png";

						ProcessBuilder pb7 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output7, actual7, reference7, Log);
						
						pb7.directory(new File("E:\\node_modules\\.bin"));
						pb7.start();
			/*
			 * final Screenshot screenshot22 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image22 = screenshot22.getImage();
			 * ImageIO.write(image22, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\10_Purchasepage.png"));
			 * 
			 */ 
			System.out.println("Continue without signing in successfully..");

			Thread.sleep(4000);

			/*String myArg9 = "D:\\Ajit\\Script_SS\\BPC\\10_Purchasepage.png";
			String myArg10 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\10_Purchasepage.png";
			String myCommand4 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output4 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\10_Purchasepage.png";

			ProcessBuilder pb4 = new ProcessBuilder(myCommand4, myArg9, myArg10, Output4, Log);
			pb4.directory(new File("D:\\Ajit\\Script_SS"));
			Process p4 = pb4.start();

			System.out.println("" + p4);*/

			// For price on purchase page
			String pricesonpur = driver
					.findElement(
							By.xpath("//tr[@class='psGrTotal']//following::td[2]")).getText();
			System.out.println("Price on purchase page is:- " + pricesonpur);
			Reporter.log("Price on purchase page is:- " + pricesonpur, true);
			Thread.sleep(800);

			try {
				System.out.println("FirstName_Of_Guest1: " + FirstName_Of_Guest1);
				System.out.println("MiddleName_Of_Guest1: " + MiddleName_Of_Guest1);
				System.out.println("LastName_Of_Guest1: " + LastName_Of_Guest1);
				System.out.println("Address: " + Address);
				System.out.println("CityName: " + CityName);
				System.out.println("PinCode: " + PinCode);
				System.out.println("EmailAddress: " + EmailAddress);
				System.out.println("Phone: " + Phone);
				System.out.println("Firstname_of_Guest2: " + Firstname_of_Guest2);
				System.out.println("Middlename_of_Guest2: " + Middlename_of_Guest2);
				System.out.println("Lastname_of_Guest2: " + Lastname_of_Guest2);

				crspurchpge.PurchaseToTittle(FirstName_Of_Guest1, MiddleName_Of_Guest1, LastName_Of_Guest1, Address,
						CityName, PinCode, EmailAddress, Phone, Firstname_of_Guest2, Middlename_of_Guest2,
						Lastname_of_Guest2);

				 
			} catch (Exception e) {
				System.out.println(
						"Invalid guest information on purchase page, Please enter valid details for the required fields...");
				System.out.println("\n");
				System.out.println("Invalid guest information on purchasepage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot34 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image34 = screenshot34.getImage();
				 * ImageIO.write(image34, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\8_InvalidGuestinfoonPurchasepage.png"
				 * ));
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log(
						"Invalid guest information on purchase page, Please enter valid details for the required fields...");
				AssertJUnit.assertTrue(
						"Invalid guest information on purchase page, Please enter valid details for the required fields....",
						crsbkngpge.isDisplayed());
				throw (e);
			}
			long start16 = System.currentTimeMillis();
			Thread.sleep(2070);
			long finish03 = System.currentTimeMillis();
			long totalTime03 = finish03 - start16;
			Reporter.log("Total Time for purchase page to confirmation page load(Milisec) - " + totalTime03, true);
			
			Thread.sleep(10000);
			
			driver1 = new Augmenter().augment(driver);
			File file8  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file8, new File("E:\\Automation\\BPC_Test\\Confirmationpage.png"));
			
			// For Image comparison
						String actual8 = "E:\\Automation\\BPC_Test\\Confirmationpage.png";
						String reference8 = "E:\\Automation\\Reference images\\BPC\\Confirmationpage.png";
						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output8 = "E:\\Automation\\Difference of Images\\BPC\\Difference_Confirmationpage.png";

						ProcessBuilder pb8 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output8, actual8, reference8, Log);
						
						pb8.directory(new File("E:\\node_modules\\.bin"));
						pb8.start();
			/*
			 * final Screenshot screenshot44 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image44 = screenshot44.getImage();
			 * ImageIO.write(image44, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\11_Confirmationpage.png"));
			 * Thread.sleep(2000);
			 */
			/*String myArg11 = "D:\\Ajit\\Script_SS\\BPC\\11_Confirmationpage.png";
			String myArg12 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\11_Confirmationpage.png";
			String myCommand5 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output5 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\11_Confirmationpage.png";

			ProcessBuilder pb5 = new ProcessBuilder(myCommand5, myArg11, myArg12, Output5, Log);
			pb5.directory(new File("D:\\Ajit\\Script_SS"));
			Process p5 = pb5.start();

			System.out.println("" + p5);*/

			// For Price on confirmation page
			String price = driver
					.findElement(
							By.xpath("//tr[@class='psGrTotal']//following::td[@class='psmItem']//following-sibling::td")).getText();
			Reporter.log("Price on the passenger details page is:- " + price, true);
			Thread.sleep(3000);

			// For Insurance
			try {
				if (driver.findElement(By.xpath("//input[contains(@id, 'InsurranceCHK')]")).isDisplayed()) 
				{
					driver.findElement(
							By.xpath("//input[contains(@id, 'InsurranceCHK')]")).click();					Thread.sleep(1500);
					Reporter.log("Insurance applied successfully..", true);

					// For check insurance price
					String insurprice = driver
							.findElement(By.xpath("//td[@class='bkInsPr']//following::span[@class='priceUp']"))
							.getText();
					Reporter.log("Prices of After applied insurance on the passenger details page is:- " + insurprice, true);
					Thread.sleep(5000);
					
					driver1 = new Augmenter().augment(driver);
					File file9  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(file9, new File("E:\\Automation\\BPC_Test\\AppliedInsurance.png"));
					
					// For Image comparison
								String actual9 = "E:\\Automation\\BPC_Test\\AppliedInsurance.png";
								String reference9 = "E:\\Automation\\Reference images\\BPC\\AppliedInsurance.png";
								//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
								String Output9 = "E:\\Automation\\Difference of Images\\BPC\\Difference_AppliedInsurance.png";

								ProcessBuilder pb9 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output9, actual9, reference9, Log);
								
								pb9.directory(new File("E:\\node_modules\\.bin"));
								pb9.start();
					/*
					 * final Screenshot screenshot64 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image64 = screenshot64.getImage();
					 * ImageIO.write(image64, "PNG", new
					 * File("D:\\Ajit\\Script_SS\\BPC\\12_AppliedInsurance.png")
					 * ); Thread.sleep(2000);
					 */
					/*String myArg13 = "D:\\Ajit\\Script_SS\\BPC\\12_AppliedInsurance.png";
					String myArg14 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\12_AppliedInsurance.png";
					String myCommand6 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
					String Output6 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\12_AppliedInsurance.png";

					ProcessBuilder pb6 = new ProcessBuilder(myCommand6, myArg13, myArg14, Output6, Log);
					pb6.directory(new File("D:\\Ajit\\Script_SS"));
					Process p6 = pb6.start();

					System.out.println("" + p6);*/

					// Applied insurance price
					String appliedinsurprice = driver
							.findElement(By.xpath("//td[@class='pmtsAmt']")).getText();
					Reporter.log("Insurance price:- " + appliedinsurprice, true);
					Thread.sleep(1000);

					// For Remove insurance
					WebElement insurnc = driver.findElement(By.id("InsurranceCHK_NON"));
					insurnc.click();
					Thread.sleep(3000);
					driver.context(Native);

                    driver.findElement(By.xpath("//*[@text='OK']")).click();
                    
                    driver.context(Webview);
                    Thread.sleep(4000);

					Reporter.log("Insurance Removed successfully..", true);
					
					

					// For check insurance price
					String insprice = driver
							.findElement(By.xpath("//td[@class='bkInsPr']//following::span[@class='priceUp']"))
							.getText();
					Reporter.log("Prices of After removed insurance on the passenger details page is:- " + insprice, true);
					Thread.sleep(1000);
					
					Thread.sleep(5000);
					
					driver.findElement(By.id("InsurranceCHK_NON"));
					driver1 = new Augmenter().augment(driver);
					File file10  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(file10, new File("E:\\Automation\\BPC_Test\\RemovedInsurance.png"));
					
					// For Image comparison
								String actual10 = "E:\\Automation\\BPC_Test\\RemovedInsurance.png";
								String reference10 = "E:\\Automation\\Reference images\\BPC\\RemovedInsurance.png";
								//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
								String Output10 = "E:\\Automation\\Difference of Images\\BPC\\Difference_RemovedInsurance.png";

								ProcessBuilder pb10 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output10, actual10, reference10, Log);
								
								pb10.directory(new File("E:\\node_modules\\.bin"));
								pb10.start();
					/*
					 * final Screenshot screenshot66 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image66 = screenshot66.getImage();
					 * ImageIO.write(image66, "PNG", new
					 * File("D:\\Ajit\\Script_SS\\BPC\\13_RemovedInsurance.png")
					 * ); Thread.sleep(2000);
					 * 
					 */ /*String myArg15 = "D:\\Ajit\\Script_SS\\BPC\\13_RemovedInsurance.png";
					String myArg16 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\13_RemovedInsurance.png";
					String myCommand7 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
					String Output7 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\13_RemovedInsurance.png";

					ProcessBuilder pb7 = new ProcessBuilder(myCommand7, myArg15, myArg16, Output7, Log);
					pb7.directory(new File("D:\\Ajit\\Script_SS"));
					Process p7 = pb7.start();

					System.out.println("" + p7);*/

				}
			} catch (Exception e) {
				System.out.println("Insurance not applied/removed...");
				System.out.println("\n");
				System.out.println("Insurance not applied/removed on confirmation page...");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot72 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image72 = screenshot72.getImage();
				 * ImageIO.write(image72, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\9_InsurancenotAppliedOrRemoved.png"
				 * ));
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Insurance not applied/removed...", true);
				// AssertJUnit.assertTrue("Insurance not applied/removed....",
				// crsbkngpge.isDisplayed());
				// throw(e);
			}

			try {
				// For select terms and condition check box
				driver.findElement(By.id("AgreeTermsCON")).click();
				Thread.sleep(1000);

				// Click on Pay in full payment button
				driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_ContinueBTN")).click();
				long start17 = System.currentTimeMillis();
				Thread.sleep(1001);

				long finish30 = System.currentTimeMillis();
				long totalTime30 = finish30 - start17;
				Reporter.log("Total Time for confirmation page to payment page load(Milisec) - " + totalTime30, true);
				Thread.sleep(2000);
			} catch (Exception e) {

				System.out.println("Page navigate on invalid page Or Displaying Error message on confirmation page...");
				System.out.println("\n");
				System.out.println("Page navigate on invalid page Or Displaying Error message on confirmation page...");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot27 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image27 = screenshot27.getImage();
				 * ImageIO.write(image27, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\10_PageNavigateonInvalidPageOrErrormessaage.png"
				 * ));
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Page navigate on invalid page Or Displaying Error message on confirmation page...", true);
				AssertJUnit.assertTrue(
						"Page navigate on invalid page Or Displaying Error message on confirmation page....",
						crsbkngpge.isDisplayed());
				throw (e);

			}
			
			
			/*
			 * final Screenshot screenshot49 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image49 = screenshot49.getImage();
			 * ImageIO.write(image49, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\14_Paymentpage.png"));
			 * Thread.sleep(2000);
			 * 
			  String myArg15 = "D:\\Ajit\\Script_SS\\BPC\\14_Paymentpage.png";
			String myArg16 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\14_Paymentpage.png";
			String myCommand7 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output7 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\14_Paymentpage.png";

			ProcessBuilder pb7 = new ProcessBuilder(myCommand7, myArg15, myArg16, Output7, Log);
			pb7.directory(new File("D:\\Ajit\\Script_SS"));
			Process p7 = pb7.start();

			System.out.println("" + p7);*/

			String paymntprice = driver.findElement(By.id("PayPrice")).getText();
			String paymntprice1 = driver.findElement(By.xpath("//div[@class='col-sm-6']//following::label")).getText();
			Reporter.log("Price on payment page is:- " + paymntprice, true);
			Reporter.log("Price on payment page is:- " + paymntprice1, true);
			if (paymntprice.equals(price)) {

				System.out.println("Actual String: " + price);
				System.out.println("Passenger details page and Payment page's Price matched");
				Reporter.log("Passenger details page and Payment page's Price matched..", true);

				System.out.println("Actual price is:- " + price);
				Reporter.log("Actual price is:- " + price, true);
			} else {
				System.out.println("Passenger details page and Payment page's Price not match..");
				Reporter.log("Passenger details page and Payment page's Price not match..", true);
				System.out.println("Actual price is:- " + price);
				Reporter.log("Actual price is:- " + price, true);
			}

			Thread.sleep(1500);
			
			Thread.sleep(5000);
			
			driver1 = new Augmenter().augment(driver);
			File file11  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file11, new File("E:\\Automation\\BPC_Test\\Paymentpage.png"));
			
			// For Image comparison
						String actual11 = "E:\\Automation\\BPC_Test\\Paymentpage.png";
						String reference11 = "E:\\Automation\\Reference images\\BPC\\Paymentpage.png";
						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output11 = "E:\\Automation\\Difference of Images\\BPC\\Difference_Paymentpage.png";

						ProcessBuilder pb11 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output11, actual11, reference11, Log);
						
						pb11.directory(new File("E:\\node_modules\\.bin"));
						pb11.start();

			// For the compare logs
			/*String ConsleArgument = "D:\\Ajit\\Script_SS\\ConsoleError\\BPCCruisebookingError.txt";
			String ConsoleCommand = "D:\\Ajit\\Script_SS\\ConsoleApplication2.exe";
			String ConsoleOutput = "D:\\Ajit\\Script_SS\\BPCError\\UncaughtBPC.txt";

			ProcessBuilder pb05 = new ProcessBuilder(ConsoleCommand, ConsleArgument, ConsoleOutput);
			pb05.directory(new File("D:\\Ajit\\Script_SS"));
			Process p05 = pb05.start();

			System.out.println("" + p05);

			Thread.sleep(1000);*/
		}

	

	@DataProvider
	public String[][] Authentications() throws Exception {

		String[][] testObjArray = LocalAirUtils.getTableArray(
				"E:\\Automation\\Workspace\\Odysseus_Responsive\\src\\main\\java\\testData\\BPCData.xlsx", "Sheet1");
		return testObjArray;

	}

	@AfterClass
	public void closeBrowser() throws InterruptedException {

		/*
		 * if(driver!=null) { System.out.println("Closing the browser");
		 * driver.quit(); }
		 */
	}

}
