package cruisetestclass;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.experitest.client.Client;

import PageObject.BPCCruiseBookingpge;
import PageObject.BPCCruisepurchasepge;
import PageObject.Bookingpgeobjct;
import PageObject.Purchasepgeobjct;
import Utility.LocalAirUtils;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

public class CruiseFusionTest {
	
	DesiredCapabilities dc = new DesiredCapabilities();
	BPCCruisepurchasepge crspurchpge;
	BPCCruiseBookingpge crsbkngpge;

	String Log = "E:\\Automation\\Result-log\\CruiseFusion.txt";

	

	Reporter report = new Reporter();

	private String reportDirectory = "reports";
	private String reportFormat = "xml";
	private String testName = "Faredepot(B2C)";
	static WebDriver wd;
	public IOSDriver<IOSElement> driver;
	public RemoteWebDriver driver2;
	

	public Client client = null;

	int scrollTimeout = 500;
	int header = 98;
	int footer = 0;
	float dpr = 2;

	String Native = "NATIVE_APP";
	String Webview = "WEBVIEW_1";

	
	
	public boolean implicitwait(long time)

	{

		try

		{

			driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);

			System.out.println("Waited for" + time + "sec implicitly.");

		} catch (Exception e)

		{

			System.out.println(e.getMessage());

		}

		return true;

	}

	@BeforeTest
    public void baseClass() throws InterruptedException, IOException

	{

		try {
			dc.setCapability("deviceName", "iPhone 6+");
			dc.setCapability(MobileCapabilityType.UDID, "c0009b556ca641c9f3f9932c327180bd5f671680");

			dc.setCapability("platformName", "iOS");
			dc.setCapability("platformVersion", "10.3.3");
			// dc.setCapability("bundleId", "com.apple.mobilesafari");

			// dc.setCapability(MobileCapabilityType.APP, "Safari");
			dc.setBrowserName(MobileBrowserType.SAFARI);
			// driver = new IOSDriver<IOSElement>(new
			// URL("http://0.0.0.0:8888/wd/hub"), dc);

			// Earl's PC
			 driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.75:8888/wd/hub"), dc);

			// Rutuja's PC
//			driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.131:4723/wd/hub"), dc);
		
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			/*airbkpge = PageFactory.initElements(driver, Bookingpgeobjct.class);

			faregkpurchse = PageFactory.initElements(driver, Purchasepgeobjct.class);
*/
			dc.setCapability("reportDirectory", reportDirectory); //
			dc.setCapability("reportFormat", reportFormat);
			dc.setCapability("testName", testName);
			dc.setCapability("autoAcceptAlerts", true);
			dc.setCapability("showIOSLog", true);

			
			
			// driver = new IOSDriver<IOSElement>(new
			// URL("http://localhost:4724/wd/hub"), dc);
			
			/*String text =  driver.getContext();
			  System.out.println(text);
			if (driver.findElement(By.xpath("//*[@text='Submit']")).isDisplayed()){
				

                driver.findElement(By.xpath("//*[@text='Submit']")).click();
                
                driver.context(Webview);
                
                driver.get("https://book.bestpricecruises.com/");
			}
			else{*/
			/*if (driver.findElement(By.xpath("//*[@text=Submit]")).isDisplayed()){
				driver.context(Native);
				driver.findElement(By.xpath("//*[@text=Submit]")).click();
				driver.context(Webview);
			}*/
			
			//driver.switchTo().alert().accept();			
			driver.get("https://book.cruisefusion.com.au/web/cruises/details.aspx?pid=973116&showtrace=true");
			//}
			//driver.context(Webview);
			//Save all sysout and reporter log comments to console
	         /*String testResultFile="E:\\Automation\\ConsoleError\\CDORBookingError.txt";
	         File file = new File(testResultFile);  
	         FileOutputStream fis = new FileOutputStream(file);  
	         PrintStream out = new PrintStream(fis);  
	         System.setOut(out); */
	                    
	         Thread.sleep(1000);
/*	         
	         String myArg1 = "D:\\Ajit\\Script_SS\\BPC\\1_Searchpage.png";
	         String myArg2 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\1_Searchpage.png";
	         String myCommand = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
	         String Output = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\1_Searchpage.png";
	          
	         ProcessBuilder pb = new ProcessBuilder(myCommand, myArg1, myArg2, Output, Log);
	         pb.directory(new File("D:\\Ajit\\Script_SS"));
	         Process p = pb.start();
	         
	         System.out.println("" +p);
	         
	         System.out.println("\n");
		     System.out.println("Searchpage Logs..");
		     System.out.println("\n");
		     ExtractJSLogs();
*/	         
	         	         
	        // crsbkngpge = PageFactory.initElements(driver, BPCCruiseBookingpge.class);
	         crspurchpge = PageFactory.initElements(driver, BPCCruisepurchasepge.class);
		}
	              
	     
	 
	     
		
		catch (Exception e) {
			e.getMessage();
		}
		
	}


	@Test(dataProvider = "Authentications")
	public void BPC_data(String FirstName_Of_Guest1, String MiddleName_Of_Guest1, String LastName_Of_Guest1,
			String Address, String CityName, String PinCode, String EmailAddress, String Phone,
			String Firstname_of_Guest2, String Middlename_of_Guest2, String Lastname_of_Guest2) throws Exception {

		try {
			// For web site and booking details
			Reporter.log("Website Name:- CruiseFusion", true);

			//crsbkngpge.BookingToTittle();

			long start = System.currentTimeMillis();

			Thread.sleep(3900);


			Thread.sleep(800);

			long finish = System.currentTimeMillis();
			long totalTime = finish - start;
			Reporter.log("Total Time for Guest info page to load(Milisec) - " + totalTime, true);
			Thread.sleep(1000);

			// For verify error message on result page
			/*try {
				if (driver.findElement(By.cssSelector("#MainForm > table > tbody > tr:nth-child(2) > td"))
						.isDisplayed()) {
					String err = driver.findElement(By.cssSelector("#MainForm > table > tbody > tr:nth-child(2) > td"))
							.getText();
					Thread.sleep(300);
					Reporter.log("Error message: " + err, true);
					Thread.sleep(1000);
					
					 * final Screenshot screenshot87 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image87 = screenshot87.getImage();
					 * ImageIO.write(image87, "PNG", new File(
					 * "D:\\Ajit\\Script_SS\\BPCError\\04_Erroronresultpage.png"
					 * ));
					 
					Assert.assertFalse(false, "FAIL");
					AssertJUnit.assertTrue("Error on result page..: " + err, crsbkngpge.isDisplayed());
					return;
				} else {
					driver.navigate().refresh();
				}
			} catch (Exception e) {
				e.getMessage();
			}*/
			/*
			 * Thread.sleep(300); final Screenshot screenshot4 = new
			 * AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image4 = screenshot4.getImage();
			 * ImageIO.write(image4, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\2_Resultpage.png"));
			 * Thread.sleep(2000);
			 */
			/*String myArg1 = "D:\\Ajit\\Script_SS\\BPC\\2_Resultpage.png";
			String myArg2 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\2_Resultpage.png";
			String myCommand = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\2_Resultpage.png";

			ProcessBuilder pb = new ProcessBuilder(myCommand, myArg1, myArg2, Output, Log);
			pb.directory(new File("D:\\Ajit\\Script_SS"));
			Process p = pb.start();

			System.out.println("" + p);*/

			Thread.sleep(6000);

		} catch (Exception e) {
			
			/*
			 * final Screenshot screenshot33 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image33 = screenshot33.getImage();
			 * ImageIO.write(image33, "PNG", new File(
			 * "D:\\Ajit\\Script_SS\\BPCError\\1_TimeoutORinvalidsearchdetails.png"
			 * ));
			 */
			Assert.assertFalse(false, "FAIL");
			Reporter.log("Time out or Invalid search criteria on resultpage..", true);
			AssertJUnit.assertTrue("Time out or Invalid search criteria on resultpage...", crsbkngpge.isDisplayed());
			throw (e);
		}

		//driver.navigate().refresh();
		//driver.get("https://book.cruisedirect.com/web/cruises/results.aspx?showtrace=true");
		
		//driver.get("https://book.cruisedirect.com/web/cruises/details.aspx?pid=972855&showtrace=true");
		//Thread.sleep(1000);

		// For Select cruise from result page
		/*try 
		{
				
			driver.findElement(By.xpath("//a[starts-with(@href, 'details.aspx?pid=976155') and @class='red-btn']")).click();
			driver.findElement(By.xpath("//*[@css=concat('A[href=', ''', 'details.aspx?pid=976155', ''', ']')]")).click();
			driver.findElement(By.xpath("(.//*[contains(text(),'Select')])[position()=1]")).click();
//			driver.findElement(By.xpath("(.//*[contains(text(),'Select')])[position()=1]")).sendKeys(Keys.ENTER);
			//driver.findElement(By.xpath("//*[@text='Select']")).click();


			
			
		//	driver.findElement(By.xpath("//*[@id='ResultsContainer']/div[2]/section/div[1]/section/table/tbody/tr/td[7]/a")).sendKeys(Keys.ENTER);
			Thread.sleep(3000);
			
		} catch (Exception e) {

			System.out.println("Cruise not available..");
			System.out.println("\n");
			System.out.println("Cruise not available on resultpage Logs..");
			System.out.println("\n");
//			ExtractJSLogs();
			//
			// * final Screenshot screenshot5 = new AShot().shootingStrategy(new
			// * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 //* BufferedImage image5 = screenshot5.getImage();
			 //* ImageIO.write(image5, "PNG", new
			 //* File("D:\\Ajit\\Script_SS\\BPCError\\2_Cruisenotavailable.png"));
			 
			Assert.assertFalse(false, "FAIL");
			Reporter.log("Cruise not available..", true);
			AssertJUnit.assertTrue("Cruise not available...", crsbkngpge.isDisplayed());
			throw (e);
		}*/
		long start = System.currentTimeMillis();

		Thread.sleep(1100);

		long finish = System.currentTimeMillis();
		long totalTime = finish - start;
		Reporter.log("Total Time for result page to details page load(Milisec) - " + totalTime, true);
		Thread.sleep(1000);

		/*for (int i = driver.getWindowHandles().size() - 1; i > 0; i--) {

			String winHandle = driver.getWindowHandles().toArray()[i].toString();

			driver.switchTo().window(winHandle);

			driver.navigate().refresh();

			Thread.sleep(3000);

			driver.switchTo().frame(driver.findElement(By.tagName("iframe")));

			if (driver.findElement(By.xpath("//*[@id='PopUp']/div[2]/div/div[7]/div[3]/button")).isDisplayed()) {

				driver.findElement(By.xpath("//*[@id='PopUp']/div[2]/div/div[7]/div[3]/button")).click(); // Handle
																											// popup
																											// on
																											// details
																											// page
				Thread.sleep(100);

			}
		
			driver.switchTo().defaultContent();
			Thread.sleep(3000);*/
		
			
			/*
			 * final Screenshot screenshot6 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image6 = screenshot6.getImage();
			 * ImageIO.write(image6, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\3_Cruisedetailspage.png"));
			 * Thread.sleep(1500);
			 */
			/*String myArg1 = "D:\\Ajit\\Script_SS\\BPC\\3_Cruisedetailspage.png";
			String myArg2 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\3_Cruisedetailspage.png";
			String myCommand = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\3_Cruisedetailspage.png";

			ProcessBuilder pb = new ProcessBuilder(myCommand, myArg1, myArg2, Output, Log);
			pb.directory(new File("D:\\Ajit\\Script_SS"));
			Process p = pb.start();

			System.out.println("" + p);*/

			//System.out.println("Cruise selected successfully..");

			//Thread.sleep(4000);

			// For the Cruise details are print on HTML report
			/*String Departing = driver
					.findElement(By.xpath("//p[contains(text(),'Departing:')]//following::p[1]"))
					.getText();
			Reporter.log("Departing:- " + Departing, true);
			String Sailing = driver
					.findElement(By.xpath("//p[contains(text(),'Departing:')]//following::p[3]"))
					.getText();
			Reporter.log("Sailing Dates:- " + Sailing, true);
			String shp = driver
					.findElement(By.xpath("//p[contains(text(),'Departing:')]//following::p[5]"))
					.getText();
			Reporter.log("Ship: " + shp, true);
			String Destination = driver
					.findElement(By.xpath("//p[contains(text(),'Departing:')]//following::p[7]"))
					.getText();
			Reporter.log("Destination:- " + Destination, true);
			String Duration = driver
					.findElement(By.xpath("//p[contains(text(),'Departing:')]//following::p[9]"))
					.getText();
			Reporter.log("Duration:- " + Duration, true);*/
			
			// For the Cruise details page
		
		Thread.sleep(10000);
		
		WebDriver driver1 = new Augmenter().augment(driver);
		File file1  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file1, new File("E:\\Automation\\CruiseFusion_Test\\Guestinfopage.png"));
		
		// For Image comparison
					String actual1 = "E:\\Automation\\CruiseFusion_Test\\Guestinfopage.png";
					String reference1 = "E:\\Automation\\Reference images\\CruiseFusion\\Guestinfopage.png";
					String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
					String Output1 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Guestinfopage.png";

					ProcessBuilder pb1 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output1, actual1, reference1, Log);
					
					pb1.directory(new File("E:\\node_modules\\.bin"));
					pb1.start();
			try {

				
				driver.findElement(By.id("_ctl0:MainContentsPH:_ctl0:_ctl0_GuestAge_1")).sendKeys("10");
				Thread.sleep(300);
				
				driver.findElement(By.id("_ctl0:MainContentsPH:_ctl0:_ctl0_GuestAge_2")).sendKeys("67");
				Thread.sleep(300);
				;
				Select select1 = new Select(driver.findElement(By.id("_ctl0_MainContentsPH__ctl0__ctl0_ResidentState")));
				select1.selectByVisibleText("California (CA)");
				
				/*if(driver.findElement(By.id("_ctl0_MainContentsPH__ctl0__ctl0_EmailTXT")).isDisplayed())
                {
                 
                  driver.findElement(By.id("_ctl0_MainContentsPH__ctl0__ctl0_EmailTXT")).sendKeys("ajit_nakum@odysseussolutions.com");
                }*/
				
				Select residentcity = new Select(driver.findElement(By.id("_ctl0_MainContentsPH__ctl0__ctl0_ResidentCity")));
				residentcity.selectByVisibleText("Long Beach (LGB)");
				
				driver.findElement(By.id("_ctl0_MainContentsPH__ctl0__ctl0_CategoryLNK"));
				
Thread.sleep(2000);
				
				driver1 = new Augmenter().augment(driver);
				File file7  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(file7, new File("E:\\Automation\\CruiseFusion_Test\\Guestinfopage2.png"));
				
				// For Image comparison
							String actual7 = "E:\\Automation\\CruiseFusion_Test\\Guestinfopage2.png";
							String reference7 = "E:\\Automation\\Reference images\\CruiseFusion\\Guestinfopage2.png";
							//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
							String Output7 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Guestinfopage2.png";

							ProcessBuilder pb7 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output7, actual7, reference7, Log);
							
							pb7.directory(new File("E:\\node_modules\\.bin"));
							pb7.start();
				
				
				driver.findElement(By.id("_ctl0_MainContentsPH__ctl0__ctl0_CategoryLNK")).sendKeys(Keys.ENTER);

								
				//driver.findElement(By.xpath("//*[@id='_ctl0_MainContentsPH__ctl0__ctl1_CategoryLNK']")).sendKeys(Keys.ENTER);

				long start12 = System.currentTimeMillis();

				System.out.println("\n");
				System.out.println("Categorypage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();

				Thread.sleep(8150);

				long finish1 = System.currentTimeMillis();
				long totalTime1 = finish1 - start12;
				Reporter.log("Total Time for details page to category page load(Milisec) - " + totalTime1, true);
				Thread.sleep(2000);
			} 
			
			
			
			catch (Exception e) {
				System.out.println("\n");
				System.out.println("Invalid guest information on cruise detailspage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot8 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image8 = screenshot8.getImage();
				 * ImageIO.write(image8, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\3_InvalidGuestinformation.png"
				 * ));
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Invalid guest information on detailspage...", true);
				AssertJUnit.assertTrue("Invalid guest information on detailspage...", crsbkngpge.isDisplayed());
				throw (e);
			}

			// For select category from the category page
			
Thread.sleep(15000);
			
			driver1 = new Augmenter().augment(driver);
			File file2  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file2, new File("E:\\Automation\\CruiseFusion_Test\\Categorypage.png"));
			
			// For Image comparison
						String actual2 = "E:\\Automation\\CruiseFusion_Test\\Categorypage.png";
						String reference2 = "E:\\Automation\\Reference images\\CruiseFusion\\Categorypage.png";
						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output2 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Categorypage.png";

						ProcessBuilder pb2 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output2, actual2, reference2, Log);
						
						pb2.directory(new File("E:\\node_modules\\.bin"));
						pb2.start();
			
			//Price on category page
			String priceoncategorypge = driver.findElement(By.xpath("(//div[@class='res_catprice'])[position()=1]")).getText();
			Reporter.log("Price on the category page per person is:- " + priceoncategorypge, true);
			
			Thread.sleep(2000);

			
			 driver.findElement(By.xpath("(//a[text()='Select'])[position()=1]")).click();

			 
			 //Price on category details (Book Now) page after selecting category
			 /*String totalcruiserate = driver.findElement(By.xpath("(//div[@class='res_catprice'])[position()=1]")).getText();
				Reporter.log("Total cruise rate on category page is:- " + totalcruiserate, true);
				
				
				
				String totalperguest = driver.findElement(By.xpath("(//div[@class='res_catprice'])[position()=1]")).getText();
				Reporter.log("Total per guest on category page is:- " + totalperguest, true);*/
			 
			 Thread.sleep(10000);
				
				driver1 = new Augmenter().augment(driver);
				File file111  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(file111, new File("E:\\Automation\\CruiseFusion_Test\\Booknowpage.png"));
				
				// For Image comparison
							String actual111 = "E:\\Automation\\CruiseFusion_Test\\Booknowpage.png";
							String reference111 = "E:\\Automation\\Reference images\\CruiseFusion\\Booknowpage.png";
							//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
							String Output111 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Booknowpage.png";

							ProcessBuilder pb111 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output111, actual111, reference111, Log);
							
							pb111.directory(new File("E:\\node_modules\\.bin"));
							pb111.start();
			 
			 driver.findElement(By.xpath("//*[@text='Book Now' and ./parent::*[@nodeName='SPAN']]")).click();
			 
			 //Category page popup handling
				 
					 
					 
					 Thread.sleep(2000);
						
						driver1 = new Augmenter().augment(driver);
						File file112  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
						FileUtils.copyFile(file112, new File("E:\\Automation\\CruiseFusion_Test\\Booknowpage_continuebtn.png"));
						
						// For Image comparison
									String actual112 = "E:\\Automation\\CruiseFusion_Test\\Booknowpage_continuebtn.png";
									String reference112 = "E:\\Automation\\Reference images\\CruiseFusion\\Booknowpage_continuebtn.png";
									//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
									String Output112 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Booknowpage_continuebtn.png";

									ProcessBuilder pb112 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output112, actual112, reference112, Log);
									
									pb112.directory(new File("E:\\node_modules\\.bin"));
									pb112.start();
				 
									driver.findElement(By.xpath("//*[@text='Continue']")).click();
									 System.out.println("Popup on category page handled");
			 
			
				/*
				  driver.findElement(By.id("showIn")).click();
				  Thread.sleep(1000);
				  driver.findElement(By.id("showOut")).click();
				  Thread.sleep(3000);
				 

				
				 * final Screenshot screenshot9 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image9 = screenshot9.getImage();
				 * ImageIO.write(image9, "PNG", new
				 * File("D:\\Ajit\\Script_SS\\BPC\\4_Categorypage.png"));
				 * Thread.sleep(4000);
				 */
				/*String myArg3 = "D:\\Ajit\\Script_SS\\BPC\\4_Categorypage.png";
				String myArg4 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\4_Categorypage.png";
				String myCommand1 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
				String Output1 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\4_Categorypage.png";

				ProcessBuilder pb1 = new ProcessBuilder(myCommand1, myArg3, myArg4, Output1, Log);
				pb1.directory(new File("D:\\Ajit\\Script_SS"));
				Process p1 = pb1.start();

				System.out.println("" + p1);*/

				/*String expr1 = driver.findElement(By.id("TracerBlock")).getText();
				String proccessingloops = expr1.split("WEB-93")[58].split(":")[1].trim();
				Reporter.log("Category processing loop :- " + proccessingloops, true);
				Thread.sleep(500);
				String totalprocesstime = expr1.split("WEB-93")[69].split(":")[1].trim();
				Reporter.log("TotalProcessing Time on category page:- " + totalprocesstime, true);
				Thread.sleep(3000);*/
			
			
//				driver.findElement(By.id("CabinBook_")).click();
			//For Stateroom selection
			
			

					 
					 String priceonstateroompge = driver.findElement(By.id("cabinfare")).getText();
						Reporter.log("Total Price on the Stateroom page is:- " + priceonstateroompge.trim(), true);
						
						Thread.sleep(2000);
						
						driver1 = new Augmenter().augment(driver);
						File file3  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
						FileUtils.copyFile(file3, new File("E:\\Automation\\CruiseFusion_Test\\StateroomSelectionpage.png"));
						
						
						
						// For Image comparison
									String actual3 = "E:\\Automation\\CruiseFusion_Test\\StateroomSelectionpage.png";
									String reference3 = "E:\\Automation\\Reference images\\CruiseFusion\\StateroomSelectionpage.png";
									//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
									String Output3 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_StateroomSelectionpage.png";

									ProcessBuilder pb3 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output3, actual3, reference3, Log);
									
									pb3.directory(new File("E:\\node_modules\\.bin"));
									pb3.start();
									
									
				try
		          {
									driver.findElement(By.xpath("//*[@text='Book Now']")).click();
		                              Thread.sleep(2000);
		                              
		          }
		          catch(Exception e)
		          {
		                          	      System.out.println("Deck plan not available..");
		                          	      System.out.println("\n");
		                          	      System.out.println("Deck plan not available on select Stateroom Logs..");
		                          	      System.out.println("\n");
		                          	      //ExtractJSLogs();
		                             
		                                  /*final Screenshot screenshot13 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);
		                                  final BufferedImage image13 = screenshot13.getImage();
		                                  ImageIO.write(image13, "PNG", new File("D:\\Ajit\\Script_SS\\VivaVoyageError\\4_Cabinnotavailable.png"));
		                             */
		                                  Assert.assertFalse(false, "FAIL");
		              	                  Reporter.log("Stateroom not available..");
		              			          AssertJUnit.assertTrue("Stateroom not available...", crsbkngpge.isDisplayed());
		              			          throw(e);
		           }
				
				long start4 = System.currentTimeMillis();
                Thread.sleep(1000);
                System.out.println("\n");
                System.out.println("Purchase page Logs..");
                System.out.println("\n");
                //ExtractJSLogs();              
                Thread.sleep(1000);
                
                long finish11 = System.currentTimeMillis();
                long totalTime11 = finish11 - start4; 
                Reporter.log("Total Time from Stateroom selection page to purchase page load(Milisec) - "+totalTime11); 
                Thread.sleep(1000);
                
                Thread.sleep(2000);
                
                /*//For Stateroom selection
				if(driver.findElement(By.xpath("(//button[@title='Select'])[position()=1]")).isDisplayed())
		          {
		                              driver.findElement(By.xpath("(//button[@title='Select'])[position()=1]")).click();
		                              Thread.sleep(2000);
		                              driver.navigate().refresh();
		                              
		                              long start21 = System.currentTimeMillis();
		              			          Thread.sleep(1000);
		              			          System.out.println("\n");
		              			          System.out.println("Purchase page Logs..");
		              			          System.out.println("\n");
		              			          //ExtractJSLogs();              
		              			          Thread.sleep(1000);
                
		              			          long finish21 = System.currentTimeMillis();
		              			          long totalTime21 = finish21 - start21; 
		              			          Reporter.log("Total Time for cabin selection page to purchase page load(Milisec) - "+totalTime21); 
		              			          Thread.sleep(1000);
                
		              			          driver.navigate().refresh();
		              			          Thread.sleep(2000);
		          }
				else
		          {
		                          	      System.out.println("Stateroom not available..");
		                          	      System.out.println("\n");
		                          	      System.out.println("Stateroom not available on select cabinpage Logs..");
		                          	      System.out.println("\n");
		                          	      //ExtractJSLogs();
		                             
		                                  final Screenshot screenshot13 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);
		                                  final BufferedImage image13 = screenshot13.getImage();
		                                  ImageIO.write(image13, "PNG", new File("D:\\Ajit\\Script_SS\\VivaVoyageError\\4_Cabinnotavailable.png"));
		                             
		                                  Assert.assertFalse(false, "FAIL");
		              	                  Reporter.log("Stateroom not available..");
		              			          AssertJUnit.assertTrue("Stateroom not available...", crsbkngpge.isDisplayed());
		              			          
		              			          
		              			          
		          }*/
				
				
				
				
					// For price check on the purchase page
					/*if (driver
							.findElement(By.xpath("//div[@class='col-xs-4 col-sm-2 text-center pad-fix']/p")).isDisplayed()) {
						String pricesoncat = driver
								.findElement(By.xpath("//div[@class='col-xs-4 col-sm-2 text-center pad-fix']/p")).getText();
						System.out.println("Prices on purchase page is:- " + pricesoncat);
						Reporter.log("Prices on purchase page is:- " + pricesoncat, true);
						Thread.sleep(4000);
					}
				 else {
					
					 * final Screenshot screenshot80 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image80 = screenshot80.getImage();
					 * ImageIO.write(image80, "PNG", new File(
					 * "D:\\Ajit\\Script_SS\\BPCError\\4_Pricenotavailableoncategorypage.png"
					 * ));
					 
					Assert.assertFalse(false, "FAIL");
					Reporter.log("Price not availabe on category page...", true);
					AssertJUnit.assertTrue("Price not availabe on category page...", crsbkngpge.isDisplayed());
					
				}*/
                String Paxinfopgeprice = driver.findElement(By.id("PricesGTotal")).getText();
                Reporter.log("Total due on pax info page before entering passenger info =" +Paxinfopgeprice);
                		
Thread.sleep(10000);
    			
    			driver1 = new Augmenter().augment(driver);
    			File file4  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
    			FileUtils.copyFile(file4, new File("E:\\Automation\\CruiseFusion_Test\\Detailspage.png"));
    			
    			// For Image comparison
    						String actual4 = "E:\\Automation\\CruiseFusion_Test\\Detailspage.png";
    						String reference4 = "E:\\Automation\\Reference images\\CruiseFusion\\Detailspage.png";
    						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
    						String Output4 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Detailspage.png";

    						ProcessBuilder pb4 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output4, actual4, reference4, Log);
    						
    						pb4.directory(new File("E:\\node_modules\\.bin"));
    						pb4.start();
    						
    						Thread.sleep(2000);
    						
    						driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_ContinueLNK"));
    						
    						driver1 = new Augmenter().augment(driver);
    						File file10  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
    						FileUtils.copyFile(file10, new File("E:\\Automation\\CruiseFusion_Test\\Detailspage2.png"));
    						
    						// For Image comparison
    									String actual10 = "E:\\Automation\\CruiseFusion_Test\\Detailspage2.png";
    									String reference10 = "E:\\Automation\\Reference images\\CruiseFusion\\Detailspage2.png";
    									//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
    									String Output10 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Detailspage2.png";

    									ProcessBuilder pb10 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output10, actual10, reference10, Log);
    									
    									pb10.directory(new File("E:\\node_modules\\.bin"));
    									pb10.start();

			try {
				System.out.println("FirstName_Of_Guest1: " + FirstName_Of_Guest1);
				System.out.println("MiddleName_Of_Guest1: " + MiddleName_Of_Guest1);
				System.out.println("LastName_Of_Guest1: " + LastName_Of_Guest1);
				System.out.println("Address: " + Address);
				System.out.println("CityName: " + CityName);
				System.out.println("PinCode: " + PinCode);
				System.out.println("EmailAddress: " + EmailAddress);
				System.out.println("Phone: " + Phone);
				System.out.println("Firstname_of_Guest2: " + Firstname_of_Guest2);
				System.out.println("Middlename_of_Guest2: " + Middlename_of_Guest2);
				System.out.println("Lastname_of_Guest2: " + Lastname_of_Guest2);

				crspurchpge.DLCPurchasetotitle(FirstName_Of_Guest1, MiddleName_Of_Guest1, LastName_Of_Guest1, Address,
						CityName, PinCode, EmailAddress, Phone, Firstname_of_Guest2, Middlename_of_Guest2,
						Lastname_of_Guest2);
				
					driver.swipe(748, 616, 748, 1947, 288);
				   driver.swipe(748, 616, 748, 1947, 288);
				   driver.swipe(748, 616, 748, 1947, 288);
				   
				   driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_ContinueLNK")).click();

			} catch (Exception e) {
				System.out.println(
						"Invalid guest information on purchase page, Please enter valid details for the required fields...");
				System.out.println("\n");
				System.out.println("Invalid guest information on purchasepage Logs..");
				System.out.println("\n");
//				ExtractJSLogs();
				/*
				 * final Screenshot screenshot34 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image34 = screenshot34.getImage();
				 * ImageIO.write(image34, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\8_InvalidGuestinfoonPurchasepage.png"
				 * ));
				 */
				Assert.assertFalse(false, "FAIL");
				Reporter.log(
						"Invalid guest information on purchase page, Please enter valid details for the required fields...");
				AssertJUnit.assertTrue(
						"Invalid guest information on purchase page, Please enter valid details for the required fields....",
						crsbkngpge.isDisplayed());
				throw (e);
			}
			long start16 = System.currentTimeMillis();
			Thread.sleep(2070);
			long finish03 = System.currentTimeMillis();
			long totalTime03 = finish03 - start16;
			Reporter.log("Total Time after entering passenger info and clicking continue button to Paxinfo page(Milisec) - " + totalTime03, true);
			Thread.sleep(2000);

			System.out.println("\n");
			System.out.println("Paxinfo page Logs..");
			System.out.println("\n");
//			ExtractJSLogs();
			
			//For prices on paxinfo page 2nd time
			String Paxinfopgeprice1 = driver.findElement(By.id("PricesGTotal")).getText();
            Reporter.log("Total due on pax info page after entering passenger info =" +Paxinfopgeprice1, true);
            
            Thread.sleep(10000);
            
            
			
			driver1 = new Augmenter().augment(driver);
			File file5  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file5, new File("E:\\Automation\\CruiseFusion_Test\\Pricesonpaxinfopage.png"));
			
			// For Image comparison
						String actual5 = "E:\\Automation\\CruiseFusion_Test\\Pricesonpaxinfopage.png";
						String reference5 = "E:\\Automation\\Reference images\\CruiseFusion\\Pricesonpaxinfopage.png";
						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output5 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Pricesonpaxinfopage.png";

						ProcessBuilder pb5 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output5, actual5, reference5, Log);
						
						pb5.directory(new File("E:\\node_modules\\.bin"));
						pb5.start();
            
            //Comparing prices before and after entering passenger info
            if (Paxinfopgeprice==Paxinfopgeprice1){
            	Reporter.log("Prices on pax info page before and after entering passenger information is same and equal to:" +Paxinfopgeprice1, true);
            }
			driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_ContinueBTN")).click();
			
			long start17 = System.currentTimeMillis();
			Thread.sleep(2070);
			long finish17 = System.currentTimeMillis();
			long totalTime17 = finish17 - start17;
			Reporter.log("Total Time from Paxinfo page to billing page load(Milisec) - " + totalTime17, true);
			Thread.sleep(2000);

			
			
			String paymentpageprice = driver.findElement(By.id("PaymentAmountToPay")).getText();
            Reporter.log("Total due on payment page =" +paymentpageprice, true);
            
            if (Paxinfopgeprice==paymentpageprice){
            	Reporter.log("Prices on pax info page and payment page is same and equal to:" +paymentpageprice, true);
            }
			
            driver.swipe(748, 616, 748, 1947, 288);
            driver.swipe(748, 616, 748, 1947, 288);
            driver.swipe(748, 616, 748, 1947, 288);
            
            Thread.sleep(2000);
            
            driver1 = new Augmenter().augment(driver);
			File file6  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file6, new File("E:\\Automation\\CruiseFusion_Test\\Pricesonpaxinfopage2.png"));
			
			// For Image comparison
						String actual6 = "E:\\Automation\\CruiseFusion_Test\\Pricesonpaxinfopage2.png";
						String reference6 = "E:\\Automation\\Reference images\\CruiseFusion\\Pricesonpaxinfopage2.png";
						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output6 = "E:\\Automation\\Difference of Images\\CruiseFusion\\Difference_Pricesonpaxinfopage2.png";

						ProcessBuilder pb6 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output6, actual6, reference6, Log);
						
						pb6.directory(new File("E:\\node_modules\\.bin"));
						pb6.start();
            
            
			/*
			 * final Screenshot screenshot44 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image44 = screenshot44.getImage();
			 * ImageIO.write(image44, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\11_Confirmationpage.png"));
			 * Thread.sleep(2000);
			 */
			/*String myArg11 = "D:\\Ajit\\Script_SS\\BPC\\11_Confirmationpage.png";
			String myArg12 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\11_Confirmationpage.png";
			String myCommand5 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output5 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\11_Confirmationpage.png";

			ProcessBuilder pb5 = new ProcessBuilder(myCommand5, myArg11, myArg12, Output5, Log);
			pb5.directory(new File("D:\\Ajit\\Script_SS"));
			Process p5 = pb5.start();

			System.out.println("" + p5);*/

			// For Price on confirmation page
			/*String price = driver.findElement(By.xpath("//div[@class='col-xs-4 col-sm-2 text-center pad-fix']//following::p[@class='red mar-xs-top']")).getText();
			Reporter.log("Price on the passenger details page is:- " + price, true);
			
			// For Insurance
			try {
				if (driver
						.findElement(By.xpath("(//input[starts-with(@id,'InsurranceCHK')][@name='InsurranceOption']//following::span)[position()=1]")).isDisplayed()) {
					driver.findElement(
							By.xpath("(//input[starts-with(@id,'InsurranceCHK')][@name='InsurranceOption']//following::span)[position()=1]")).click();
					Thread.sleep(1500);
					Reporter.log("Insurance applied successfully..", true);

					// For check insurance price
					String insurprice = driver
							.findElement(By.xpath("//div[@class='col-xs-4 col-sm-2 text-center pad-fix']//following::p[@class='red mar-xs-top']"))
							.getText();
					Reporter.log("Prices of After applied insurance on the passenger details page is:- " + insurprice, true);
					Thread.sleep(1000);
					
					String insuranceprice = driver.findElement(By.xpath("//*[contains(text(),'Insurance Payment')]//following-sibling::div[3]")).getText();
					
					Reporter.log("Price of selected insurance:-" + insuranceprice, true);
					
					
					
					 * final Screenshot screenshot64 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image64 = screenshot64.getImage();
					 * ImageIO.write(image64, "PNG", new
					 * File("D:\\Ajit\\Script_SS\\BPC\\12_AppliedInsurance.png")
					 * ); Thread.sleep(2000);
					 
					String myArg13 = "D:\\Ajit\\Script_SS\\BPC\\12_AppliedInsurance.png";
					String myArg14 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\12_AppliedInsurance.png";
					String myCommand6 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
					String Output6 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\12_AppliedInsurance.png";

					ProcessBuilder pb6 = new ProcessBuilder(myCommand6, myArg13, myArg14, Output6, Log);
					pb6.directory(new File("D:\\Ajit\\Script_SS"));
					Process p6 = pb6.start();

					System.out.println("" + p6);

					// Applied insurance price
					String appliedinsurprice = driver
							.findElement(By.xpath("//td[@class='pmtsAmt']")).getText();
					Reporter.log("Insurance price:- " + appliedinsurprice, true);
					Thread.sleep(1000);

					// For Remove insurance
					WebElement insurnc = driver.findElement(By.id("InsurranceCHK_NON"));
					insurnc.click();
					Thread.sleep(3000);
					driver.context(Native);

                    driver.findElement(By.xpath("//*[@text='OK']")).click();
                    
                    driver.context(Webview);
                    Thread.sleep(4000);

					Reporter.log("Insurance Removed successfully..", true);

					// For check insurance price
					String insprice = driver
							.findElement(By.xpath("//div[@class='col-xs-4 col-sm-2 text-center pad-fix']//following::p[@class='red mar-xs-top']"))
							.getText();
					Reporter.log("Prices of After removed insurance on the passenger details page is:- " + insprice, true);
					Thread.sleep(1000);
					
					 * final Screenshot screenshot66 = new
					 * AShot().shootingStrategy(new
					 * ViewportPastingStrategy(500)).takeScreenshot(driver);
					 * final BufferedImage image66 = screenshot66.getImage();
					 * ImageIO.write(image66, "PNG", new
					 * File("D:\\Ajit\\Script_SS\\BPC\\13_RemovedInsurance.png")
					 * ); Thread.sleep(2000);
					 * 
					  String myArg15 = "D:\\Ajit\\Script_SS\\BPC\\13_RemovedInsurance.png";
					String myArg16 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\13_RemovedInsurance.png";
					String myCommand7 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
					String Output7 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\13_RemovedInsurance.png";

					ProcessBuilder pb7 = new ProcessBuilder(myCommand7, myArg15, myArg16, Output7, Log);
					pb7.directory(new File("D:\\Ajit\\Script_SS"));
					Process p7 = pb7.start();

					System.out.println("" + p7);

				}
			} catch (Exception e) {
				System.out.println("Insurance not applied/removed...");
				System.out.println("\n");
				System.out.println("Insurance not applied/removed on confirmation page...");
				System.out.println("\n");
//				ExtractJSLogs();
				
				 * final Screenshot screenshot72 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image72 = screenshot72.getImage();
				 * ImageIO.write(image72, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\9_InsurancenotAppliedOrRemoved.png"
				 * ));
				 
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Insurance not applied/removed...", true);
				// AssertJUnit.assertTrue("Insurance not applied/removed....",
				// crsbkngpge.isDisplayed());
				// throw(e);
			}*/

			/*try {
				// For select terms and condition check box
				driver.findElement(By.id("AgreeTermsCON")).click();
				Thread.sleep(1000);

				// Click on Pay in full payment button
				driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_ContinueBTN")).click();
				long start17 = System.currentTimeMillis();
				Thread.sleep(1001);

				long finish30 = System.currentTimeMillis();
				long totalTime30 = finish30 - start17;
				Reporter.log("Total Time for confirmation page to payment page load(Milisec) - " + totalTime30, true);
				Thread.sleep(2000);
			} catch (Exception e) {

				System.out.println("Page navigate on invalid page Or Displaying Error message on confirmation page...");
				System.out.println("\n");
				System.out.println("Page navigate on invalid page Or Displaying Error message on confirmation page...");
				System.out.println("\n");
//				ExtractJSLogs();
				
				 * final Screenshot screenshot27 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
				 * BufferedImage image27 = screenshot27.getImage();
				 * ImageIO.write(image27, "PNG", new File(
				 * "D:\\Ajit\\Script_SS\\BPCError\\10_PageNavigateonInvalidPageOrErrormessaage.png"
				 * ));
				 
				Assert.assertFalse(false, "FAIL");
				Reporter.log("Page navigate on invalid page Or Displaying Error message on confirmation page...", true);
				AssertJUnit.assertTrue(
						"Page navigate on invalid page Or Displaying Error message on confirmation page....",
						crsbkngpge.isDisplayed());
				throw (e);

			}
			System.out.println("\n");
			System.out.println("Paymentpage Logs..");
			System.out.println("\n");
//			ExtractJSLogs();
			Thread.sleep(1000);
			*/
			/*
			 * final Screenshot screenshot49 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver); final
			 * BufferedImage image49 = screenshot49.getImage();
			 * ImageIO.write(image49, "PNG", new
			 * File("D:\\Ajit\\Script_SS\\BPC\\14_Paymentpage.png"));
			 * Thread.sleep(2000);
			 * 
			  String myArg15 = "D:\\Ajit\\Script_SS\\BPC\\14_Paymentpage.png";
			String myArg16 = "D:\\Ajit\\Script_SS\\Daily Sanity\\August_2017\\30 Aug 2017\\BPC\\BPC\\14_Paymentpage.png";
			String myCommand7 = "D:\\Ajit\\Script_SS\\ImageCompConsole.exe";
			String Output7 = "D:\\Ajit\\Script_SS\\Differences of Images\\BPC\\14_Paymentpage.png";

			ProcessBuilder pb7 = new ProcessBuilder(myCommand7, myArg15, myArg16, Output7, Log);
			pb7.directory(new File("D:\\Ajit\\Script_SS"));
			Process p7 = pb7.start();

			System.out.println("" + p7);*/

			/*String paymntprice = driver.findElement(By.id("PayPrice")).getText();
			String paymntprice1 = driver.findElement(By.xpath("//div[@class='col-sm-6']//following::label")).getText();
			Reporter.log("Price on payment page is:- " + paymntprice, true);
			Reporter.log("Price on payment page is:- " + paymntprice1, true);
			if (paymntprice.equals(price)) {

				System.out.println("Actual String: " + price);
				System.out.println("Passenger details page and Payment page's Price matched");
				Reporter.log("Passenger details page and Payment page's Price matched..", true);

				System.out.println("Actual price is:- " + price);
				Reporter.log("Actual price is:- " + price, true);
			} else {
				System.out.println("Passenger details page and Payment page's Price not match..");
				Reporter.log("Passenger details page and Payment page's Price not match..", true);
				System.out.println("Actual price is:- " + price);
				Reporter.log("Actual price is:- " + price, true);
			}

			Thread.sleep(1500);
*/
			// For the compare logs
			/*String ConsleArgument = "D:\\Ajit\\Script_SS\\ConsoleError\\BPCCruisebookingError.txt";
			String ConsoleCommand = "D:\\Ajit\\Script_SS\\ConsoleApplication2.exe";
			String ConsoleOutput = "D:\\Ajit\\Script_SS\\BPCError\\UncaughtBPC.txt";

			ProcessBuilder pb05 = new ProcessBuilder(ConsoleCommand, ConsleArgument, ConsoleOutput);
			pb05.directory(new File("D:\\Ajit\\Script_SS"));
			Process p05 = pb05.start();

			System.out.println("" + p05);

			Thread.sleep(1000);*/
		}

	

	@DataProvider
	public String[][] Authentications() throws Exception {

		String[][] testObjArray = LocalAirUtils.getTableArray(
				"E:\\Automation\\Workspace\\Odysseus_Responsive\\src\\main\\java\\testData\\CDORData.xlsx", "Sheet1");
		return testObjArray;

	}

	@AfterClass
	public void closeBrowser() throws InterruptedException {

		
		 if(driver!=null) { System.out.println("Closing the browser");
		  driver.quit(); }
		 
	}

}
