package airtestclass;

import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.apache.bcel.generic.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import Utility.UtilClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;			

public class MyFirstClass {
	WebDriver driver = new ChromeDriver();
	WebDriverWait myWait = new WebDriverWait(driver, 60);
	@BeforeTest
	
	public void Beforeclasstest(){
	
	System.setProperty("webdriver.chrome.driver","E:\\Eclipse\\chromedriver.exe");							//Declaring to Initiate the ChromeExtension
	String baseUrl = "http://192.168.1.147:4426/";
	driver.get(baseUrl);
	WebElement Username = driver.findElement(By.id("username"));											//create Username textbox object to fill value in it
        	Username.sendKeys("ycmanager");
        
        WebElement Password = driver.findElement(By.id("password"));											//create Password textbox object to fill value in it
        	Password.sendKeys("ody@ncl");	
        
        driver.findElement(By.id("btnSubmit")).click();
        driver.findElement(By.xpath("//*[@id=\'form1\']/div[5]/div[15]/button[1]")).click();
	}
	
	@Test(dataProvider="DataDriven")
	public void  FirstTest(String Cabinnumber) {
		
		//RegistrationProcess Reg = null;
		
		
	
/* Passing a URL*/
		
				// Search for the URL
/* End Of Passing URL Section*/
       
/* Login Page */         
        															// Click on Submit Button
/* End Login Page*/        
       
/* Manage Registration*/    
        
         //driver.findElement(By.id("megaanchor")).click();														// Click on Menu
         //myWait.until(ExpectedConditions.presenceOfElementLocated(By.className("tile greentile bigtile")));
        //String[][] data=(String[][])Cabinnumber;
        //for (int i=0; i<=UtilClass.totalRows;i++) {
        
         													// Click on Manage Registration
         
         driver.findElement(By.id("cabinNoChild")).sendKeys(Cabinnumber);
         driver.findElement(By.id("btnSubmitChild")).click();
         long starttime = System.currentTimeMillis();
         
         myWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'gu_topinfopagename')]")));
         long Endtime = System.currentTimeMillis();
         long Totaltime = Endtime - starttime;
         
         System.out.println("Total time required to redirect in miliseconds:" +  Totaltime);
         
         driver.findElement(By.id("megaanchor")).click();
         myWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("btnYes")));
         driver.findElement(By.id("btnYes")).click();
         
         myWait.until(ExpectedConditions.visibilityOfElementLocated(By.className("menu_registration")));

         driver.findElement(By.className("menu_registration")).click();
         myWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cabinNoChild")));


        //}
	}
         
         @DataProvider
         public String[][] DataDriven() throws Exception
         {
         
         String[][] testObjArray = UtilClass.getTableArray("E:\\Automation\\Workspace\\Odysseus_Responsive\\src\\main\\java\\testData\\Code.xlsx", "Sheet1");
         return testObjArray;
         
         
    }

}
