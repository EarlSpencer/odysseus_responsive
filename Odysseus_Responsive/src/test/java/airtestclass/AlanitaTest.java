package airtestclass;

import java.awt.image.BufferedImage;

//import java.awt.image.BufferedImage;

import java.io.File;

import java.io.IOException;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

//import javax.imageio.ImageIO;

import org.openqa.selenium.By;

import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.Keys;

import org.openqa.selenium.OutputType;

import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;

//import org.openqa.selenium.remote.CapabilityType;

import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.remote.RemoteWebDriver;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import org.testng.AssertJUnit;

import org.testng.Reporter;

//import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import org.testng.annotations.Test;

import com.experitest.client.Client;

import PageObject.Purchasepgeobjct;

import PageObject.Bookingpgeobjct;
import Utility.AirUtils;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
//import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

//import ru.yandex.qatools.ashot.Screenshot;

//import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;
//import ru.yandex.qatools.ashot.shooting.ShootingStrategy;

public class AlanitaTest

{

	DesiredCapabilities dc = new DesiredCapabilities();

	Bookingpgeobjct alanitabkpge;

	Purchasepgeobjct faredptpurchse;
	
	String Log = "E:\\Automation\\Result-log\\Alanita.txt";

	public static WebDriver driver1;

	Reporter report = new Reporter();

	private String reportDirectory = "reports";
	private String reportFormat = "xml";
	private String testName = "Faredepot(B2C)";
	static WebDriver wd;

	public IOSDriver<IOSElement> driver;
	public RemoteWebDriver driver2;
	public Client client = null;

	int scrollTimeout = 500;
	int header = 98;
	int footer = 0;
	float dpr = 2;
	
	String Native="NATIVE_APP";
	String Webview="WEBVIEW_1";

	public boolean implicitwait(long time)

	{

		try

		{

			driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);

			System.out.println("Waited for" + time + "sec implicitly.");

		} catch (Exception e)

		{

			System.out.println(e.getMessage());

		}

		return true;

	}

	@BeforeTest

	public void baseClass() throws InterruptedException, IOException

	{

		try {
			dc.setCapability("deviceName", "iPhone 6+");
			dc.setCapability(MobileCapabilityType.UDID, "c0009b556ca641c9f3f9932c327180bd5f671680");

			dc.setCapability("platformName", "iOS");
			dc.setCapability("platformVersion", "10.3.3");
			// dc.setCapability("bundleId", "com.apple.mobilesafari");

			// dc.setCapability(MobileCapabilityType.APP, "Safari");
			dc.setBrowserName(MobileBrowserType.SAFARI);
			// driver = new IOSDriver<IOSElement>(new
			// URL("http://0.0.0.0:8888/wd/hub"), dc);

			// Earl's PC
			// driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.75:8888/wd/hub"), dc);

			 // Rutuja's PC
			 //driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.131:4723/wd/hub"), dc);
			 
			 //Chintan's PC
			driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.55:4723/wd/hub"), dc); 
		
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			alanitabkpge = PageFactory.initElements(driver, Bookingpgeobjct.class);

			faredptpurchse = PageFactory.initElements(driver, Purchasepgeobjct.class);
			//WebDriverWait mywait = new WebDriverWait(driver, 30);
			dc.setCapability("reportDirectory", reportDirectory); //
			dc.setCapability("reportFormat", reportFormat);
			dc.setCapability("testName", testName);

			// driver = new IOSDriver<IOSElement>(new
			// URL("http://localhost:4724/wd/hub"), dc);
			driver.get("air.alanitatravel.com");

			Thread.sleep(3000);
		} catch (Exception e) {
			e.getMessage();
		}

		

	}

	@Test(dataProvider = "Authentication")

	public void Alanita_data(String FromLocation, String ToLocation, String Firstname_Of_Adult1,
			String Middlename_of_Adult1, String Lastname_Of_Adult1, String Firstname_Of_Adult2,
			String Middlename_of_Adult2, String Lastname_Of_Adult2, String Firstname_Of_Adult3,
			String Middlename_of_Adult3, String Lastname_Of_Adult3, String Email, String Confirm_Email, String Phone,
			String Destination_Phone) throws Exception

	{
			WebDriverWait mywait = new WebDriverWait(driver, 30);

		try

		{

			

			// For web site and booking details

			Reporter.log("Website Name :- Alanita", true);

			Thread.sleep(200);

			System.out.println("FromLocation: " + FromLocation);

			System.out.println("ToLocation: " + ToLocation);

			alanitabkpge.BookingToTitle(FromLocation, ToLocation);
			
			mywait.until(ExpectedConditions.visibilityOfElementLocated(By.id("AirSearchForm_SearchButton")));
            driver.findElement(By.id("AirSearchForm_SearchButton")).click();
            
            driver.get("https://air.alanitatravel.com/Web/air/results.aspx?showtrace=true");
            
            Thread.sleep(10000);

            
            //For noting down the time from traces when showtrace=true
            /*try
            {
            	if(driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).isDisplayed())
            	{
            		//String ResultpageLoad = driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).getText();
            		List<IOSElement> list = driver.findElements(By.id("TracerBlock"));
            		int listcount = list.size();    
            		@SuppressWarnings("rawtypes")
    				List optionsList = null;
            		String options[] = new String[listcount];

            		for (int i=1; i<=listcount; i++) 
            		{                     
            		    options[i-1] = driver.findElement(By.id("TracerBlock")).getText();
            		    //contains three words eg: Test Engineer Madhu               
            		    String[] expectedOptions = options[i-1].split(" ");
            		    //splitting based on the space           
            		    // I hope the next line gives you the correct lastName
            		    String lastName =expectedOptions[expectedOptions.length-1]; 
            		    // lets store the last name in the array
            		    options[i-1] = lastName;        
            		 }         
            		 optionsList = Arrays.asList(options);
            		 Reporter.log("Total Time for search page to result page load(Milisec) - " +optionsList, true);
            		
            		 Thread.sleep(1000);
            		
            	}
            	
            }
            catch(Exception e)
            {
            	e.getMessage();
            }

			Thread.sleep(11000);*/
			
			WebDriver driver1 = new Augmenter().augment(driver);
			File file1  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file1, new File("E:\\Automation\\Alanita_Test\\SearchPage.png"));
			
			// For Image comparison
						String actual1 = "E:\\Automation\\Alanita_Test\\SearchPage.png";
						String reference1 = "E:\\Automation\\Reference images\\Alanita\\SearchPage.png";
						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
						String Output1 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_SearchPage.png";

						ProcessBuilder pb1 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output1, actual1, reference1, Log);
						
						pb1.directory(new File("E:\\node_modules\\.bin"));
						Process p1 = pb1.start();

						System.out.println("" + p1);

		}

		catch (Exception e)

		{

			

			/*
			 * final Screenshot screenshot33 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver);
			 * 
			 * final BufferedImage image33 = screenshot33.getImage();
			 * 
			 * ImageIO.write(image33, "PNG", new File(
			 * "//Users//admin//Desktop//Odysseus//Odysseus project//FaredepotError//1_TimeoutORinvalidsearchdetails.png"
			 * ));
			 */
			
			WebDriver driver1 = new Augmenter().augment(driver);
			File file  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file, new File("E:\\Automation\\Alanita_TestError\\1_TimeoutORinvalidsearchdetails.png"));

			Assert.assertFalse(false, "FAIL");

			Reporter.log("Time out or Invalid search criteria on resultpage..", true);

			AssertJUnit.assertTrue("Time out or Invalid search criteria on resultpage...", alanitabkpge.isDisplayed());

			throw (e);

		}

		/*
		 * 
		 * // For select dynamic flights from result page
		 * 
		 * 
		 * 
		 * driver.navigate().refresh();
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * driver.manage().window().maximize();
		 * 
		 * 
		 * 
		 * System.out.println("\n");
		 * 
		 * System.out.println("Resultpage Logs..");
		 * 
		 * System.out.println("\n");
		 * 
		 * ExtractJSLogs();
		 * 
		 * 
		 * 
		 * Thread.sleep(2000);
		 * 
		 */

		/*
		 * final Screenshot screenshot4 = new AShot().shootingStrategy(new
		 * ViewportPastingStrategy(500)).takeScreenshot(driver);
		 * 
		 * final BufferedImage image4 = screenshot4.getImage();
		 * 
		 * ImageIO.write(image4, "PNG", new File(
		 * "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//2_Resultpage.png"
		 * ));
		 * 
		 */


		/*
		 * 
		 * //For View Details
		 * 
		 * if(driver.findElement(By.cssSelector(
		 * "[id*='flight-summary_'] > div.row.result-box-footer > div.col-lg-2.col-md-2.col-sm-3.col-xs-3.flight-details-box-right [id*='details_']"
		 * )).isDisplayed())
		 * 
		 * {
		 * 
		 * WebElement element = driver.findElement(By.cssSelector(
		 * "[id*='flight-summary_'] > div.row.result-box-footer > div.col-lg-2.col-md-2.col-sm-3.col-xs-3.flight-details-box-right [id*='details_']"
		 * ));
		 * 
		 * JavascriptExecutor js =(JavascriptExecutor)driver;
		 * 
		 * js.executeScript("window.scrollTo(0,'element.getLocation().y+')");
		 * 
		 * element.click();
		 * 
		 * 
		 * 
		 * Thread.sleep(500);
		 * 
		 * 
		 * 
		 * final Screenshot screenshot22 = new AShot().shootingStrategy(new
		 * ViewportPastingStrategy(500)).takeScreenshot(driver);
		 * 
		 * final BufferedImage image22 = screenshot22.getImage();
		 * 
		 * ImageIO.write(image22, "PNG", new File(
		 * "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//3_ViewDetails.png"
		 * ));
		 * 
		 * Thread.sleep(1000);
		 * 
		 * 
		 * 
		 * driver.findElement(By.cssSelector(
		 * "[id*='flight-summary_'] > div.row.result-box-footer > div.col-lg-2.col-md-2.col-sm-3.col-xs-3.flight-details-box-right [id*='details_']"
		 * )).click();
		 * 
		 * Thread.sleep(3000);
		 * 
		 * }
		 * 
		 * 
		 * 
		 * // For verify price on result page
		 * 
		 * String perperson = driver.findElement(By.cssSelector(
		 * "[id*='flight-summary_'] > div.row.result-box-header > div.col-md-4.col-sm-4.col-xs-12.result-box-header-left.pull-right > div > div.price-box.col-md-6.col-sm-6.col-xs-6 > h3.price"
		 * )).getText();
		 * 
		 * Reporter.log("Price per person on result page: " +perperson, true);
		 * 
		 * Thread.sleep(2000);
		 * 
		 * 
		 * 
		 * String totalpriceonresult =
		 * driver.findElement(By.cssSelector("[id*='Price_']")).getText();
		 * 
		 * Reporter.log("Total Price on result page: " +totalpriceonresult, true);
		 * 
		 * Thread.sleep(1000);
		 * 
		 */

		// for Air Matrix

		try

		{

			driver.findElement(By.xpath("//*[@id='MatrixView']/a")).click();

			Thread.sleep(800);

			/*
			 * final Screenshot screenshot04 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver);
			 * 
			 * final BufferedImage image04 = screenshot04.getImage();
			 * 
			 * ImageIO.write(image04, "PNG", new File(
			 * "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//003_AirMatrixpage.png"
			 * ));
			 * 
			 * Thread.sleep(2000);
			 */
			
			 WebDriver driver1 = new Augmenter().augment(driver);
			File file2  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file2, new File("E:\\Automation\\Alanita_Test\\003_AirMatrixpage.png"));
			
			String actual2 = "E:\\Automation\\Alanita_Test\\003_AirMatrixpage.png";
			String reference2 = "E:\\Automation\\Reference images\\Alanita\\003_AirMatrixpage.png";
			String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
			String Output2 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_003_AirMatrixpage.png";

			ProcessBuilder pb2 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output2, actual2, reference2, Log);
			pb2.directory(new File("E:\\node_modules\\.bin"));
			Process p2 = pb2.start();

			System.out.println("" + p2);

		}

		catch (Exception e)

		{

			e.getMessage();

		}

		// for Search

		try

		{

			driver.findElement(By.xpath("//*[@id='SearchForm']/a")).click();

			Thread.sleep(800);

			/*
			 * final Screenshot screenshot04 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver);
			 * 
			 * final BufferedImage image04 = screenshot04.getImage();
			 * 
			 * ImageIO.write(image04, "PNG", new File(
			 * "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//0003_AirSearchpage.png"
			 * ));
			 * 
			 * Thread.sleep(2000);
			 */
			WebDriver driver1 = new Augmenter().augment(driver);
			File file3  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file3, new File("E:\\Automation\\Alanita_Test\\0003_AirSearchpage.png"));
			
			String actual3 = "E:\\Automation\\Alanita_Test\\0003_AirSearchpage.png";
			String reference3 = "E:\\Automation\\Reference images\\Alanita\\0003_AirSearchpage.png";
			String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
			String Output3 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_0003_AirSearchpage.png";

			ProcessBuilder pb3 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output3, actual3, reference3, Log);
			pb3.directory(new File("E:\\node_modules\\.bin"));
			Process p3 = pb3.start();

			System.out.println("" + p3);

		}

		catch (Exception e)

		{

			e.getMessage();

		}

		// For select flight

		try

		{

			// For Navigate on List view

			driver.findElement(By.xpath("//*[@id='ListView']/a")).click();

			Thread.sleep(2000);

			/*driver.findElement(By.xpath("//div[@class='mo-flight-desc special']")).click();

			Thread.sleep(4000);*/

		}

		catch (Exception e)

		{

			e.getMessage();

		}

		// For details page

		/*File scr22 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		org.codehaus.plexus.util.FileUtils.copyFile(scr22,
				new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//003_detailspage.png"));

		Thread.sleep(1000);*/
		
		WebDriver driver1 = new Augmenter().augment(driver);
		File file4  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file4, new File("E:\\Automation\\Alanita_Test\\003_detailspage.png"));
		
		String actual4 = "E:\\Automation\\Alanita_Test\\003_detailspage.png";
		String reference4 = "E:\\Automation\\Reference images\\Alanita\\003_detailspage.png";
		String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
		String Output4 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_003_detailspage.png";

		ProcessBuilder pb4 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output4, actual4, reference4, Log);
		pb4.directory(new File("E:\\node_modules\\.bin"));
		Process p4 = pb4.start();

		System.out.println("" + p4);

		/*if (driver.findElement(By.cssSelector("[id*='details_'] > div.row.mo-flight-details > h2 > span [id*='book_']"))
				.isDisplayed())

		{

			driver.findElement(By.cssSelector("[id*='details_'] > div.row.mo-flight-details > h2 > span [id*='book_']"))
					.click();

			Thread.sleep(4000);

		}
		*/
		
//		if (driver.findElement(By.linkText(" Select")).isDisplayed())

//		{

		mywait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(" Select"))).click();
		System.out.println("Element visible");
		
		  driver.findElement(By.linkText(" Select"));
		  
		  System.out.println("Element select found");
		  Thread.sleep(3000);
		  
		  driver.findElement(By.linkText(" Select")).click();
		  
		  System.out.println("Element select clicked");


//		}
		
    
		
		if (driver.findElement(By.linkText("Book Now"))
				.isDisplayed())

		{

			driver.findElement(By.linkText("Book Now"))
					.click();

			Thread.sleep(4000);

		}

		long start = System.currentTimeMillis();

		Thread.sleep(1500);

		long finish = System.currentTimeMillis();

		long totalTime = finish - start;

		Reporter.log("Total Time for result page to checkout page load(Milisec) - " + totalTime, true);

		Thread.sleep(1000);

		/*
		 * final Screenshot screenshot3 = new AShot().shootingStrategy(new
		 * ViewportPastingStrategy(500)).takeScreenshot(driver);
		 * 
		 * final BufferedImage image3 = screenshot3.getImage();
		 * 
		 * ImageIO.write(image3, "PNG", new File(
		 * "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//4_Checkoutpage.png"
		 * ));
		 */
		Thread.sleep(7000);
		driver1 = new Augmenter().augment(driver);
		File file5  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file5, new File("E:\\Automation\\Alanita_Test\\4_Checkoutpage.png"));
		
		String actual5 = "E:\\Automation\\Alanita_Test\\4_Checkoutpage.png";
		String reference5 = "E:\\Automation\\Reference images\\Alanita\\4_Checkoutpage.png";
		//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
		String Output5 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_4_Checkoutpage.png";

		ProcessBuilder pb5 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output5, actual5, reference5, Log);
		pb5.directory(new File("E:\\node_modules\\.bin"));
		Process p5 = pb5.start();

		System.out.println("" + p5);
		
		System.out.println("Flight selected successfully..");


		// For Trip details on checkout page

//		String frmloc = driver.findElement(By.xpath("//*[@id='flight_details' or @class='flt-descprition']/div[1]/div/div[1]")).getText();

		String frmloc = driver.findElement(By.xpath("//*[@class='depart-seg']")).getText();

		Reporter.log("Departure Trip: " + frmloc, true);


//		String toloc = driver.findElement(By.xpath("//*[@id='flight_details' or @class='flt-descprition']/div[2]/div/div[1]")).getText();

		String toloc = driver.findElement(By.xpath("//*[@class='arrive-seg']")).getText();

		Reporter.log("Return Trip: " + toloc, true);


		// Price verify on checkout page

		String priceoncheckout = driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).getText();

		Reporter.log("Price on checkout page: " + priceoncheckout, true);


		driver.navigate().refresh();
		
		Thread.sleep(5000);
		driver.swipe(748, 616, 748, 1947, 288);
		driver.swipe(748, 616, 748, 1947, 288);
		driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).click();
		
		Thread.sleep(3000);
		
		driver1 = new Augmenter().augment(driver);
		File file15  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file15, new File("E:\\Automation\\Alanita_Test\\Price_on_Checkoutpage.png"));
		
		driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).click();
		/*
		 * 
		 * //For check API, Office id And Environment on checkout page
		 * 
		 * String expr = driver.findElement(By.id("TracerBlock")).getText();
		 * 
		 * String api = expr.split("OdysseyGateway")[7].split(":")[1].trim();
		 * 
		 * //List<String> items = Arrays.asList(expr.split(
		 * "$($('#TracerBlock')[0].innerHTML.split('OdysseyGateway')[2])[0].nodeValue.toString().split(':')[1]"
		 * ));
		 * 
		 * Reporter.log("API :- " +api, true);
		 * 
		 * Thread.sleep(500);
		 * 
		 * String envrnmnt =
		 * expr.split("OdysseyGateway")[7].split(":")[2].trim();
		 * 
		 * Reporter.log("Environment :- " +envrnmnt, true);
		 * 
		 * Thread.sleep(500);
		 * 
		 * String officeid =
		 * expr.split("OdysseyGateway")[7].split(":")[3].trim();
		 * 
		 * Reporter.log("Office Id :- " +officeid, true);
		 * 
		 * Thread.sleep(4000);
		 * 
		 */

		// For Passenger details

		try

		{

			System.out.println("Firstname_Of_Adult1: " + Firstname_Of_Adult1);

			System.out.println("Middlename_of_Adult1:" + Middlename_of_Adult1);

			System.out.println("Lastname_Of_Adult1: " + Lastname_Of_Adult1);

			System.out.println("Firstname_Of_Adult2: " + Firstname_Of_Adult2);

			System.out.println("Middlename_of_Adult2:" + Middlename_of_Adult2);

			System.out.println("Lastname_Of_Adult2: " + Lastname_Of_Adult2);

			System.out.println("Firstname_Of_Adult3: " + Firstname_Of_Adult3);

			System.out.println("Middlename_of_Adult3:" + Middlename_of_Adult3);

			System.out.println("Lastname_Of_Adult3: " + Lastname_Of_Adult3);

			System.out.println("Email: " + Email);

			System.out.println("Confirm_Email: " + Confirm_Email);

			System.out.println("Phone_No: " + Phone);

			// System.out.println("Destination_Phone: " + Destination_Phone);

			faredptpurchse.AlanitaBookingToTitle(Firstname_Of_Adult1, Middlename_of_Adult1, Lastname_Of_Adult1,
					Firstname_Of_Adult2, Middlename_of_Adult2, Lastname_Of_Adult2, Firstname_Of_Adult3,
					Middlename_of_Adult3, Lastname_Of_Adult3, Email, Confirm_Email, Phone, Destination_Phone);

		}

		catch (Exception e)

		{

			
			/*
			 * final Screenshot screenshot44 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver);
			 * 
			 * final BufferedImage image44 = screenshot44.getImage();
			 * 
			 * ImageIO.write(image44, "PNG", new File(
			 * "//Users//admin//Desktop//Odysseus//Odysseus project//FaredepotError//3_InvalidGuestInfo.png"
			 * ));
			 */
			
			driver1 = new Augmenter().augment(driver);
			File file6  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file6, new File("E:\\Automation\\Alanita_TestError\\3_InvalidGuestInfo.png"));

			Assert.assertFalse(false, "FAIL");

			Reporter.log("Invalid guest information on checkoutpage...", true);

			AssertJUnit.assertTrue("Invalid guest information on checkoutpage..", faredptpurchse.isDisplayed());

			throw (e);

		}

		Thread.sleep(6000);

		// For Insurance

		try

		{

			if (driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_GuestResidency_CountrySel")).isDisplayed())

			{

				Select country = new Select(driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_GuestResidency_CountrySel")));
						country.selectByVisibleText("United States");


						Select state = new Select(driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_GuestResidency_StateSel")));
						state.selectByVisibleText("California");
			

			

			}

			/*Thread.sleep(5000);

			// For check insurance price

			//String insuranceprice = driver
					//.findElement(By.xpath("//*[@id='section_insurance']/div[2]/div/div[2]/div[2]/div[3]/span[1]")).getText();

			String insuranceprice = driver.findElement(By.xpath("//span[@class='priceUp']")).getText();
			Reporter.log("Insurance Price:- " + insuranceprice, true);

			Thread.sleep(4000);*/

		}

		catch (Exception e)

		{

			e.getMessage();
			System.out.println(e);

		}

		try

		{

			if (driver.findElement(By.id("InsurranceCHK_239399")).isDisplayed())

			{

				driver.findElement(By.id("InsurranceCHK_239399")).click();

				Thread.sleep(4000);

				driver.context(Native);
				  driver.findElement(By.xpath("//*[@text='Pages']")).click();
				  driver.findElement(By.xpath("//*[@text='Close ‎webservices.travelguard.com/Product/FileRetrieval.aspx?CountryCode=US&StateCode=CA&ProductCode=CA9081&PlanCode=P1&FileType=PROD_PLAN_DOC']")).click();
				  driver.findElement(By.xpath("//*[@text='Select Air Passengers & Preferences : Alanita Travel']")).click();
				  driver.context(Webview);

				  Thread.sleep(5000);
				  driver1 = new Augmenter().augment(driver);
					File file7  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(file7, new File("E:\\Automation\\Alanita_Test\\7_AppliedInsurance.png"));
					
									
					String actual7 = "E:\\Automation\\Alanita_Test\\7_AppliedInsurance.png";
					String reference7 = "E:\\Automation\\Reference images\\Alanita\\7_AppliedInsurance.png";
					//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
					String Output7 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_7_AppliedInsurance.png";

					ProcessBuilder pb7 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output7, actual7, reference7, Log);
					pb7.directory(new File("E:\\node_modules\\.bin"));
					Process p7 = pb7.start();

					System.out.println("" + p7);
					
					Thread.sleep(5000);
					
				  // Switch to new window opened
				
				
				/*String parentWindow = driver2.getWindowHandle();
				Set<String> handles =  driver2.getWindowHandles();
				for(String windowHandle  : handles)
				{
				    if(!windowHandle.equals(parentWindow))
				   {
				     driver2.switchTo().window(windowHandle);
//				      <!--Perform your operation here for new window-->
				     driver2.close(); //closing child window
				     driver2.switchTo().window(parentWindow); //cntrl to parent window
				    }
				}*/
				  
				  
				  /*String winHandleBefore = driver.getWindowHandle();
				  
				  
				  
				  for(String winHandle : driver.getWindowHandles())
				  
				  {
				  
				  driver.switchTo().window(winHandle);
				  
				  }
				  
				  
				  
				  // Close the new window
				  
				  driver.close();
				  
				  
				  
				  driver.switchTo().window(winHandleBefore);*/
				  
				  
				  
				  Reporter.log("Insurance applied successfully..", true);
				  
				  
				  
				  Thread.sleep(1000);
				  
				 

				// For check insurance price

				String insurprice = driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).getText();

				Reporter.log("Prices of After applied insurance on the Payment page:- " + insurprice, true);

//For screenshot of prices after insurance is selected
				
				driver.swipe(748, 616, 748, 1947, 288);
				driver.swipe(748, 616, 748, 1947, 288);
				driver.swipe(748, 616, 748, 1947, 288);
				
				
				
				driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).click();
				
				Thread.sleep(5000);

				driver1 = new Augmenter().augment(driver);
				File file14  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(file14, new File("E:\\Automation\\Alanita_Test\\Price_on_Checkoutpage_with_insurance.png"));
				
				String actual14 = "E:\\Automation\\Alanita_Test\\Price_on_Checkoutpage_with_insurance.png";
				String reference14 = "E:\\Automation\\Reference images\\Alanita\\Price_on_Checkoutpage_with_insurance.png";
				//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
				String Output14 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_Price_on_Checkoutpage_with_insurance.png";

				ProcessBuilder pb14 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output14, actual14, reference14, Log);
				pb14.directory(new File("E:\\node_modules\\.bin"));
				Process p14 = pb14.start();

				System.out.println("" + p14);
				Thread.sleep(4000);

				/*
				 * final Screenshot screenshot64 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver);
				 * 
				 * final BufferedImage image64 = screenshot64.getImage();
				 * 
				 * ImageIO.write(image64, "PNG", new File(
				 * "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//7_AppliedInsurance.png"
				 * ));
				 */

				driver.swipe(748, 1947, 748, 616, 288);
				driver.swipe(748, 1947, 748, 616, 288);
				
				Thread.sleep(6000);

				// For Remove insurance

				WebElement element = driver.findElement(By.id("InsurranceCHK_NON"));

//				JavascriptExecutor js = (JavascriptExecutor) driver;
//
//				js.executeScript("window.scrollTo(0,'element.getLocation().y+')");

				element.click();

				Thread.sleep(3000);

				driver.findElement(By.xpath("//*[@id='DivTravlGuardPopup']/div[2]/div[1]/div[4]/a")).click();

				Thread.sleep(15000);

				Reporter.log("Insurance Removed successfully..", true);

				// For check insurance price

				String insprice = driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).getText();

				Reporter.log("Prices of After removed insurance on the Payment page:- " + insprice, true);

				Thread.sleep(3000);

				/*
				 * final Screenshot screenshot66 = new
				 * AShot().shootingStrategy(new
				 * ViewportPastingStrategy(500)).takeScreenshot(driver);
				 * 
				 * final BufferedImage image66 = screenshot66.getImage();
				 * 
				 * ImageIO.write(image66, "PNG", new File(
				 * "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//8_RemovedInsurance.png"
				 * ));
				 */

				
				//For screenshot of removed insurance selected
				driver1 = new Augmenter().augment(driver);
				File file8  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(file8, new File("E:\\Automation\\Alanita_Test\\8_RemovedInsurance.png"));
				
				String actual8 = "E:\\Automation\\Alanita_Test\\8_RemovedInsurance.png";
				String reference8 = "E:\\Automation\\Reference images\\Alanita\\8_RemovedInsurance.png";
				//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
				String Output8 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_8_RemovedInsurance.png";

				ProcessBuilder pb8 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output8, actual8, reference8, Log);
				pb8.directory(new File("E:\\node_modules\\.bin"));
				Process p8 = pb8.start();

				System.out.println("" + p8);
				
				Thread.sleep(4000);
				
				//For getting insurance price after removing insurance
				driver.swipe(748, 616, 748, 1947, 288);
				driver.swipe(748, 616, 748, 1947, 288);
				driver.swipe(748, 616, 748, 1947, 288);
				
				driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).click();
				
				Thread.sleep(3000);
				driver1 = new Augmenter().augment(driver);
				File file16  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(file16, new File("E:\\Automation\\Alanita_Test\\Price_on_Checkoutpage_after_insurance_removed.png"));
				
				String actual16 = "E:\\Automation\\Alanita_Test\\Price_on_Checkoutpage_after_insurance_removed.png";
				String reference16 = "E:\\Automation\\Reference images\\Alanita\\Price_on_Checkoutpage_after_insurance_removed.png";
				//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
				String Output16 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_Price_on_Checkoutpage_after_insurance_removed.png";

				ProcessBuilder pb16 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output16, actual16, reference16, Log);
				pb16.directory(new File("E:\\node_modules\\.bin"));
				Process p16 = pb16.start();

				System.out.println("" + p16);
				
				//driver.findElement(By.xpath("//*[@id='trip_summary_package_total']")).click();

				driver.swipe(748, 1947, 748, 616, 288);
				driver.swipe(748, 1947, 748, 616, 288);
				
				Thread.sleep(4000);

			}

		}

		catch (Exception e)
		
		{

			
			System.out.println("Insurance not applied/removed...");

			System.out.println("\n");

			System.out.println("Insurance not applied/removed on confirmation page...");

			System.out.println("\n");

			
			/*
			 * final Screenshot screenshot72 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver);
			 * 
			 * final BufferedImage image72 = screenshot72.getImage();
			 * 
			 * ImageIO.write(image72, "PNG", new File(
			 * "//Users//admin//Desktop//Odysseus//Odysseus project//FaredepotError//9_InsurancenotAppliedOrRemoved.png"
			 * ));
			 */
			
			driver1 = new Augmenter().augment(driver);
			File file9  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file9, new File("E:\\Automation\\Alanita_TestError\\9_InsurancenotAppliedOrRemoved.png"));

			Assert.assertFalse(false, "FAIL");

			Reporter.log("Insurance not applied/removed...", true);

			AssertJUnit.assertTrue("Insurance not applied/removed....", faredptpurchse.isDisplayed());

			System.out.println(e);
			throw (e);

		}

		// For Additional Services

		try

		{

			driver.findElement(By.xpath("//*[@css='INPUT.spc-cont-btn.primary-btn.fl-right-imp']")).click();
			Thread.sleep(1000);

//			File scr03 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//
//			org.codehaus.plexus.util.FileUtils.copyFile(scr03, new File(
//					"//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//9_AdditionalServiceOnCheckoutpage.png"));
			
			driver1 = new Augmenter().augment(driver);
			File file10  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file10, new File("E:\\Automation\\Alanita_Test\\9_AdditionalServiceOnCheckoutpage.png"));
			
			String actual10 = "E:\\Automation\\Alanita_Test\\9_AdditionalServiceOnCheckoutpage.png";
			String reference10 = "E:\\Automation\\Reference images\\Alanita\\9_AdditionalServiceOnCheckoutpage.png";
			//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
			String Output10 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_9_AdditionalServiceOnCheckoutpage.png";

			ProcessBuilder pb10 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output10, actual10, reference10, Log);
			pb10.directory(new File("E:\\node_modules\\.bin"));
			Process p10 = pb10.start();

			System.out.println("" + p10);

			Thread.sleep(1500);

		}

		catch (Exception e)

		{

			/*
			 * final Screenshot screenshot74 = new AShot().shootingStrategy(new
			 * ViewportPastingStrategy(500)).takeScreenshot(driver);
			 * 
			 * final BufferedImage image74 = screenshot74.getImage();
			 * 
			 * ImageIO.write(image74, "PNG", new File(
			 * "//Users//admin//Desktop//Odysseus//Odysseus project//FaredepotError//10_ErrorOnAdditionalService.png"
			 * ));
			 * 
			 * Thread.sleep(1000);
			 */
			
			driver1 = new Augmenter().augment(driver);
			File file11  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file11, new File("E:\\Automation\\Alanita_TestError\\10_ErrorOnAdditionalService.png"));

			AssertJUnit.assertTrue("Additional Service not working or showing error on additional service page",
					faredptpurchse.isDisplayed());
			System.out.println(e);
			throw (e);

		}

		// For Payment Information

		try

		{

			driver.findElement(By.xpath("//*[@css='INPUT.spc-cont-btn.primary-btn.fl-right-imp']")).click();

			Thread.sleep(2000);

			
			  /*final Screenshot screenshot76 = new AShot().shootingStrategy(new
			  ViewportPastingStrategy(500)).takeScreenshot(driver);
			  
			  final BufferedImage image76 = screenshot76.getImage();
			  
			  ImageIO.write(image76, "PNG", new File(
			  "//Users//admin//Desktop//Odysseus//Odysseus project//Faredepot//11_PaymentInfoPage.png"
			  ));*/
			  
			driver1 = new Augmenter().augment(driver);
			File file12  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file12, new File("E:\\Automation\\Alanita_Test\\11_PaymentInfoPage.png"));
			
			String actual12 = "E:\\Automation\\Alanita_Test\\11_PaymentInfoPage.png";
			String reference12 = "E:\\Automation\\Reference images\\Alanita\\11_PaymentInfoPage.png";
			//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
			String Output12 = "E:\\Automation\\Difference of Images\\Alanita\\Difference_11_PaymentInfoPage.png";

			ProcessBuilder pb12 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output12, actual12, reference12, Log);
			pb12.directory(new File("E:\\node_modules\\.bin"));
			Process p12 = pb12.start();

			System.out.println("" + p12);
			
			  Thread.sleep(2000);
			 

		}

		catch (Exception e)

		{

			
			 /* final Screenshot screenshot77 = new AShot().shootingStrategy(new
			  ViewportPastingStrategy(500)).takeScreenshot(driver);
			  
			  final BufferedImage image77 = screenshot77.getImage();
			  
			  ImageIO.write(image77, "PNG", new File(
			  "//Users//admin//Desktop//Odysseus//Odysseus project//FaredepotError//10_ErrorOnPaymentInfoPage.png"
			  ));
			  
			  Thread.sleep(1000);*/
			
			driver1 = new Augmenter().augment(driver);
			File file13  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file13, new File("E:\\Automation\\Alanita_TestError\\10_ErrorOnPaymentInfoPage.png"));
			 

			AssertJUnit.assertTrue("Payment information tab is not working or showing error on payment page",
					faredptpurchse.isDisplayed());
			System.out.println(e);
			throw (e);

			
		}

		/*
		 * 
		 * // Price verify for the checkout page to result page
		 * 
		 * if(priceoncheckout.equals(totalpriceonresult))
		 * 
		 * {
		 * 
		 * System.out.println("Result page and Checkout page's Price matched");
		 * 
		 * Reporter.log("Result page and Checkout page's Price matched..", true);
		 * 
		 * 
		 * 
		 * System.out.println("Actual price is:- " +priceoncheckout);
		 * 
		 * Reporter.log("Actual price is:- " +priceoncheckout, true);
		 * 
		 * }
		 * 
		 * else
		 * 
		 * {
		 * 
		 * System.out.println(
		 * "Result page and Checkout page's Price not match..");
		 * 
		 * Reporter.log("Result page and Checkout page's Price not match..", true);
		 * 
		 * System.out.println("Actual price is:- " +priceoncheckout);
		 * 
		 * Reporter.log("Actual price is:- " +priceoncheckout, true);
		 * 
		 * }
		 * 
		 * 
		 */ 
		  Thread.sleep(1500);
		  
		  
		  
		  //For verify prices on payment page (Payment page to Checkout page)
		  
		  String priceonpaymnt = driver.findElement(By.id("PaymentAmountToPay")).getText();
		  
		  Reporter.log("Price on payment page: " +priceonpaymnt, true);
		  
		  
		  
		  if(priceonpaymnt.equals(priceoncheckout))
		  
		  {
		  
		  System.out.println("Checkout page and Payment page's Price matched");
		  
		  Reporter.log("Checkout page and Payment page's Price matched..", true);
		  
		  
		  Reporter.log("Actual price is:- " +priceoncheckout, true);
		  
		  }
		  
		  else
		  
		  {
		  
		  System.out.println(
		  "Checkout page and Payment page's Price not match..");
		  
		  Reporter.log("Checkout page and Payment page's Price not match..", true);
		  
	  
		  Reporter.log("Actual price is:- " +priceoncheckout, true);
		  
		  }
		  
		  
		  
		  Thread.sleep(1500);
		  
		 

	}

	@DataProvider

	public String[][] Authentication() throws Exception

	{

		String[][] testObjArray = AirUtils.getTableArray(
				"E:\\Automation\\Workspace\\Odysseus_Responsive\\src\\main\\java\\testData\\AlanitaData.xlsx",
				"Sheet1");

		return testObjArray;

	}


	@AfterTest

	public void tearDown() {
		try

		{
			driver.quit();
		} catch (NullPointerException e) {
			e.getMessage();
		}

	}

}
