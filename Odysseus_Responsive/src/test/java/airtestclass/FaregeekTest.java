package airtestclass;



import java.awt.image.BufferedImage;

import java.io.File;

import java.io.IOException;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.tools.ant.taskdefs.WaitFor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;

import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.remote.RemoteWebDriver;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import org.testng.AssertJUnit;

import org.testng.Reporter;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import org.testng.annotations.Test;

import com.experitest.client.Client;

import PageObject.Bookingpgeobjct;
import PageObject.Purchasepgeobjct;
import Utility.AirUtils;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import ru.yandex.qatools.ashot.AShot;

import ru.yandex.qatools.ashot.Screenshot;

import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;





public class FaregeekTest

{

			DesiredCapabilities dc = new DesiredCapabilities();

	        Bookingpgeobjct airbkpge;

	        Purchasepgeobjct faregkpurchse;

	        String Log = "E:\\Automation\\Result-log\\Faregeek.txt";
	        
	    	Reporter report = new Reporter();

	    	private String reportDirectory = "reports";
	    	private String reportFormat = "xml";
	    	private String testName = "Faregeek(B2C)";
	    	static WebDriver wd;
	    	public IOSDriver<IOSElement> driver;
	    	public RemoteWebDriver driver2;
	    	
	    	public Client client = null;

	    	int scrollTimeout = 500;
	    	int header = 98;
	    	int footer = 0;
	    	float dpr = 2;
	    	
	    	String Native="NATIVE_APP";
	    	String Webview="WEBVIEW_1";

	    	public boolean implicitwait(long time)

	    	{

	    		try

	    		{

	    			driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);

	    			System.out.println("Waited for" + time + "sec implicitly.");

	    		} catch (Exception e)

	    		{

	    			System.out.println(e.getMessage());

	    		}

	    		return true;

	    	}



	     @BeforeTest

	              

	    	 public void baseClass() throws InterruptedException, IOException

	    		{

	    			try {
	    				dc.setCapability("deviceName", "iPhone 6+");
	    				dc.setCapability(MobileCapabilityType.UDID, "c0009b556ca641c9f3f9932c327180bd5f671680");

	    				dc.setCapability("platformName", "iOS");
	    				dc.setCapability("platformVersion", "10.3.3");
	    				// dc.setCapability("bundleId", "com.apple.mobilesafari");

	    				// dc.setCapability(MobileCapabilityType.APP, "Safari");
	    				dc.setBrowserName(MobileBrowserType.SAFARI);
	    				// driver = new IOSDriver<IOSElement>(new
	    				// URL("http://0.0.0.0:8888/wd/hub"), dc);

	    				// Earl's PC
	//    				 driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.75:8888/wd/hub"), dc);

	    				// Rutuja's PC
//	    				driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.131:4723/wd/hub"), dc);
	    				 
	    				 //Chintan's PC
	    				 driver = new IOSDriver<IOSElement>(new URL("http://192.168.1.55:4723/wd/hub"), dc);
	    			
	    				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

	    				airbkpge = PageFactory.initElements(driver, Bookingpgeobjct.class);

	    				faregkpurchse = PageFactory.initElements(driver, Purchasepgeobjct.class);

	    				dc.setCapability("reportDirectory", reportDirectory); //
	    				dc.setCapability("reportFormat", reportFormat);
	    				dc.setCapability("testName", testName);
	    				dc.setCapability("autoAcceptAlerts", true);

	    				// driver = new IOSDriver<IOSElement>(new
	    				// URL("http://localhost:4724/wd/hub"), dc);
	    				driver.get("http://faregeek.com/");

	    				Thread.sleep(3000);
	    			} catch (Exception e) {
	    				e.getMessage();
	    			}

	    		
	    		}

    

             //Save console

      //       String testResultFile="//Users//admin//Desktop//Odysseus//Odysseus project//ConsoleError//FaregeekError.txt";

      //       File file = new File(testResultFile);  

      //       FileOutputStream fis = new FileOutputStream(file);  

      //       PrintStream out = new PrintStream(fis);  

      //       System.setOut(out); 

                  

       //      Thread.sleep(1000);

            

            /* final Screenshot screenshot1 = new AShot().shootingStrategy(new ViewportPastingStrategy(1000)).takeScreenshot(driver);

             final BufferedImage image1 = screenshot1.getImage();

             ImageIO.write(image1, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//1_Searchpage.png"));

             Thread.sleep(15000);*/

       //      System.out.println("\n");

	   //      System.out.println("Searchpage Logs..");

	   //      System.out.println("\n");

	   //      ExtractJSLogs();



            //driver.navigate().to("https://localhost/odyssey/website/air/results.aspx?");

           

/*

	    public void ExtractJSLogs()

	    {

	         LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);



	           for (LogEntry entry : logEntries) 

	           {

	                System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());

	           }

	    }

*/

	    @Test(dataProvider="Authentication")

	    public void Fregeek_Data(String FromLocation, String ToLocation, String Firstname_Of_Adult1, String Middlename_of_Adult1, String Lastname_Of_Adult1, String Phone_Adult1, String Email, String Confirm_Email, String Firstname_Of_Adult2, String Middlename_of_Adult2, String Lastname_Of_Adult2, String Firstname_Of_Adult3, String Middlename_of_Adult3, String Lastname_Of_Adult3, String Phone_Adult3) throws Exception

	    {

	    	WebDriverWait mywait = new WebDriverWait(driver, 30);

	    	   try

	    	   {

	    		     //For web site and booking details

	                 Reporter.log("Website Name :- Faregeek", true);

	                 Thread.sleep(200);

	     		 

	                 System.out.println("FromLocation: " + FromLocation);

	                 System.out.println("ToLocation: " + ToLocation);

	                 
	                 airbkpge.BookingToTitle(FromLocation, ToLocation);
	                 
	                 
	                 mywait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='AirSearchForm']/div/div[2]/div/dl[6]/dd/a[1]/span")));
	                 driver.findElement(By.xpath("//*[@id='AirSearchForm']/div/div[2]/div/dl[6]/dd/a[1]/span")).click();
	                 
	                 Thread.sleep(5000);
	                 
	                 driver.get("https://air.faregeek.com/web/air/results.aspx?showtrace=true");
	                 
	                 Thread.sleep(10000);

	                 //For noting time from traces when showtrace=true
	                 /*try
	                 {
	                 	if(driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).isDisplayed())
	                 	{
	                 		//String ResultpageLoad = driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).getText();
	                 		List<IOSElement> list = driver.findElements(By.id("TracerBlock"));
	                 		int listcount = list.size();    
	                 		@SuppressWarnings("rawtypes")
	         				List optionsList = null;
	                 		String options[] = new String[listcount];

	                 		for (int i=1; i<=listcount; i++) 
	                 		{                     
	                 		    options[i-1] = driver.findElement(By.id("TracerBlock")).getText();
	                 		    //contains three words eg: Test Engineer Madhu               
	                 		    String[] expectedOptions = options[i-1].split(" ");
	                 		    //splitting based on the space           
	                 		    // I hope the next line gives you the correct lastName
	                 		    String lastName =expectedOptions[expectedOptions.length-1]; 
	                 		    // lets store the last name in the array
	                 		    options[i-1] = lastName;        
	                 		 }         
	                 		 optionsList = Arrays.asList(options);
	                 		 Reporter.log("Total Time for search page to result page load(Milisec) - " +optionsList, true);
	                 		
	                 		 Thread.sleep(1000);
	                 		
	                 	}
	                 	
	                 }
	                 catch(Exception e)
	                 {
	                 	e.getMessage();
	                 }*/

	                 

	         //	     System.out.println("\n");

	    	 //        System.out.println("Resultpage Logs..");

	    	 //        System.out.println("\n");

	    	 //        ExtractJSLogs();

	            

	                 Thread.sleep(15000);

	                 WebDriver driver1 = new Augmenter().augment(driver);
	     			File file1  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	     			FileUtils.copyFile(file1, new File("E:\\Automation\\Faregeek_Test\\Listview_ResultPage.png"));
	     			
	     			// For Image comparison
	     						String actual1 = "E:\\Automation\\Faregeek_Test\\Listview_ResultPage.png";
	     						String reference1 = "E:\\Automation\\Reference images\\Faregeek\\Listview_ResultPage.png";
	     						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
	     						String Output1 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Listview_ResultPage.png";

	     						ProcessBuilder pb1 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output1, actual1, reference1, Log);
	     						
	     						pb1.directory(new File("E:\\node_modules\\.bin"));
	     						Process p1 = pb1.start();

	     						System.out.println("" + p1);

	           }

	           catch(Exception e)

	           {

	        	     e.getMessage();

	        	     

	    	 

	 	            /* final Screenshot screenshot33 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

	 	             final BufferedImage image33 = screenshot33.getImage();

	 	             ImageIO.write(image33, "PNG", new File("D:\\Ajit\\Script_SS\\FaregeekError\\1_TimeoutORinvalidsearchdetails.png"));*/

	   	             WebDriver driver1 = new Augmenter().augment(driver);
	   	             File file  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	   	             FileUtils.copyFile(file, new File("E:\\Automation\\Faregeek_TestError\\1_TimeoutORinvalidsearchdetails.png"));

	    	         Assert.assertFalse(false, "FAIL");

	    	         Reporter.log("Time out or Invalid search criteria on resultpage..", true);

	    	         AssertJUnit.assertTrue("Time out or Invalid search criteria on resultpage...", airbkpge.isDisplayed());

	    	         throw(e);

	           }

	    	   

	    	 

	    	 // For select dynamic flights from result page

	/*         

	    	        driver.navigate().refresh();

	          

	                driver.get("https://air.faregeek.com/web/air/results.aspx?showtrace=true");

	      

	    	        driver.manage().window().maximize();

	    	        

	    	        System.out.println("\n");

	    	        System.out.println("Resultpage Logs..");

	    	        System.out.println("\n");

	    	        ExtractJSLogs();

	    	        

	                Thread.sleep(1000);

	   */ 

	               /* final Screenshot screenshot4 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

	                final BufferedImage image4 = screenshot4.getImage();

	                ImageIO.write(image4, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//2_Resultpage.png"));*/

	        

	    	        Thread.sleep(4000);

	    /*	        

	    	        //For View Details 

	    	        if(driver.findElement(By.cssSelector("[id*='flight-summary_'] > div.col-md-12.links > a.details")).isDisplayed())

	    	        {

	    	        	  driver.findElement(By.cssSelector("[id*='flight-summary_'] > div.col-md-12.links > a.details")).click();

	    	        	  Thread.sleep(500);

	  	    	        

	  	                  File scr02 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

       	                  org.codehaus.plexus.util.FileUtils.copyFile(scr02, new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//3_ViewDetails.png")); 

	  	                  Thread.sleep(2000);

	  	                

	  	                  driver.navigate().refresh();

	  	                  Thread.sleep(3000);

	    	        }

	    */	        

	    	        // For verify price on result page

	    	        String perperson = driver.findElement(By.xpath("//*[contains(@class, 'price')][contains(@data-popover, 'true')]")).getText();

	    	        Reporter.log("Price per person on result page: " +perperson, true);

	    	        Thread.sleep(1000);

	    	        

	    	        /*String totalpriceonresult = driver.findElement(By.cssSelector("#tootltipdetails")).getText();

	    	        Reporter.log("Total Price on result page: " +totalpriceonresult, true);

	    	        Thread.sleep(1000);*/

	    	        

	    	     //For Airline Matrix

	    	     try

	    	     {

	    	    	 driver.findElement(By.xpath("//*[@id='MatrixView']/a")).click();

	    	    	 Thread.sleep(1000);
	    	    	 Reporter.log("Airline Matrix works fine", true);
	    	    	 

	    	    	   /* final Screenshot screenshot04 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

		                final BufferedImage image04 = screenshot04.getImage();

		                ImageIO.write(image04, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//02_AirlineMatrixpage.png"));*/

	    	    	 Thread.sleep(2000);

	                 WebDriver driver1 = new Augmenter().augment(driver);
	     			File file2  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	     			FileUtils.copyFile(file2, new File("E:\\Automation\\Faregeek_Test\\AirMatrixpage.png"));
	     			
	     			// For Image comparison
	     						String actual2 = "E:\\Automation\\Faregeek_Test\\AirMatrixpage.png";
	     						String reference2 = "E:\\Automation\\Reference images\\Faregeek\\AirMatrixpage.png";
	     						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
	     						String Output2 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_AirMatrixpage.png";

	     						ProcessBuilder pb2 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output2, actual2, reference2, Log);
	     						
	     						pb2.directory(new File("E:\\node_modules\\.bin"));
	     						Process p2 = pb2.start();

	     						System.out.println("" + p2);
		        

		    	        Thread.sleep(2000);

	    	     }

	    	     catch(Exception e)

	    	     {

	    	    	 e.getMessage();

	    	     }

	    	     

	    	   //For Modify Search

	    	     try

	    	     {

	    	    	 driver.findElement(By.xpath("//*[@id='SearchForm']/a")).click();

	    	    	 Thread.sleep(1000);
	    	    	 Reporter.log("Modify Search works fine", true);
	    	    	 

	    	    	   /* final Screenshot screenshot04 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

		                final BufferedImage image04 = screenshot04.getImage();

		                ImageIO.write(image04, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//002_ModifySearchpage.png"));*/

	    	    	 Thread.sleep(2000);

	                 WebDriver driver1 = new Augmenter().augment(driver);
	     			File file3  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	     			FileUtils.copyFile(file3, new File("E:\\Automation\\Faregeek_Test\\AirSearchpage.png"));
	     			
	     			// For Image comparison
	     						String actual3 = "E:\\Automation\\Faregeek_Test\\AirSearchpage.png";
	     						String reference3 = "E:\\Automation\\Reference images\\Faregeek\\AirSearchpage.png";
	     						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
	     						String Output3 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_AirSearchpage.png";

	     						ProcessBuilder pb3 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output3, actual3, reference3, Log);
	     						
	     						pb3.directory(new File("E:\\node_modules\\.bin"));
	     						Process p3 = pb3.start();

	     						System.out.println("" + p3);
		        

		    	        Thread.sleep(2000);

	    	     }

	    	     catch(Exception e)

	    	     {

	    	    	 e.getMessage();

	    	     }

	    	     

	    	   //For Filter

	    	     try

	    	     {

	    	    	 driver.findElement(By.xpath("//*[@id='FilterView']/a")).click();

	    	    	 Thread.sleep(1000);

	    	    	 Reporter.log("Filter works fine", true);

	    	    	    /*final Screenshot screenshot04 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

		                final BufferedImage image04 = screenshot04.getImage();

		                ImageIO.write(image04, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//0002_Filterpage.png"));*/

		        
	    	    	 Thread.sleep(2000);

	                 WebDriver driver1 = new Augmenter().augment(driver);
	     			File file4  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	     			FileUtils.copyFile(file4, new File("E:\\Automation\\Faregeek_Test\\Filterview.png"));
	     			
	     			// For Image comparison
	     						String actual4 = "E:\\Automation\\Faregeek_Test\\Filterview.png";
	     						String reference4 = "E:\\Automation\\Reference images\\Faregeek\\Filterview.png";
	     						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
	     						String Output4 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Filterview.png";

	     						ProcessBuilder pb4 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output4, actual4, reference4, Log);
	     						
	     						pb4.directory(new File("E:\\node_modules\\.bin"));
	     						Process p4 = pb4.start();

	     						System.out.println("" + p4);

		    	        Thread.sleep(2000);

	    	     }

	    	     catch(Exception e)

	    	     {

	    	    	 e.getMessage();

	    	     }

	    	  

	          // For select flight on result page

	    	  try

	    	  {

	    		  //Navigate on List view

	    		  driver.findElement(By.xpath("//*[@id='ListView']/a")).click();

	    		  Thread.sleep(1000);

	    		  Reporter.log("Navigated back to listview", true);

	             if(driver.findElement(By.xpath("//a[contains(@href, '#flight-details_0')] [contains(@class, 'm-select-button visible-xs')]")).isDisplayed())

	             {

	            	   driver.findElement(By.xpath("//a[contains(@href, '#flight-details_0')] [contains(@class, 'm-select-button visible-xs')]")).click();

	            	   Thread.sleep(7000);

	             }

	             /*else if(driver.findElement(By.cssSelector("#mainBody > div.error-box.priceErr")).isDisplayed()) // For error handling

          	     {

          		       driver.navigate().refresh();

          		       driver.findElement(By.cssSelector("[id*='flight-summary_'] > div.col-md-2.no-padding.pull-right.fix-price > div > a")).click();

	            	   Thread.sleep(6000);

          	     }

	             else if(driver.findElement(By.cssSelector("#mainBody > div.error-box.priceErr")).isDisplayed())

	             {

	            	  String errormess = driver.findElement(By.cssSelector("#mainBody > div.error-box.priceErr")).getText();

	            	  Reporter.log("Error on result page: " +errormess, true);

	            	  Thread.sleep(500);

	            	  AssertJUnit.assertTrue(" " +errormess, airbkpge.isDisplayed());

		    		  return;

	             }*/



	             else 

	             {

	             	System.out.println("Flight not available..");

	             	System.out.println("\n");

	             	System.out.println("Flight not available on resultpage Logs..");

	             	System.out.println("\n");

	         //   	ExtractJSLogs();

	              	

	             	/*final Screenshot screenshot2 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

	                final BufferedImage image2 = screenshot2.getImage();

	                ImageIO.write(image2, "PNG", new File("D:\\Ajit\\Script_SS\\FaregeekError\\2_Flightnotavailable.png"));*/

	             	WebDriver driver1 = new Augmenter().augment(driver);
	    			File file  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	    			FileUtils.copyFile(file, new File("E:\\Automation\\Faregeek_TestError\\2_Flightnotavailable.png"));

	                Assert.assertFalse(false, "FAIL");

	    	        Reporter.log("Flights not available..", true);

	    			AssertJUnit.assertTrue("Flight not available...", airbkpge.isDisplayed());

	    			return;

	    		 }

	    	  }

	    	  catch(Exception e)

	    	  {

	    		  e.getMessage();

	    	  }

	    	  

	                //For details page

	               /* final Screenshot screenshot40 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

	                final BufferedImage image40 = screenshot40.getImage();

	                ImageIO.write(image40, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//3_Detailspage.png"));
*/
	        

	    	        Thread.sleep(3000);

	    	        

	    	        
	            	
	            	Thread.sleep(2000);

	                 WebDriver driver1 = new Augmenter().augment(driver);
	     			File file5  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	     			FileUtils.copyFile(file5, new File("E:\\Automation\\Faregeek_Test\\Booknowpage.png"));
	     			
	     			// For Image comparison
	     						String actual5 = "E:\\Automation\\Faregeek_Test\\Booknowpage.png";
	     						String reference5 = "E:\\Automation\\Reference images\\Faregeek\\Booknowpage.png";
	     						String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
	     						String Output5 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Booknowpage.png";

	     						ProcessBuilder pb5 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output5, actual5, reference5, Log);
	     						
	     						pb5.directory(new File("E:\\node_modules\\.bin"));
	     						Process p5 = pb5.start();

	     						System.out.println("" + p5);
	     						Thread.sleep(4000);
	     						
	             driver.findElement(By.xpath("//a[@class='col-xs-3 m-book-btn pull-right' or @text='Book']")).click();

	            	
	           //For noting time from traces when showtrace=true
                 /*
	             try
                 {
                 	if(driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).isDisplayed())
                 	{
                 		//String ResultpageLoad = driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).getText();
                 		List<IOSElement> list = driver.findElements(By.id("TracerBlock"));
                 		int listcount = list.size();    
                 		@SuppressWarnings("rawtypes")
         				List optionsList = null;
                 		String options[] = new String[listcount];

                 		for (int i=1; i<=listcount; i++) 
                 		{                     
                 		    options[i-1] = driver.findElement(By.id("TracerBlock")).getText();
                 		    //contains three words eg: Test Engineer Madhu               
                 		    String[] expectedOptions = options[i-1].split(" ");
                 		    //splitting based on the space           
                 		    // I hope the next line gives you the correct lastName
                 		    String lastName =expectedOptions[expectedOptions.length-1]; 
                 		    // lets store the last name in the array
                 		    options[i-1] = lastName;        
                 		 }         
                 		 optionsList = Arrays.asList(options);
                 		 Reporter.log("Total Time for details page to checkout page load(Milisec) - " +optionsList, true);
                 		
                 		 Thread.sleep(1000);
                 		
                 	}
                 	
                 }
                 catch(Exception e)
                 {
                 	e.getMessage();
                 } */

	                

	                              

	                

	                

/*	                final Screenshot screenshot3 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

	                final BufferedImage image3 = screenshot3.getImage();

	                ImageIO.write(image3, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//4_Checkoutpage.png"));
*/
	                

	               

	     /*           

	              //For Trip details on checkout page

		  	      String frmloc = driver.findElement(By.cssSelector("#Flights > div:nth-child(1) > div > div.col-md-10.col-sm-10")).getText();

		  	      Reporter.log("Departure Trip: " +frmloc, true);

		  	      Thread.sleep(500);

		  	      String toloc = driver.findElement(By.cssSelector("#Flights > div:nth-child(2) > div > div.col-md-10.col-sm-10")).getText();

		  	      Reporter.log("Return Trip: " +toloc, true);

		  	      Thread.sleep(500);

		  	      String noofadult = driver.findElement(By.cssSelector("#FareDetails > div.panel-body.fare-details > div:nth-child(1) > div > p")).getText();

		  	      Reporter.log(" " +noofadult, true);

		  	      Thread.sleep(500);

		  	      String noofchilds = driver.findElement(By.cssSelector("#FareDetails > div.panel-body.fare-details > div:nth-child(2) > div > p")).getText();

		  	      Reporter.log(" " +noofchilds, true);

		  	      Thread.sleep(500);

		  	      String taxes = driver.findElement(By.cssSelector("#FareDetails > div.total-price.no-padding > div:nth-child(1) > div")).getText();

		  	      Reporter.log(" " +taxes, true);

		  	      Thread.sleep(500);

	   */        

	           //For Price summary

	    	     try

	    	     {

	    	    	 driver.findElement(By.xpath("//*[@id='PriceInfo']/a")).click();

	    	    	 Thread.sleep(1000);

	    	    	 

/*	    	    	    final Screenshot screenshot04 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

		                final BufferedImage image04 = screenshot04.getImage();

		                ImageIO.write(image04, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//04_Pricesummarypage.png"));
*/
	    	    	 Thread.sleep(2000);

	                  driver1 = new Augmenter().augment(driver);
	     			File file6  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	     			FileUtils.copyFile(file6, new File("E:\\Automation\\Faregeek_Test\\Pricesummarypage.png"));
	     			
	     			// For Image comparison
	     						String actual6 = "E:\\Automation\\Faregeek_Test\\Pricesummarypage.png";
	     						String reference6 = "E:\\Automation\\Reference images\\Faregeek\\Pricesummarypage.png";
	     						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
	     						String Output6 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Pricesummarypage.png";

	     						ProcessBuilder pb6 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output6, actual6, reference6, Log);
	     						
	     						pb6.directory(new File("E:\\node_modules\\.bin"));
	     						Process p6 = pb6.start();

	     						System.out.println("" + p6);

		    	        Thread.sleep(2000);

		    	        

		    	      //Price verify on checkout page

			  	      	  String priceoncheckout = driver.findElement(By.xpath("//*[@id='PricesGTotal']")).getText();

			  	      	  Reporter.log("Price on checkout page: " +priceoncheckout, true);

			  	      	  Thread.sleep(1000);

	    	     }

	    	     catch(Exception e)

	    	     {

	    	    	 e.getMessage();

	    	     }

	    	     

	    	   //For Flight details

	    	     try

	    	     {

	    	    	 driver.findElement(By.xpath("//*[@id='FlightInfo']/a")).click();

	    	    	 

	    	    	 
/*
	    	    	    final Screenshot screenshot04 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

		                final BufferedImage image04 = screenshot04.getImage();

		                ImageIO.write(image04, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//004_Flightdetailspage.png"));
*/					Thread.sleep(2000);
	    	    	 driver1 = new Augmenter().augment(driver);
		     			File file7  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
		     			FileUtils.copyFile(file7, new File("E:\\Automation\\Faregeek_Test\\Flightdetailspage.png"));
		     			
		     			// For Image comparison
		     						String actual7 = "E:\\Automation\\Faregeek_Test\\Flightdetailspage.png";
		     						String reference7 = "E:\\Automation\\Reference images\\Faregeek\\Flightdetailspage.png";
		     						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
		     						String Output7 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Flightdetailspage.png";

		     						ProcessBuilder pb7 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output7, actual7, reference7, Log);
		     						
		     						pb7.directory(new File("E:\\node_modules\\.bin"));
		     						Process p7 = pb7.start();

		     						System.out.println("" + p7);

			    	        
		    	        Thread.sleep(2000);

		 /*   	        

		    	      // Click on flight details

		    	          driver.findElement(By.cssSelector("#flight-summary_0 > div.row > a.m-select.visible-xs > span")).click();

			  	      	  Thread.sleep(1000);

			  	      	  

			  	      	final Screenshot screenshot004 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

		                final BufferedImage image004 = screenshot004.getImage();

		                ImageIO.write(image004, "PNG", new File("//Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//0004_Flightdetails.png"));

		                Thread.sleep(1000);

	    */	                

	    	     }

	    	     catch(Exception e)

	    	     {

	    	    	 e.getMessage();

	    	     }

	              

	              

	  	/*      	 

	  	      	  //For check API, Office id And Environment on checkout page

	  	      	  String expr = driver.findElement(By.id("TracerBlock")).getText();

	  	          String api = expr.split("OdysseyGateway")[7].split(":")[1].trim();

	  	          //List<String> items = Arrays.asList(expr.split("$($('#TracerBlock')[0].innerHTML.split('OdysseyGateway')[2])[0].nodeValue.toString().split(':')[1]"));

	  	          Reporter.log("API :- " +api, true);

	  	          Thread.sleep(500);

	  	          String envrnmnt = expr.split("OdysseyGateway")[7].split(":")[2].trim();

	  	          Reporter.log("Environment :- " +envrnmnt, true);

	  	          Thread.sleep(500);

	  	          String officeid = expr.split("OdysseyGateway")[7].split(":")[3].trim();

	  	          Reporter.log("Office Id :- " +officeid, true);

	  	          Thread.sleep(2000);

	    */   

	  	   //For Passenger details         

	       try

	       {

	    	   // Navigate on pax info

	    	   driver.findElement(By.xpath("//*[@id='PaxInfo']")).click();
	    	   
	    	   Thread.sleep(2000);
  	    	 driver1 = new Augmenter().augment(driver);
	     			File file8  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	     			FileUtils.copyFile(file8, new File("E:\\Automation\\Faregeek_Test\\Paxinfopage.png"));
	     			
	     			// For Image comparison
	     						String actual8 = "E:\\Automation\\Faregeek_Test\\Paxinfopage.png";
	     						String reference8 = "E:\\Automation\\Reference images\\Faregeek\\Paxinfopage.png";
	     						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
	     						String Output8 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Paxinfopage.png";

	     						ProcessBuilder pb8 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output8, actual8, reference8, Log);
	     						
	     						pb8.directory(new File("E:\\node_modules\\.bin"));
	     						Process p8 = pb8.start();

	     						System.out.println("" + p8);

	    	   Thread.sleep(2000);

	    	   

	    	   System.out.println("Firstname_Of_Adult1: " + Firstname_Of_Adult1);

	           System.out.println("Middlename_of_Adult1:" + Middlename_of_Adult1);

	           System.out.println("Lastname_Of_Adult1: " + Lastname_Of_Adult1);

	           System.out.println("Lastname_Of_Adult1: " + Phone_Adult1);

	           System.out.println("Email: " + Email);

	           System.out.println("Confirm_Email: " + Confirm_Email);

	           System.out.println("Firstname_Of_Adult2: " + Firstname_Of_Adult2);

	           System.out.println("Middlename_of_Adult2:" + Middlename_of_Adult2);

	           System.out.println("Lastname_Of_Adult2: " + Lastname_Of_Adult2);

	        //   System.out.println("Lastname_Of_Adult2: " + Phone_Adult2);

	           System.out.println("Firstname_Of_Adult3: " + Firstname_Of_Adult3);

	           System.out.println("Middlename_of_Adult3:" + Middlename_of_Adult3);

	           System.out.println("Lastname_Of_Adult3: " + Lastname_Of_Adult3);

	           System.out.println("Phone_No: " + Phone_Adult3);

	                 

	           faregkpurchse.FaregeekToTitle(Firstname_Of_Adult1, Middlename_of_Adult1, Lastname_Of_Adult1, Phone_Adult1, Email, Confirm_Email, Firstname_Of_Adult2, Middlename_of_Adult2, Lastname_Of_Adult2, Firstname_Of_Adult3, Middlename_of_Adult3, Lastname_Of_Adult3, Phone_Adult3);

	       }

	       catch(Exception e)

	       {

	      	  System.out.println("\n");

	      	  System.out.println("Timeout on checkoutpage Logs..");

	      	  System.out.println("\n");

	   //   	  ExtractJSLogs();

	      	 
/*
	          final Screenshot screenshot44 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

	  		  final BufferedImage image44 = screenshot44.getImage();

	  		  ImageIO.write(image44, "PNG", new File("/Users//admin//Desktop//Odysseus//Odysseus project//FaregeekError//3_Timeouterror.png"));
*/
	           
	      	driver1 = new Augmenter().augment(driver);
			File file  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file, new File("E:\\Automation\\Faregeek_TestError\\3_Timeouterror.png"));

	          Assert.assertFalse(false, "FAIL");

	          Reporter.log("Timeout error...", true);

	     	  AssertJUnit.assertTrue("Timeout error...", faregkpurchse.isDisplayed());

	     	  throw(e);

	       }

	       

	           Thread.sleep(4000);

	           

	/*           

	        // For Apply Best Price Guarantee   

	        if(driver.findElement(By.cssSelector("#MainForm > div.col-md-9.col-xs-12.col-sm-9.pull-right.pax-main-cont > div.row.booking-section.satisfaction.panel-box.pax-info-mo > div > div > div > label [id*='ex_gr_']")).isDisplayed())

	        {                                     

	        	driver.findElement(By.cssSelector("#MainForm > div.col-md-9.col-xs-12.col-sm-9.pull-right.pax-main-cont > div.row.booking-section.satisfaction.panel-box.pax-info-mo > div > div > div > label [id*='ex_gr_']")).click();

	        	Thread.sleep(2000);

	        	Reporter.log("Best Price Gaurantee Applied successfully..", true);

	        	Thread.sleep(500);

	        	final Screenshot screenshot37 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

                final BufferedImage image37 = screenshot37.getImage();

                ImageIO.write(image37, "PNG", new File("D/Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//05_AppliedBestPriceGaurantee.png"));

                Thread.sleep(1000);

                

                //For Additional payment display in testng report

                String additionalitems = driver.findElement(By.xpath("//*[@id='60']")).getText();

	        	Reporter.log(" " +additionalitems, true);

	        	Thread.sleep(1000);

	        	

	        	// Verify price After Applied Best Price Guarantee

	        	String afteradditional = driver.findElement(By.xpath("//*[@id='PricesGTotal']")).getText();

	        	Reporter.log("After applied Best Price Guarantee: " +afteradditional, true);

	        	Thread.sleep(1000);

	        	

	        	// For remove Best Price Guarantee

	        	driver.findElement(By.cssSelector("#MainForm > div.col-md-9.col-xs-12.col-sm-9.pull-right.pax-main-cont > div.row.booking-section.satisfaction.panel-box.pax-info-mo > div > div > div > label [id*='ex_gr_']")).click();

	        	Thread.sleep(1000);

	        	final Screenshot screenshot39 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

                final BufferedImage image39 = screenshot39.getImage();

                ImageIO.write(image39, "PNG", new File("/Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//5_RemovedBestPriceGaurantee.png"));

                Thread.sleep(1500);

	        	

	        	Reporter.log("Best Price Gaurantee Removed successfully..", true);

	        	Thread.sleep(500);

	        	

	        	// Verify price After Removed Best Price Guarantee

	        	String Removedadditional = driver.findElement(By.xpath("//*[@id='PricesGTotal']")).getText();

	        	Reporter.log("After Removed Best Price Guarantee: " +Removedadditional, true);

	        	Thread.sleep(1000);

	        	

	        }

	  */  

	      try

	      {

	           //Click on Continue to Payment Button
	    	  
         	   driver.swipe(748, 1947, 748, 616, 288);
         	   driver.swipe(748, 1947, 748, 616, 288);
         	   driver.swipe(748, 1947, 748, 616, 288);


	           driver.findElement(By.id("_ctl0_MainContentsPH__ctl0_ContinueLNK")).sendKeys(Keys.ENTER);	    
	           
	           Thread.sleep(5000);
	           
	         //For noting time from traces when showtrace=true
               /*
	           try
               {
               	if(driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).isDisplayed())
               	{
               		//String ResultpageLoad = driver.findElement(By.xpath("//div[@id='TracerBlock']//following::b[last()]")).getText();
               		List<IOSElement> list = driver.findElements(By.id("TracerBlock"));
               		int listcount = list.size();    
               		@SuppressWarnings("rawtypes")
       				List optionsList = null;
               		String options[] = new String[listcount];

               		for (int i=1; i<=listcount; i++) 
               		{                     
               		    options[i-1] = driver.findElement(By.id("TracerBlock")).getText();
               		    //contains three words eg: Test Engineer Madhu               
               		    String[] expectedOptions = options[i-1].split(" ");
               		    //splitting based on the space           
               		    // I hope the next line gives you the correct lastName
               		    String lastName =expectedOptions[expectedOptions.length-1]; 
               		    // lets store the last name in the array
               		    options[i-1] = lastName;        
               		 }         
               		 optionsList = Arrays.asList(options);
               		 Reporter.log("Total Time for checkout page to payment page load(Milisec) - " +optionsList, true);
               		
               		 Thread.sleep(1000);
               		
               	}
               	
               }
               catch(Exception e)
               {
               	e.getMessage();
               }*/

	           

	      }

	      catch(Exception e)

	      {

	    	  System.out.println("\n");

	      	  System.out.println("Invalid guest information on checkoutpage Logs..");

	      	  System.out.println("\n");

	   //   	  ExtractJSLogs();

	      	 
/*
	          final Screenshot screenshot44 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

	  		  final BufferedImage image44 = screenshot44.getImage();

	  		  ImageIO.write(image44, "PNG", new File("/Users//admin//Desktop//Odysseus//Odysseus project//FaregeekError//3_InvalidGuestInfo.png"));
*/
	      	driver1 = new Augmenter().augment(driver);
			File file6  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file6, new File("E:\\Automation\\Faregeek_TestError\\3_InvalidGuestInfo.png"));

	          Assert.assertFalse(false, "FAIL");

	          Reporter.log("Invalid guest information on checkoutpage...", true);

	     	  AssertJUnit.assertTrue("Invalid guest information on checkoutpage...", faregkpurchse.isDisplayed());

	     	  throw(e);

	       }

	      

	          System.out.println("\n");

	          System.out.println("Paymentpage Logs..");

	          System.out.println("\n");

	  //        ExtractJSLogs();

	        

              Thread.sleep(1000);

	 
/*
              final Screenshot screenshot48 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

              final BufferedImage image48 = screenshot48.getImage();

              ImageIO.write(image48, "PNG", new File("/Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//6_Paymentpage.png"));
*/
              

              Thread.sleep(10000);


              //For Insurance

              try

              {
              	 	

                        driver.findElement(By.id("InsurranceCHK_167022")).click();
                        driver.findElement(By.id("InsurranceCHK_167022")).click();
                        
                        Thread.sleep(5000);
                        
                        
                        //Swipe down
                        //driver.swipe(748, 1947, 748, 616, 288);

                        Reporter.log("Insurance applied successfully..", true);
                        
                        Thread.sleep(2000);
                        driver.findElement(By.id("InsurranceCHK_167022"));
                        
             	    	 driver1 = new Augmenter().augment(driver);
           	     			File file9  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
           	     			FileUtils.copyFile(file9, new File("E:\\Automation\\Faregeek_Test\\InsuranceApplied.png"));
           	     			
           	     			// For Image comparison
           	     						String actual9 = "E:\\Automation\\Faregeek_Test\\InsuranceApplied.png";
           	     						String reference9 = "E:\\Automation\\Reference images\\Faregeek\\InsuranceApplied.png";
           	     						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
           	     						String Output9 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_InsuranceApplied.png";

           	     						ProcessBuilder pb9 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output9, actual9, reference9, Log);
           	     						
           	     						pb9.directory(new File("E:\\node_modules\\.bin"));
           	     						Process p9 = pb9.start();

           	     						System.out.println("" + p9);

           	    	   Thread.sleep(2000);
           	    	   
                       // Navigate on price summary

                        driver.findElement(By.id("PriceInfo")).click();
                        
                        Thread.sleep(3000);
            	    	 driver1 = new Augmenter().augment(driver);
          	     			File file10  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
          	     			FileUtils.copyFile(file10, new File("E:\\Automation\\Faregeek_Test\\Price_after_InsuranceApplied.png"));
          	     			
          	     			// For Image comparison
          	     						String actual10 = "E:\\Automation\\Faregeek_Test\\Price_after_InsuranceApplied.png";
          	     						String reference10 = "E:\\Automation\\Reference images\\Faregeek\\Price_after_InsuranceApplied.png";
          	     						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
          	     						String Output10 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Price_after_InsuranceApplied.png";

          	     						ProcessBuilder pb10 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output10, actual10, reference10, Log);
          	     						
          	     						pb10.directory(new File("E:\\node_modules\\.bin"));
          	     						Process p10 = pb10.start();

          	     						System.out.println("" + p10);

                        Thread.sleep(1500);
                      try
                      {
                        if(driver.findElement(By.xpath("(//div[contains(@class,'col-md-12')])[position()=9]")).isDisplayed())
                        {
                          String insuranceprice = driver.findElement(By.xpath("(//div[contains(@class,'col-md-12')])[position()=9]")).getText();
                          Reporter.log("" +insuranceprice);
                          Reporter.log(insuranceprice, true); 
                        }
                      }  
                      catch(Exception e)
                      {
                    	  e.getMessage();
                    	  System.out.println(e);
                      }

                       //For check insurance price

                        String insurprice = driver.findElement(By.id("PricesGTotal")).getText();

                        Reporter.log("Total price after insurance applied on the Payment page:- " +insurprice, true);

                        Thread.sleep(1000);

                        
/*
                        final Screenshot screenshot64 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

                        final BufferedImage image64 = screenshot64.getImage();

                        ImageIO.write(image64, "PNG", new File("/Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//7_AppliedInsurance.png"));

   */                     

                        Thread.sleep(2000);

                        

                       // Applied insurance price

                        //String appliedinsurprice = driver.findElement(By.xpath("//	")).getText();

                        //Reporter.log("Individual selected insurance price is: " +appliedinsurprice, true);

                        //Thread.sleep(1000);

                        

                       // For Navigate Billing information

                        driver.findElement(By.id("PaymentInfo")).click();

                        Thread.sleep(1000);

                        

                        //For Remove insurance
                        
                        if( driver.findElement(By.id("InsurranceCHK_NON")).isDisplayed()){

                        WebElement insurnc = driver.findElement(By.id("InsurranceCHK_NON"));

                        insurnc.click();
                        
                        Thread.sleep(3000);
                        driver.context(Native);

                        driver.findElement(By.xpath("//*[@text='OK']")).click();
                        
                        driver.context(Webview);
                        Thread.sleep(4000);

                        

                        Reporter.log("Insurance Removed successfully..", true);
                        

                        Thread.sleep(3000);
                        }
                        
                        driver.findElement(By.id("InsurranceCHK_NON"));
              	    	 driver1 = new Augmenter().augment(driver);
            	     			File file11  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
            	     			FileUtils.copyFile(file11, new File("E:\\Automation\\Faregeek_Test\\InsuranceRemoved.png"));
            	     			
            	     			// For Image comparison
            	     						String actual11 = "E:\\Automation\\Faregeek_Test\\InsuranceRemoved.png";
            	     						String reference11 = "E:\\Automation\\Reference images\\Faregeek\\InsuranceRemoved.png";
            	     						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
            	     						String Output11 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_InsuranceRemoved.png";

            	     						ProcessBuilder pb11 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output11, actual11, reference11, Log);
            	     						
            	     						pb11.directory(new File("E:\\node_modules\\.bin"));
            	     						Process p11 = pb11.start();

            	     						System.out.println("" + p11);

                           Thread.sleep(3000);

                       // Navigate on price summary

                        driver.findElement(By.id("PriceInfo")).click();
                        
                        Thread.sleep(3000);
             	    	 driver1 = new Augmenter().augment(driver);
           	     			File file12  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
           	     			FileUtils.copyFile(file12, new File("E:\\Automation\\Faregeek_Test\\Price_after_InsuranceRemoved.png"));
           	     			
           	     			// For Image comparison
           	     						String actual12 = "E:\\Automation\\Faregeek_Test\\Price_after_InsuranceRemoved.png";
           	     						String reference12 = "E:\\Automation\\Reference images\\Faregeek\\Price_after_InsuranceRemoved.png";
           	     						//String myCommand = "E:\\node_modules\\.bin\\blink-diff.cmd";
           	     						String Output12 = "E:\\Automation\\Difference of Images\\Faregeek\\Difference_Price_after_InsuranceRemoved.png";

           	     						ProcessBuilder pb12 = new ProcessBuilder(myCommand,"--threshold","5", "--output", Output12, actual12, reference12, Log);
           	     						
           	     						pb12.directory(new File("E:\\node_modules\\.bin"));
           	     						Process p12 = pb12.start();

           	     						System.out.println("" + p12);

                        Thread.sleep(2000);

                        

                        //For check insurance price

                        String insprice = driver.findElement(By.id("PricesGTotal")).getText();

                        Reporter.log("Total price after insurance removed on the Payment page:- " +insprice, true);

                        Thread.sleep(1000);  

                                               
/*
                        final Screenshot screenshot66 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

                        final BufferedImage image66 = screenshot66.getImage();

                        ImageIO.write(image66, "PNG", new File("/Users//admin//Desktop//Odysseus//Odysseus project//Faregeek//8_RemovedInsurance.png"));
*/
                        

                        Thread.sleep(2000);

                        

                     // For Navigate Billing information

                        driver.findElement(By.id("PaymentInfo")).click();

                        Thread.sleep(1000);

                         

              	      

              }

              catch(Exception e)

              {

              	   System.out.println("Insurance not applied/removed...");

       	           System.out.println("\n");

       	           System.out.println("Insurance not applied/removed on confirmation page...");

       	           System.out.println("\n");

       	   //        ExtractJSLogs();

       	      
       	           
       	       
/*
                   final Screenshot screenshot72 = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);

                   final BufferedImage image72 = screenshot72.getImage();

                   ImageIO.write(image72, "PNG", new File("/Users//admin//Desktop//Odysseus//Odysseus project//FaregeekError//9_InsurancenotAppliedOrRemoved.png"));

    */      

       	        driver1 = new Augmenter().augment(driver);
    			File file  = ((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
    			FileUtils.copyFile(file, new File("E:\\Automation\\Alanita_TestError\\9_InsurancenotAppliedOrRemoved.png"));
                   Assert.assertFalse(false, "FAIL");

                   Reporter.log("Insurance not applied/removed...", true);

		           AssertJUnit.assertTrue("Insurance not applied/removed....", faregkpurchse.isDisplayed());

		           throw(e);

              }

              

     /*         

              //For verify prices on payment page

              String priceonpaymnt = driver.findElement(By.xpath("//*[@id='PricesGTotal']")).getText();

              Reporter.log("Price on payment page: " +priceonpaymnt);

              

              if(priceonpaymnt.equals(priceoncheckout))

              {

                  System.out.println("Checkout page and Payment page's Price matched");

                  Reporter.log("Checkout page and Payment page's Price matched..");

                        	 

                  System.out.println("Actual price is:- " +priceoncheckout);

				  Reporter.log("Actual price is:- " +priceoncheckout);

              }

              else

              {

						  System.out.println("Checkout page and Payment page's Price not match..");

						  Reporter.log("Checkout page and Payment page's Price not match..");

						  System.out.println("Actual price is:- " +priceoncheckout);

						  Reporter.log("Actual price is:- " +priceoncheckout);

			  }

              

                         Thread.sleep(1500);

              

              if(priceoncheckout.equals(totalpriceonresult))

              {

                     System.out.println("Result page and Checkout page's Price matched");

                     Reporter.log("Result page and Checkout page's Price matched..");

                        	 

                     System.out.println("Actual price is:- " +priceoncheckout);

     			     Reporter.log("Actual price is:- " +priceoncheckout);

              }

              else

              {

     					  System.out.println("Result page and Checkout page's Price not match..");

     					  Reporter.log("Result page and Checkout page's Price not match..");

     					  System.out.println("Actual price is:- " +priceoncheckout);

     					  Reporter.log("Actual price is:- " +priceoncheckout);

     		  }

              

                         Thread.sleep(1500);

	    */	

	    }

	    

	    @DataProvider

	    public String[][] Authentication() throws Exception

	    {

	    	 

	   	     String[][] testObjArray = AirUtils.getTableArray("E:\\Automation\\Workspace\\Odysseus_Responsive\\src\\main\\java\\testData\\FaregeekData.xlsx","Sheet1");

	   	     return testObjArray;

	   	     

	    }

	    

	    

	    @AfterClass

	    public void closeBrowser() throws InterruptedException

	    {

	    	

	   	 

	   	   /*

	   	        if(driver!=null) 

	   	        {

	   		         System.out.println("Closing the browser");

	   		         driver.quit();

	   	        }   

	      */

	    	

	    }

	

}

